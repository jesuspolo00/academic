<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuario.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia        = ControlUsuario::singleton_usuario();
$instancia_perfil = ControlPerfil::singleton_perfil();

$user_completos     = $instancia->mostrarTodosUsuariosControl();
$datos_perfil       = $instancia_perfil->mostrarTodosPerfilesControl();
$datos_tipo_usuario = $instancia_perfil->mostrarTipoUsuarioControl();

$permisos = $instancia_permiso->permisosUsuarioControl(1, 2, 1, $id_perfil);

if (!$permisos) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?=BASE_URL?>inicio" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Usuarios
                    </h4>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
                            <div class="dropdown-header">Acciones:</div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_usuario">Agregar Usuario</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 form-inline">
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control filtro" placeholder="Buscar">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text rounded-right" id="basic-addon1">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">No.</th>
                                    <th scope="col">Documento</th>
                                    <th scope="col">Nombre completo</th>
                                    <th scope="col">Tipo de usuario</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Telefono</th>
                                    <th scope="col">Usuario</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-lowercase">
                                <?php
                                foreach ($user_completos as $usuario) {
                                    $id_user         = $usuario['id_user'];
                                    $documento       = $usuario['documento'];
                                    $nombre_apellido = $usuario['nombre'] . ' ' . $usuario['apellido'];
                                    $tipo_usuario    = $usuario['tipo_usuario'];
                                    $email           = $usuario['correo'];
                                    $user            = $usuario['user'];
                                    $estado          = $usuario['activo'];
                                    $telefono        = $usuario['telefono'];

                                    setlocale(LC_TIME, "spanish");
                                    $ultima_conexion = $instancia->ultimaConexionControl($id_user);
                                    $fecha_ult       = strftime("%A, %d de %B, %Y %H:%M", strtotime($ultima_conexion['fechareg']));

                                    $fecha_ult = ($ultima_conexion['fechareg'] == '') ? 'Nunca ha iniciado session' : $fecha_ult;

                                    if ($estado != 1) {
                                        $class   = 'btn-success btn-sm activar_user';
                                        $icon    = '<i class="fa fa-check"></i>';
                                        $tooltip = 'Activar usuario';
                                    } else {
                                        $class   = 'btn-danger btn-sm inactivar_user';
                                        $icon    = '<i class="fa fa-times"></i>';
                                        $tooltip = 'Inactivar usuario';
                                    }

                                    ?>
                                    <tr class="text-center">
                                        <td><?=$id_user?></td>
                                        <td><?=$documento?></td>
                                        <td class="text-uppercase"><?=$nombre_apellido?></td>
                                        <td><?=$tipo_usuario?></td>
                                        <td><?=$email?></td>
                                        <td><?=$telefono?></td>
                                        <td><?=$user?></td>
                                        <td>
                                            <button type="button" class="btn btn-primary btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Editar usuario" data-toggle="modal" data-target="#editar_usuario<?=$id_user?>">
                                                <i class="fa fa-user-edit"></i>
                                            </button>
                                        </td>
                                        <td>
                                            <button class="btn <?=$class?>" id="<?=$id_user?>" data-tooltip="tooltip" data-placement="bottom" title="<?=$tooltip?>">
                                                <?=$icon?>
                                            </button>
                                        </td>
                                    </tr>


                                    <div class="modal fade" id="editar_usuario<?=$id_user;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                      <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Editar usuario</h5>
                                        </div>
                                        <form method="POST">
                                            <input type="hidden" name="id_log" value="<?=$id_log;?>">
                                            <input type="hidden" name="id_user" value="<?=$id_user;?>">
                                            <div class="modal-body">
                                                <div class="row p-3">
                                                    <div class="form-group col-lg-6">
                                                        <label class="font-weight-bold">Documento <span class="text-danger">*</span></label>
                                                        <input type="text" required value="<?=$documento?>" name="doc_edit" class="form-control numeros">
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
                                                        <input type="text" required value="<?=$usuario['nombre']?>" name="nom_edit" class="form-control">
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="font-weight-bold">Apellido <span class="text-danger">*</span></label>
                                                        <input type="text" required value="<?=$usuario['apellido']?>" name="apel_edit" class="form-control">
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="font-weight-bold">Correo</label>
                                                        <input type="email" value="<?=$email?>" name="correo_edit" class="form-control">
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="font-weight-bold">Telefono</label>
                                                        <input type="text" value="<?=$usuario['telefono']?>" name="telefono_edit" class="form-control numeros">
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="font-weight-bold">Usuario</label>
                                                        <input type="text" value="<?=$usuario['user']?>" disabled class="form-control">
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="font-weight-bold">Perfil <span class="text-danger">*</span></label>
                                                        <select class="form-control" required name="perfil_edit">
                                                            <option value="<?=$usuario['perfil']?>" selected><?=$usuario['nom_perfil']?></option>
                                                            <?php
                                                            foreach ($datos_perfil as $perfiles) {
                                                                $id_perfil = $perfiles['id_perfil'];
                                                                $nombre    = $perfiles['nombre'];

                                                                $ver = ($usuario['perfil'] == $id_perfil) ? 'd-none' : '';
                                                                ?>
                                                                <option value="<?=$id_perfil?>" class="<?=$ver?>"><?=$nombre?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="font-weight-bold">Tipo usuario <span class="text-danger">*</span></label>
                                                        <select class="form-control" required name="tipo_user_edit">
                                                            <option value="<?=$usuario['tipo_user']?>" selected><?=$usuario['tipo_usuario']?></option>
                                                            <?php
                                                            foreach ($datos_tipo_usuario as $tipo) {
                                                                $id_tipo = $tipo['id'];
                                                                $nombre  = $tipo['nombre'];

                                                                $ver = ($usuario['tipo_user'] == $id_tipo) ? 'd-none' : '';

                                                                ?>
                                                                <option value="<?=$id_tipo?>" class="<?=$ver?>"><?=$nombre?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-lg-12 mt-2">
                                                        <p class="text-danger">Ult. conexion: <span class="font-weight-bold"><?=$fecha_ult?></span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer border-0">
                                                <button type="button" class="btn btn-primary btn-sm mr-auto restart_pass" id="<?=$id_user?>">
                                                    <i class="fas fa-redo-alt"></i>
                                                    &nbsp;
                                                    Restablecer contrase&ntilde;a
                                                </button>

                                                <button class="btn btn-danger btn-sm" data-dismiss="modal" type="button">
                                                    <i class="fa fa-times"></i>
                                                    Cancelar
                                                </button>
                                                <button class="btn btn-success btn-sm" type="submit">
                                                    <i class="fa fa-save"></i>
                                                    Guardar
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'usuarios' . DS . 'agregar_usuario.php';

if (isset($_POST['identificacion'])) {
    $instancia->registrarUsuarioControl();
}

if (isset($_POST['doc_edit'])) {
    $instancia->editarUsuarioControl();
}
?>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/usuario/funcionesUsuario.js"></script>