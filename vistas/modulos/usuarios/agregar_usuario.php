<!-- Agregar usuario -->
<div class="modal fade" id="agregar_usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Agregar usuario</h5>
      </div>
      <form method="POST" id="registro">
        <input type="hidden" name="id_log" value="<?= $id_log?>">
        <div class="modal-body border-0 p-3">
          <div class="row p-3">
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Nombres <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="nombre" minlength="1" maxlength="50" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Apellidos <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="apellido" minlength="1" maxlength="50" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Identificaci&oacute;n <span class="text-danger">*</span></label>
              <input type="text" class="form-control numeros" name="identificacion" minlength="1" maxlength="50" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Usuario <span class="text-danger">*</span></label>
              <input type="text" class="form-control user" name="usuario" id="usuario" minlength="1" maxlength="50" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Contrase&ntilde;a <span class="text-danger">*</span></label>
              <input type="password" class="form-control" name="pass" minlength="8" maxlength="16" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Perfil <span class="text-danger">*</span></label>
              <select class="form-control" required name="perfil">
                <option value="" selected>Seleccione una opci&oacute;n...</option>
                <?php 
                foreach ($datos_perfil as $perfiles) {
                  $id_perfil = $perfiles['id_perfil'];
                  $nombre = $perfiles['nombre'];
                  ?>
                  <option value="<?= $id_perfil?>"><?= $nombre?></option>
                  <?php
                }
                ?>
              </select>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Tipo de usuario <span class="text-danger">*</span></label>
              <select class="form-control" required name="tipo_usuario">
                <option value="" selected>Seleccione una opci&oacute;n...</option>
                <?php 
                foreach ($datos_tipo_usuario as $tipo) {
                  $id_tipo = $tipo['id'];
                  $nombre = $tipo['nombre'];
                  ?>
                  <option value="<?= $id_tipo?>"><?= $nombre?></option>
                  <?php
                }
                ?>
              </select>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Email</label>
              <input type="email" class="form-control" name="email">
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Telefono</label>
              <input type="text" class="form-control numeros" name="telefono">
            </div>
          </div>
        </div>
        <div class="modal-footer border-0">
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
            <i class="fa fa-times"></i>
            &nbsp;
            Cerrar
          </button>
          <button type="button" class="btn btn-success btn-sm" id="registrar_user">
            <i class="fa fa-save"></i>
            &nbsp;
            Registrar
          </button>
        </div>
      </form>
    </div>
  </div>
</div>