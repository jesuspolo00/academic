<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'estudiante' . DS . 'ControlEstudiante.php';
require_once CONTROL_PATH . 'lectivo' . DS . 'ControlLectivo.php';
require_once CONTROL_PATH . 'cursos' . DS . 'ControlCurso.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia         = ControlEstudiante::singleton_estudiante();
$instancia_lectivo = ControlLectivo::singleton_lectivo();
$instancia_curso   = ControlCursos::singleton_cursos();

$datos_tipo_documento = $instancia_lectivo->mostrarTipoDocumentoControl();

if (isset($_GET['estudiante'])) {
	$id_estudiante    = base64_decode($_GET['estudiante']);
	$id_prematricula  = base64_decode($_GET['prematricula']);
	$datos_estudiante = $instancia->mostrarEstudianteIdControl($id_estudiante);
	$datos_familiar   = $instancia->mostrarFamiliaresControl($id_estudiante);

	$imagen = ($datos_estudiante['imagen'] == '') ? 'https://placehold.it/220x220' : PUBLIC_PATH . 'upload/' . $datos_estudiante['imagen'];
	$genero = ($datos_estudiante['genero'] == 'M') ? 'Masculino' : 'Femenino';

} else {
	include_once VISTA_PATH . 'modulos' . DS . '404.php';
	exit();
}

$permisos = $instancia_permiso->permisosUsuarioControl(1, 4, 1, $id_perfil);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						<a href="<?=BASE_URL?>estudiante/listado" class="text-decoration-none">
							<i class="fa fa-arrow-left text-primary"></i>
						</a>
						&nbsp;
						Subir documentos
					</h4>
				</div>
				<form method="POST" enctype="multipart/form-data">
					<input type="hidden" value="<?=$id_log?>" name="id_log">
					<input type="hidden" value="<?=$id_estudiante?>" name="id_estudiante">
					<input type="hidden" value="<?=$id_prematricula?>" name="prematricula">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12 text-center mb-3 text-primary">
								<h4 class="font-weight-bold">Datos Basicos</h4>
								<hr>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Foto del estudiante</label>
								<div class="col-sm-12 text-center mt-4">
									<img src="<?=$imagen?>" id="preview" class="img-thumbnail img-fluid">
								</div>
							</div>
							<div class="col-lg-8">
								<div class="row">
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Nombres</label>
										<input type="text" class="form-control text-uppercase" disabled value="<?=$datos_estudiante['primer_nombre'] . ' ' . $datos_estudiante['segundo_nombre']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Apellidos</label>
										<input type="text" class="form-control text-uppercase" disabled value="<?=$datos_estudiante['primer_apellido'] . ' ' . $datos_estudiante['segundo_apellido']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Tipo de documento</label>
										<input type="text" class="form-control text-uppercase" disabled value="<?=$datos_estudiante['tipo_documento']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Numero de documento</label>
										<input type="text" class="form-control" disabled value="<?=$datos_estudiante['identificacion']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Genero</label>
										<input type="text" class="form-control" disabled value="<?=$genero?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Fecha de nacimiento</label>
										<input type="text" class="form-control" disabled value="<?=$datos_estudiante['fecha_nacimiento']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Edad</label>
										<input type="text" class="form-control" disabled value="<?=calculaedad($datos_estudiante['fecha_nacimiento'])?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Curso</label>
										<input type="text" class="form-control" disabled value="<?=$datos_estudiante['curso']?>">
									</div>
								</div>
							</div>

							<div class="col-lg-12 mt-4"></div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Documento del estudiante <span class="text-danger">*</span></label>
								<input id="file" type="file" class="file" name="documento_estudiante" accept=".png,.jpg,.jpeg,.pdf" data-preview-file-type="any" required>
							</div>

							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Certfificado medico <span class="text-danger">*</span></label>
								<input id="file" type="file" class="file" name="certificado_medico" accept=".png,.jpg,.jpeg,.pdf" data-preview-file-type="any" required>
							</div>

							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Certfificado (vista - audicion) <span class="text-danger">*</span></label>
								<input id="file" type="file" class="file" name="certificado_vista" accept=".png,.jpg,.jpeg,.pdf" data-preview-file-type="any" required>
							</div>

							<!-------------------------------------------------------------------------------------------------------------------->

							<div class="col-lg-12 text-center mt-4 mb-3 text-primary">
								<h4 class="font-weight-bold">Datos de familiares</h4>
								<hr>
							</div>
							<?php
							foreach ($datos_familiar as $familiar) {
								$id_familiar      = $familiar['id'];
								$nombres          = $familiar['primer_nombre'] . ' ' . $familiar['segundo_nombre'];
								$apellidos        = $familiar['primer_apellido'] . ' ' . $familiar['segundo_apellido'];
								$documento        = $familiar['documento'];
								$celular          = $familiar['celular'];
								$tel_residencia   = $familiar['tel_residencia'];
								$tel_oficina      = $familiar['tel_oficina'];
								$id_tipo_familiar = $familiar['id_tipo_familiar'];
								$tipo_familiar    = $familiar['tipo_familiar'];
								$contacto         = $familiar['contacto'];
								$familiaridad     = $familiar['familiaridad'];
								$id_relacion      = $familiar['id_relacion'];
								$resp_economico   = $familiar['resp_economico'];
								$imagen_familiar  = ($familiar['imagen'] == '') ? 'https://placehold.it/220x220' : PUBLIC_PATH . 'upload/' . $familiar['imagen'];

								$span     = ($id_tipo_familiar == $contacto) ? '<h6><span class="badge badge-info mt-3 p-1">Responsable contacto</span></h6>' : '';
								$span_eco = ($resp_economico == 'Si') ? '<h6><span class="badge badge-success mt-3 ml-2 p-1">Responsable economico</span></h6>' : '';
								?>
								<input type="hidden" name="id_familiar[]" value="<?=$id_familiar?>">
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Foto del familiar</label>
									<div class="col-sm-12 text-center mt-4">
										<img src="<?=$imagen_familiar?>" id="preview" class="img-thumbnail img-fluid">
									</div>
								</div>
								<div class="col-lg-8 form-group mb-4">
									<div class="row">
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Nombres</label>
											<input type="text" class="form-control text-uppercase" disabled value="<?=$nombres?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Apellidos</label>
											<input type="text" class="form-control text-uppercase" disabled value="<?=$apellidos?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Documento</label>
											<input type="text" class="form-control" disabled value="<?=$documento?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Celular</label>
											<input type="text" class="form-control" disabled value="<?=$celular?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Telefono residencia</label>
											<input type="text" class="form-control" disabled value="<?=$tel_residencia?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Telefono oficina</label>
											<input type="text" class="form-control" disabled value="<?=$tel_oficina?>">
										</div>
										<div class="col-lg-6 form-group">
											<label class="font-weight-bold">Documento del familiar <span class="text-danger">*</span></label>
											<input id="file" type="file" class="file" name="documento_familiar[]" accept=".png,.jpg,.jpeg,.pdf" data-preview-file-type="any" required>
										</div>
										<div class="col-lg-6 form-group mt-4 d-flex">
											<?=$span?> <?=$span_eco?>
										</div>
									</div>
								</div>
								<?php
							}
							?>

							<div class="col-lg-12 mt-4">
								<button class="btn btn-success btn-sm float-right" type="submit">
									<i class="fa fa-arrow-right"></i>
									&nbsp;
									Guardar y siguiente
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_estudiante'])) {
	$instancia->documentosSolicitadosControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/fileinput2.js"></script>