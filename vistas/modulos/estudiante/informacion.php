<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'estudiante' . DS . 'ControlEstudiante.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlEstudiante::singleton_estudiante();

$contacto = $instancia->mostrarContactoControl();

if (isset($_GET['estudiante'])) {
	$id_estudiante    = base64_decode($_GET['estudiante']);
	$datos_estudiante = $instancia->mostrarEstudianteIdControl($id_estudiante);
	$genero           = ($datos_estudiante['genero'] == 'M') ? 'Masculino' : 'Femenino';
} else {
	include_once VISTA_PATH . 'modulos' . DS . '404.php';
	exit();
}

$permisos = $instancia_permiso->permisosUsuarioControl(1, 4, 1, $id_perfil);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						<a href="<?=BASE_URL?>estudiante/listado" class="text-decoration-none">
							<i class="fa fa-arrow-left text-primary"></i>
						</a>
						&nbsp;
						Completar informaci&oacute;n
					</h4>
				</div>
				<form method="POST" enctype="multipart/form-data">
					<input type="hidden" value="<?=$id_log?>" name="id_log">
					<input type="hidden" value="<?=$id_estudiante?>" name="id_estudiante">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12 text-center mb-3 text-primary">
								<h4 class="font-weight-bold">Datos Basicos</h4>
								<hr>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Foto del estudiante</label>
								<div class="col-sm-12 text-center mt-4">
									<img src="https://placehold.it/220x220" id="preview" class="img-thumbnail img-fluid">
								</div>
								<input type="file" name="foto_estudiante" class="file2" accept="image/*">
								<div class="input-group my-3">
									<input type="text" class="form-control" disabled id="file">
									<div class="input-group-append">
										<button type="button" class="browse btn btn-primary">Buscar...</button>
									</div>
								</div>
							</div>
							<div class="col-lg-8">
								<div class="row">
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Nombres</label>
										<input type="text" class="form-control text-uppercase" disabled value="<?=$datos_estudiante['primer_nombre'] . ' ' . $datos_estudiante['segundo_nombre']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Apellidos</label>
										<input type="text" class="form-control text-uppercase" disabled value="<?=$datos_estudiante['primer_apellido'] . ' ' . $datos_estudiante['segundo_apellido']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Tipo de documento</label>
										<input type="text" class="form-control text-uppercase" disabled value="<?=$datos_estudiante['tipo_documento']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Numero de documento</label>
										<input type="text" class="form-control" disabled value="<?=$datos_estudiante['identificacion']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Genero</label>
										<input type="text" class="form-control" disabled value="<?=$genero?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Fecha de nacimiento</label>
										<input type="text" class="form-control" disabled value="<?=$datos_estudiante['fecha_nacimiento']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Edad</label>
										<input type="text" class="form-control" disabled value="<?=calculaedad($datos_estudiante['fecha_nacimiento'])?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Curso</label>
										<input type="text" class="form-control" disabled value="<?=$datos_estudiante['curso']?>">
									</div>
								</div>
							</div>



							<!---------------------------Datos Adicionales ------------------------------------>
							<div class="col-lg-12 text-center mt-4 mb-4 text-primary">
								<h4 class="font-weight-bold">Datos Adicionales</h4>
								<hr>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Tipo de sangre</label>
								<select class="form-control" name="tipo_sangre">
									<option value="" selected>Seleccione una opcion...</option>
									<option value="A+">A+</option>
									<option value="B+">B+</option>
									<option value="AB+">AB+</option>
									<option value="O+">O+</option>
									<option value="O-">O-</option>
									<option value="A-">A-</option>
									<option value="B-">B-</option>
									<option value="AB-">AB-</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Religion</label>
								<input type="text" class="form-control" name="religion">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Peso (Kg)</label>
								<input type="text" class="form-control" name="peso">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Estatura (Cm)</label>
								<input type="text" class="form-control" name="estatura">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Tiene hermanos</label>
								<select class="form-control" name="hermanos">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hijo mayor</label>
								<select class="form-control" name="mayor">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Estado civil (Padres)</label>
								<select class="form-control" name="estado_civil">
									<option value="" selected>Seleccione una opcion...</option>
									<option value="Casados">Casados</option>
									<option value="Divorciados">Divorciados</option>
									<option value="Separados">Separados</option>
									<option value="Union Libre">Union Libre</option>
									<option value="Viudo">Viudo</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Estrato</label>
								<select class="form-control" name="estrato">
									<option value="" selected>Seleccione una opcion...</option>
									<option value="Uno">Uno</option>
									<option value="Dos">Dos</option>
									<option value="Tres">Tres</option>
									<option value="Cuatro">Cuatro</option>
									<option value="Quinto">Quinto</option>
									<option value="Sexto">Sexto</option>
									<option value="Septimo">Septimo</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">En caso de emergencia contactar a <span class="text-danger">*</span></label>
								<select class="form-control" name="contactar" required>
									<option value="" selected>Seleccione una opcion...</option>
									<?php
									foreach ($contacto as $dato) {
										$id_contacto = $dato['id'];
										$nombre      = $dato['nombre'];
										?>
										<option value="<?=$id_contacto?>"><?=$nombre?></option>
										<?php
									}
									?>
								</select>
							</div>



							<!---------------------------Datos Medicos ------------------------------------>
							<div class="col-lg-12 text-center mt-4 mb-4 text-primary">
								<h4 class="font-weight-bold">Datos Medicos</h4>
								<hr>
							</div>
							<!---------------------------Antecedente Generales ------------------------------------>
							<div class="col-lg-12 mb-2">
								<h5 class="font-weight-bold ml-2 text-primary">Antecedente Generales</h5>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Usa diariamente medicamentos con prescripción m&eacute;dica</label>
								<select class="form-control"  name="descp_medica">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Nombre</label>
								<input type="text" name="nom_descp" class="form-control">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Dosis</label>
								<input type="text" name="dosis_descp" class="form-control">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Presenta alergias</label>
								<select class="form-control"  name="alerg_medica">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">¿Especifica a que?</label>
								<input type="text" name="espc_alerg" class="form-control">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Tratamiento</label>
								<input type="text" name="trat_alerg" class="form-control">
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Alergia alimento</label>
								<select class="form-control"  name="alerg_alimento">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-6">
								<label class="font-weight-bold">Especifique cuales</label>
								<input type="text" name="especifique_alimento" class="form-control">
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Dieta</label>
								<select class="form-control"  name="dieta">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">¿Cual?</label>
								<input type="text" class="form-control" name="cual_dieta">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Seguro de Accidente</label>
								<select class="form-control" name="seguro_accidente">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Antialergicos (alergias) Loratadina</label>
								<select class="form-control" name="antialergicos">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Analgesicos (dolor y fiebre) Dolex</label>
								<select class="form-control" name="analgesicos">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Antiacido Milanta</label>
								<select class="form-control" name="milanta">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Antiespasmodico (cólicos mestruales) Buscapina</label>
								<select class="form-control" name="antiespasmodico">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Seguro o EPS afiliada</label>
								<input type="text" class="form-control" name="eps">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Nombre del m&eacute;dico</label>
								<input type="text" class="form-control" name="nom_medico">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Telefono del m&eacute;dico</label>
								<input type="text" class="form-control numeros" name="tel_medico">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Clinica de preferencia</label>
								<input type="text" class="form-control" name="clinica">
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">N&uacute;mero de afilicaci&oacute;n</label>
								<input type="text" class="form-control numeros" name="num_afilia">
							</div>



							<!---------------------------Antecedente Personales ------------------------------------>
							<div class="col-lg-12 mb-2 mt-4">
								<h5 class="font-weight-bold ml-2 text-primary">Antecedente Personales</h5>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Enfermedad en los ojos</label>
								<select name="enf_ojos" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Lentes permanentes</label>
								<select name="lentes_perm" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Asma</label>
								<select name="asma" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Enf. Respiratorias</label>
								<select name="enf_respiratorias" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Enf. Card&iacute;acas</label>
								<select name="enf_cardiaca" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Enf. G&aacute;stricas</label>
								<select name="enf_gastricas" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Diab&eacute;tes</label>
								<select name="diabetes" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Celiaquismo</label>
								<select name="celiaquismo" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Problemas psiqui&aacute;tricos</label>
								<select name="pro_psiqui" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hepatitis</label>
								<select name="hepatitis" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Anemia</label>
								<select name="anemia" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hipertensi&oacute;n arterial</label>
								<select name="hiper_arterial" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hipotensi&oacute;n arterial</label>
								<select name="hipo_arterial" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Epilepsia</label>
								<select name="epilepsia" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hernias</label>
								<select name="hernias" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Migra&nacute;a</label>
								<select name="migrana" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fractura / Traumas</label>
								<select name="frac_traum" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Artritis reumatoidea</label>
								<select name="artritis" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Desnutrici&oacute;n</label>
								<select name="desnutricion" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Obesidad</label>
								<select name="obesidad" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">C&aacute;ncer</label>
								<select name="cancer" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">VIH</label>
								<select name="vih" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Enfermedad renal crónica (no incluye infecciones urinarias)</label>
								<select name="enf_renal" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Otros</label>
								<input type="text" class="form-control" name="otros">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Protesis</label>
								<input type="text" class="form-control" name="protesis">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Cirug&iacute;as</label>
								<input type="text" class="form-control" name="cirugias">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">El estudiantes ha convulsionado o perdido el conocimiento?</label>
								<input type="text" class="form-control" name="convulsion">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Enfermedad Actual</label>
								<input type="text" class="form-control" name="enf_actual">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Medicamentos que no puede recibir</label>
								<input type="text" class="form-control" name="medicamentos">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Condiciones especiales de salud</label>
								<input type="text" class="form-control" name="cond_salud">
							</div>




							<!---------------------------Antecedentes Familiares ------------------------------------>
							<div class="col-lg-12 mb-2 mt-4">
								<h5 class="font-weight-bold ml-2 text-primary">Antecedentes Familiares</h5>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Diabetes</label>
								<select name="diabetes_fam" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">C&aacute;ncer</label>
								<select name="cancer_fam" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hipertensi&oacute;n</label>
								<select name="hiper_fam" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Enf. Cardiovascular</label>
								<select name="cardiovascular_fam" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Otros</label>
								<input type="text" name="otros_fam" class="form-control">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">¿El estudiante ha sido diagnosticado con covid-19?</label>
								<select name="covid" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">¿Algún miembro del núcleo familiar del estudiante ha sido diagnosticado con covid-19?</label>
								<select name="covid_diag" class="form-control">
									<option value="NO" selected>NO</option>
									<option value="SI">SI</option>
								</select>
							</div>
							<div class="col-lg-6">
								<label class="font-weight-bold">Etnia</label>
								<input type="text" class="form-control" name="etnia">
							</div>
							<div class="col-lg-12 mt-4">
								<button class="btn btn-success btn-sm float-right" type="submit">
									<i class="fa fa-arrow-right"></i>
									&nbsp;
									Guardar y siguiente
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
	$instancia->informacionAdicionalControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/fileInput.js"></script>