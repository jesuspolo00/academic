<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'estudiante' . DS . 'ControlEstudiante.php';
require_once CONTROL_PATH . 'lectivo' . DS . 'ControlLectivo.php';
require_once CONTROL_PATH . 'cursos' . DS . 'ControlCurso.php';

$instancia         = ControlEstudiante::singleton_estudiante();
$instancia_lectivo = ControlLectivo::singleton_lectivo();
$instancia_curso   = ControlCursos::singleton_cursos();

$datos_curso   = $instancia_curso->mostrarTodosCursosControl();
$datos_lectivo = $instancia_lectivo->mostrarTodosLectivoControl();

$permisos = $instancia_permiso->permisosUsuarioControl(1, 4, 1, $id_perfil);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						<a href="<?=BASE_URL?>estudiante/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-primary"></i>
						</a>
						&nbsp;
						Estudiantes - Listado
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
							<div class="dropdown-header">Acciones:</div>
							<a class="dropdown-item" href="<?=BASE_URL?>estudiante/index">Agregar estudiante</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">A&ntilde;o lectivo</label>
								<select class="form-control" name="lectivo">
									<?php
									foreach ($datos_lectivo as $lectivo) {
										$id_lectivo = $lectivo['id'];
										$nombre     = $lectivo['nombre'];
										$activo     = $lectivo['activo'];

										$select = ($activo == 1) ? 'selected' : '';
										$on_off = ($activo == 1) ? 'ON' : 'OFF';
										?>
										<option value="<?=$id_lectivo?>" <?=$select?>><?=$nombre . ' - ' . $on_off?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Grado</label>
								<select name="curso" class="form-control filtro_change">
									<option value="0" selected>Todos los estudiantes</option>
									<?php
									foreach ($datos_curso as $curso) {
										$id_curso = $curso['id'];
										$nombre   = $curso['nombre'];
										$nivel    = $curso['nivel'];
										$activo   = $curso['activo'];

										$ver = ($activo == 1) ? '' : 'd-none';

										?>
										<option value="<?=$id_curso?>" class="<?=$ver?>"><?=$nombre . ' - ' . $nivel?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-3 form-group">
								<label>&nbsp;</label>
								<input type="text" class="form-control filtro" placeholder="Buscar estudiante..." name="estudiante">
							</div>
							<div class="col-lg-1 mt-2-5 ml-auto">
								<button class="btn btn-primary btn-sm float-right" type="submit">
									<i class="fa fa-search"></i>
									&nbsp;
									Buscar
								</button>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-4">
						<table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No.</th>
									<th scope="col">Identificaci&oacute;n</th>
									<th scope="col">Nombre Completo</th>
									<th scope="col">A&ntilde;o Lectivo</th>
									<th scope="col">Grado</th>
								</tr>
							</thead>
							<tbody class="buscar text-uppercase">
								<?php
								if (isset($_POST['lectivo'])) {

									$estudiantes_completos = $instancia->mostrarTodosEstudiantesControl();

									$cantidad    = count($estudiantes_completos);
									$paginas     = ceil($cantidad / 15);
									$ver_paginar = '';

									if (count($estudiantes_completos) == 0) {
										?>
										<tr class="text-center">
											<td colspan="5">No hay datos que mostrar</td>
										</tr>
										<?php
									} else {
										foreach ($estudiantes_completos as $estudiante_d) {
											$prematricula    = $estudiante_d['prematricula'];
											$id_estudiante   = $estudiante_d['id'];
											$identificacion  = $estudiante_d['identificacion'];
											$nombre_completo = $estudiante_d['nombre'] . ' ' . $estudiante_d['apellido'];
											$curso           = $estudiante_d['curso'];
											$lectivo         = $estudiante_d['lectivo'];

											$informacion         = $instancia->consultarInformcacionEstudianteControl($id_estudiante);
											$documentos_sub      = $instancia->verificarDocumentosSubidosControl($id_estudiante);
											$documentos_inst_sub = $instancia->verificarDocumentosInstSubidosControl($id_estudiante);
											$documentos_obl      = $instancia->verificarObligatorioDocInstControl();

											if ($informacion['datos_medicos'] == 'falta') {
												$completar_info  = '';
												$hoja_vida       = 'd-none';
												$url             = BASE_URL . 'estudiante/informacion?estudiante=' . base64_encode($id_estudiante);
												$documentos      = 'd-none';
												$documentos_inst = 'd-none';
											} else if ($informacion['asignar_familiar'] == 'falta') {
												$completar_info  = '';
												$hoja_vida       = 'd-none';
												$url             = BASE_URL . 'estudiante/familiar?estudiante=' . base64_encode($id_estudiante);
												$documentos      = 'd-none';
												$documentos_inst = 'd-none';
											}

											if ($informacion['datos_medicos'] != 'falta' && $informacion['asignar_familiar'] != 'falta') {
												$completar_info  = 'd-none';
												$hoja_vida       = '';
												$url             = '#';
												$documentos      = '';
												$documentos_inst = 'd-none';
											}

											if ($documentos_sub['cant'] != 0) {
												$documentos      = 'd-none';
												$documentos_inst = '';
											}

											if ($documentos_obl['id'] == "") {
												$documentos_inst = 'd-none';
											}

											if ($documentos_inst_sub['cant'] != 0) {
												$documentos_inst = 'd-none';
											}

											?>
											<tr class="text-center">
												<td><?=$id_estudiante?></td>
												<td><?=$identificacion?></td>
												<td><?=$nombre_completo?></td>
												<td><?=$lectivo?></td>
												<td><?=$curso?></td>
												<td>
													<a href="<?=BASE_URL?>estudiante/hoja_vida?estudiante=<?=base64_encode($id_estudiante)?>" class="btn btn-info btn-sm <?=$hoja_vida?>" data-tooltip="tooltip" title="Hoja de vida" data-placement="bottom">
														<i class="fas fa-id-badge"></i>
													</a>
													<a  href="<?=BASE_URL?>estudiante/documentos?estudiante=<?=base64_encode($id_estudiante)?>&prematricula=<?=base64_encode($prematricula)?>" class="btn btn-secondary btn-sm <?=$documentos?>" data-tooltip="tooltip" data-placement="bottom" title="Subir documentos">
														<i class="fas fa-id-card"></i>
													</a>
													<a  href="<?=BASE_URL?>estudiante/documentos_inst?estudiante=<?=base64_encode($id_estudiante)?>&prematricula=<?=base64_encode($prematricula)?>" class="btn btn-primary btn-sm <?=$documentos_inst?>" data-tooltip="tooltip" data-placement="bottom" title="Documentos institucionales">
														<i class="fas fa-file-pdf"></i>
													</a>
												</td>
												<td class="<?=$completar_info?>">
													<a href="<?=$url?>" class="btn btn-success btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Completar informacion">
														<i class="fas fa-clipboard-list"></i>
													</a>
												</td>
											</tr>
											<?php
										}
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>