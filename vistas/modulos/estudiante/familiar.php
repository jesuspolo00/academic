<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'estudiante' . DS . 'ControlEstudiante.php';
require_once CONTROL_PATH . 'lectivo' . DS . 'ControlLectivo.php';

$permisos = $instancia_permiso->permisosUsuarioControl(1, 4, 1, $id_perfil);

$instancia         = ControlEstudiante::singleton_estudiante();
$instancia_lectivo = ControlLectivo::singleton_lectivo();

$datos_tipo_documento = $instancia_lectivo->mostrarTipoDocumentoControl();
$contacto             = $instancia->mostrarContactoControl();

if (isset($_GET['estudiante'])) {
	$id_estudiante    = base64_decode($_GET['estudiante']);
	$datos_estudiante = $instancia->mostrarEstudianteIdControl($id_estudiante);
	$datos_familiar   = $instancia->mostrarFamiliaresControl($id_estudiante);

	$informacion = $instancia->consultarInformcacionEstudianteControl($id_estudiante);

	if ($informacion['asignar_familiar'] == 'listo') {
		$disabled = '';
		$url      = BASE_URL . 'estudiante/listado?pagina=1';
	} else {
		$disabled = 'disabled';
		$url      = '#';
	}

} else {
	include_once VISTA_PATH . 'modulos' . DS . '404.php';
	exit();
}

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						<a href="<?=BASE_URL?>estudiante/listado" class="text-decoration-none">
							<i class="fa fa-arrow-left text-primary"></i>
						</a>
						&nbsp;
						Completar informaci&oacute;n - Familiares <span class="text-secondary">(<?=$datos_estudiante['nombre'] . ' ' . $datos_estudiante['apellido'] . ' - ' . $datos_estudiante['curso']?>)</span>
					</h4>
				</div>
				<input type="hidden" value="<?=$id_log?>" id="id_log">
				<input type="hidden" value="<?=$id_estudiante?>" id="id_estudiante">
				<div class="card-body">
					<div class="row">
						<div class="col-lg-12 form-group">
							<label class="font-weight-bold">Identificaci&oacute;n</label>
							<div class="input-group mb-3">
								<input type="text" class="form-control numeros col-lg-4" id="documento">
								<div class="input-group-append">
									<button class="btn btn-primary" type="button" id="validar">
										<i class="fas fa-user-check"></i>
										&nbsp;
										Validar
									</button>
								</div>
							</div>
						</div>
						<div class="col-lg-12" id="registro_familiar">
						</div>
						<div class="col-lg-12 form-group mt-4 text-right" id="finalizar">
							<a class="btn btn-primary btn-sm <?=$disabled?>" href="<?=$url?>">
								<i class="fas fa-door-open"></i>
								&nbsp;
								Finalizar
							</a>
						</div>
						<div class="col-lg-12 mt-4 text-center">
							<h4 class="text-primary font-weight-bold">Tabla de familiares asignados</h4>
							<div class="table-responsive mt-4">
								<table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
									<thead>
										<tr class="text-center font-weight-bold">
											<th scope="col">Nro</th>
											<th scope="col">Familiaridad</th>
											<th scope="col">Identificaci&oacute;n</th>
											<th scope="col">Nombres</th>
											<th scope="col">Apellidos</th>
											<th scope="col">Celular</th>
											<th scope="col">Tel&eacute;fono Res.</th>
											<th scope="col">Tel&eacute;fono Ofic.</th>
										</tr>
									</thead>
									<tbody class="buscar">
										<?php
										foreach ($datos_familiar as $familiar) {
											$id_familiar      = $familiar['id'];
											$nombres          = $familiar['primer_nombre'] . ' ' . $familiar['segundo_nombre'];
											$apellidos        = $familiar['primer_apellido'] . ' ' . $familiar['segundo_apellido'];
											$documento        = $familiar['documento'];
											$celular          = $familiar['celular'];
											$tel_residencia   = $familiar['tel_residencia'];
											$tel_oficina      = $familiar['tel_oficina'];
											$id_tipo_familiar = $familiar['id_tipo_familiar'];
											$tipo_familiar    = $familiar['tipo_familiar'];
											$contacto         = $familiar['contacto'];
											$familiaridad     = $familiar['familiaridad'];
											$id_relacion      = $familiar['id_relacion'];
											$resp_economico   = $familiar['resp_economico'];

											$span     = ($id_tipo_familiar == $contacto) ? '<span class="badge badge-info">Responsable contacto</span>' : '';
											$span_eco = ($resp_economico == 'Si') ? '<span class="badge badge-success">Responsable economico</span>' : '';

											?>
											<tr class="text-center familiar<?=$id_relacion?>">
												<td><?=$id_familiar?></td>
												<td><?=$tipo_familiar?></td>
												<td><?=$documento?></td>
												<td class="text-uppercase"><?=$nombres?></td>
												<td class="text-uppercase"><?=$apellidos?></td>
												<td><?=$celular?></td>
												<td><?=$tel_residencia?></td>
												<td><?=$tel_oficina?></td>
												<td><?=$span?></td>
												<td><?=$span_eco?></td>
												<td></td>
												<td>
													<button class="btn btn-danger btn-sm remover_asig" id="<?=$id_relacion?>" data-tooltip="tooltip" data-placement="bottom" title="Remover asignacion">
														<i class="fa fa-times"></i>
													</button>
												</td>
											</tr>
											<?php
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_familiar'])) {
	$instancia->asignarFamiliarEstudianteControl();
}

if (isset($_POST['documento'])) {
	$instancia->informacionFamiliarControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/estudiante/funcionesEstudiante.js"></script>