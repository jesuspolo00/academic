<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'cursos' . DS . 'ControlCurso.php';
require_once CONTROL_PATH . 'lectivo' . DS . 'ControlLectivo.php';
require_once CONTROL_PATH . 'estudiante' . DS . 'ControlEstudiante.php';

$instancia         = ControlEstudiante::singleton_estudiante();
$instancia_curso   = ControlCursos::singleton_cursos();
$instancia_lectivo = ControlLectivo::singleton_lectivo();

$datos_curso          = $instancia_curso->mostrarTodosCursosControl();
$datos_lectivo        = $instancia_lectivo->mostrarTodosLectivoControl();
$datos_tipo_documento = $instancia_lectivo->mostrarTipoDocumentoControl();

$permisos = $instancia_permiso->permisosUsuarioControl(1, 4, 1, $id_perfil);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						<a href="<?=BASE_URL?>inicio" class="text-decoration-none">
							<i class="fa fa-arrow-left text-primary"></i>
						</a>
						&nbsp;
						Estudiantes - Registro
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
							<div class="dropdown-header">Acciones:</div>
							<a class="dropdown-item" href="<?=BASE_URL?>estudiante/listado?pagina=1">Ver estudiantes</a>
						</div>
					</div>
				</div>
				<form method="POST" enctype="multipart/form-data">
					<input type="hidden" name="id_log" value="<?=$id_log;?>">
					<div class="card-body">
						<div class="row p-3">
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">No. Identificaci&oacute;n <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="documento" required minlength="1" maxlength="50">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Tipo de Documento <span class="text-danger">*</span></label>
								<select class="form-control" required name="tipo_documento">
									<option value="" selected>Seleccione una opci&oacute;n</option>
									<?php
									foreach ($datos_tipo_documento as $tipo) {
										$id_tipo = $tipo['id'];
										$nombre  = $tipo['nombre'];
										$activo  = $tipo['activo'];

										$ver = ($activo == 1) ? '' : 'd-none';
										?>
										<option value="<?=$id_tipo?>" class="<?=$ver?>"><?=$nombre?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Lugar de expedici&oacute;n <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="lugar_exp" required minlength="1" maxlength="100">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Genero <span class="text-danger">*</span></label>
								<select class="form-control" required name="genero">
									<option value="" selected>Selecciona una opci&oacute;n</option>
									<option value="M">Masculino</option>
									<option value="F">Femenino</option>
								</select>
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Primer nombre <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="primer_nombre" required minlength="1" maxlength="100">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Segundo nombre</label>
								<input type="text" class="form-control" name="segundo_nombre" maxlength="100">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Primer apellido <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="primer_apellido" required minlength="1" maxlength="100">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Segundo apellido</label>
								<input type="text" class="form-control" name="segundo_apellido" maxlength="100">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Fecha de nacimiento <span class="text-danger">*</span></label>
								<input type="date" class="form-control" name="fecha_nac" required>
							</div>
							<div class="col-sm-12 mt-2 mb-1">
								<h5 class="font-weight-bold text-primary">
									<u>Lugar de nacimiento</u>
								</h5>
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Pais <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="pais_nac" required minlength="1" maxlength="100">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Departamento <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="departamento_nac" required minlength="1" maxlength="100">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Municipio <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="municipio_nac" required minlength="1" maxlength="100">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Telefono</label>
								<input type="text" name="telefono" class="form-control numeros" maxlength="100">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Celular</label>
								<input type="text" name="celular" class="form-control numeros"  maxlength="100">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Correo electronico <span class="text-danger">*</span></label>
								<input type="email" name="correo" class="form-control" required>
							</div>
							<div class="col-sm-12 mt-2 mb-1">
								<h5 class="font-weight-bold text-primary">
									<u>Lugar de residencia</u>
								</h5>
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Pais <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="pais_resi" required minlength="1" maxlength="100">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Departamento <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="departamento_resi" required minlength="1" maxlength="100">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Municipio <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="municipio_resi" required minlength="1" maxlength="100">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Direccion</label>
								<input type="text" class="form-control" name="direccion">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Barrio</label>
								<input type="text" class="form-control" name="barrio">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Tipo de vivienda</label>
								<select class="form-control" name="tipo_vivienda">
									<option value="" selected>Seleccione una opci&oacute;n...</option>
									<option value="Propia">Propia</option>
									<option value="Arrendada">Arrendada</option>
									<option value="Familiar">Familiar</option>
								</select>
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Bloqueado</label>
								<select class="form-control" name="bloqueado">
									<option value="1">No</option>
									<option value="0">Si</option>
								</select>
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Establecimiento de donde precede</label>
								<input type="text" class="form-control" name="esta_prece">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Inclusion</label>
								<select class="form-control" name="inclusion">
									<option value="1">No</option>
									<option value="0">Si</option>
								</select>
							</div>
							<div class="form-group col-lg-12 mt-4">
								<h5 class="font-weight-bold text-primary">
									<u>Datos Pre-matricula</u>
								</h5>
							</div>
							<div class="form-group col-lg-6">
								<label class="font-weight-bold">A&ntilde;o lectivo</label>
								<select name="anio_lectivo" class="form-control" required>
									<?php
									foreach ($datos_lectivo as $lectivo) {
										$id_lectivo = $lectivo['id'];
										$nombre     = $lectivo['nombre'];
										$activo     = $lectivo['activo'];

										$on_off   = ($activo == 1) ? 'ON' : 'OFF';
										$selected = ($activo == 1) ? 'selected' : '';
										?>
										<option value="<?=$id_lectivo?>" <?=$selected?>><?=$nombre . ' - ' . $on_off?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="form-group col-lg-6">
								<label class="font-weight-bold">Grado <span class="text-danger">*</span></label>
								<select name="id_grado" class="form-control" required>
									<option value="" selected>Seleccione una opcion...</option>
									<?php
									foreach ($datos_curso as $cursos) {
										$id_curso = $cursos['id'];
										$nombre   = $cursos['nombre'];
										?>
										<option value="<?=$id_curso?>"><?=$nombre?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="form-group col-lg-12 mt-4">
								<button type="submit" class="btn btn-success btn-sm float-right">
									<i class="fa fa-save"></i>
									&nbsp;
									Guardar
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
	$instancia->registroEstudianteControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/estudiante/funcionesEstudiante.js"></script>