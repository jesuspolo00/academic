<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'estudiante' . DS . 'ControlEstudiante.php';
require_once CONTROL_PATH . 'lectivo' . DS . 'ControlLectivo.php';
require_once CONTROL_PATH . 'cursos' . DS . 'ControlCurso.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia         = ControlEstudiante::singleton_estudiante();
$instancia_lectivo = ControlLectivo::singleton_lectivo();
$instancia_curso   = ControlCursos::singleton_cursos();

if (isset($_GET['estudiante'])) {
	$id_estudiante    = base64_decode($_GET['estudiante']);
	$datos_familiar   = $instancia->mostrarFamiliaresControl($id_estudiante);
	$datos_estudiante = $instancia->mostrarEstudianteIdControl($id_estudiante);

} else {
	include_once VISTA_PATH . 'modulos' . DS . '404.php';
	exit();
}

$permisos = $instancia_permiso->permisosUsuarioControl(1, 4, 1, $id_perfil);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						<a href="<?=BASE_URL?>estudiante/hoja_vida?estudiante=<?=base64_encode($id_estudiante)?>" class="text-decoration-none">
							<i class="fa fa-arrow-left text-primary"></i>
						</a>
						&nbsp;
						Hoja de vida - familiares  <span class="text-secondary">(<?=$datos_estudiante['nombre'] . ' ' . $datos_estudiante['apellido'] . ' - ' . $datos_estudiante['grado']?>)</span>
					</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8 form-inline">
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar">
									<div class="input-group-prepend">
										<span class="input-group-text rounded-right" id="basic-addon1">
											<i class="fa fa-search"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12 form-group">
							<a href="<?=BASE_URL?>estudiante/familiar?estudiante=<?=base64_encode($id_estudiante)?>" class="btn btn-success btn-sm">
								<i class="fa fa-plus"></i>
								&nbsp;
								Agregar familiar
							</a>
						</div>
					</div>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Nro</th>
									<th scope="col">Familiaridad</th>
									<th scope="col">Identificaci&oacute;n</th>
									<th scope="col">Nombres y apellidos</th>
									<th scope="col">Email</th>
									<th scope="col">Celular</th>
									<th scope="col">Tel&eacute;fono Oficina</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_familiar as $familiar) {
									$id_familiar      = $familiar['id'];
									$documento        = $familiar['documento'];
									$nombre_completo  = $familiar['primer_nombre'] . ' ' . $familiar['segundo_nombre'] . ' ' . $familiar['primer_apellido'] . ' ' . $familiar['segundo_apellido'];
									$familiaridad     = $familiar['familiaridad'];
									$correo           = $familiar['correo'];
									$celular          = $familiar['celular'];
									$telefono         = $familiar['tel_oficina'];
									$id_relacion      = $familiar['id_relacion'];
									$contacto         = $familiar['contacto'];
									$id_tipo_familiar = $familiar['id_tipo_familiar'];
									$tipo_familiar    = $familiar['tipo_familiar'];

									$resp_economico = $familiar['resp_economico'];

									$span     = ($id_tipo_familiar == $contacto) ? '<span class="badge badge-info">Responsable contacto</span>' : '';
									$span_eco = ($resp_economico == 'Si') ? '<span class="badge badge-success">Responsable economico</span>' : '';
									?>
									<tr class="text-center familiar<?=$id_relacion?>">
										<td><?=$id_familiar?></td>
										<td><?=$tipo_familiar?></td>
										<td><?=$documento?></td>
										<td class="text-uppercase"><?=$nombre_completo?></td>
										<td><?=$correo?></td>
										<td><?=$celular?></td>
										<td><?=$telefono?></td>
										<td><?=$span?></td>
										<td><?=$span_eco?></td>
										<td>
											<button class="btn btn-primary btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Editar" data-toggle="modal" data-target="#editar_familiar<?=$id_familiar?>">
												<i class="fa fa-edit"></i>
											</button>
										</td>
										<td>
											<button class="btn btn-danger btn-sm remover_asig" id="<?=$id_relacion?>" data-tooltip="tooltip" data-placement="bottom" title="Remover asignacion">
												<i class="fa fa-times"></i>
											</button>
										</td>
									</tr>



									<!-- Modal -->
									<div class="modal fade" id="editar_familiar<?=$id_familiar?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-primary" id="exampleModalLabel">Editar familiar</h5>
												</div>
												<div class="modal-body border-0">

												</div>
												<div class="modal-footer border-0">
													<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
													<button type="button" class="btn btn-success btn-sm">Save changes</button>
												</div>
											</div>
										</div>
									</div>

									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?=PUBLIC_PATH?>js/estudiante/funcionesEstudiante.js"></script>