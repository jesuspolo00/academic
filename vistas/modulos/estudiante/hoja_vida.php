<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'estudiante' . DS . 'ControlEstudiante.php';
require_once CONTROL_PATH . 'lectivo' . DS . 'ControlLectivo.php';
require_once CONTROL_PATH . 'cursos' . DS . 'ControlCurso.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia         = ControlEstudiante::singleton_estudiante();
$instancia_lectivo = ControlLectivo::singleton_lectivo();
$instancia_curso   = ControlCursos::singleton_cursos();

$datos_tipo_documento = $instancia_lectivo->mostrarTipoDocumentoControl();

if (isset($_GET['estudiante'])) {
	$id_estudiante    = base64_decode($_GET['estudiante']);
	$datos_estudiante = $instancia->mostrarEstudianteIdControl($id_estudiante);

	$imagen = ($datos_estudiante['imagen'] == '') ? 'https://placehold.it/220x220' : PUBLIC_PATH . 'upload/' . $datos_estudiante['imagen'];
	$genero = ($datos_estudiante['genero'] == 'M') ? 'Masculino' : 'Femenino';

} else {
	include_once VISTA_PATH . 'modulos' . DS . '404.php';
	exit();
}

$permisos = $instancia_permiso->permisosUsuarioControl(1, 4, 1, $id_perfil);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						<a href="<?=BASE_URL?>estudiante/listado" class="text-decoration-none">
							<i class="fa fa-arrow-left text-primary"></i>
						</a>
						&nbsp;
						Hoja de vida
					</h4>
					<a class="btn btn-success btn-sm ml-auto" href="<?=BASE_URL?>estudiante/listado_familiar?estudiante=<?=base64_encode($id_estudiante)?>">
						<i class="fa fa-eye"></i>
						&nbsp;
						Ver familiares
					</a>
					<button class="btn btn-primary btn-sm ml-2" data-toggle="modal" data-target="#observacion">
						<i class="fab fa-telegram-plane"></i>
						&nbsp;
						Enviar observacion
					</button>
				</div>
				<form method="POST" enctype="multipart/form-data">
					<input type="hidden" name="id_log" value="<?=$id_log?>">
					<input type="hidden" name="id_estudiante" value="<?=$id_estudiante?>">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12 text-center mb-3 text-primary">
								<h4 class="font-weight-bold">Datos Basicos</h4>
								<hr>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Foto del estudiante</label>
								<div class="col-sm-12 text-center mt-4">
									<img src="<?=$imagen?>" id="preview" class="img-thumbnail img-fluid">
								</div>
								<input type="file" name="foto_estudiante" class="file2" accept="image/*">
								<div class="input-group my-3">
									<input type="text" class="form-control" disabled id="file">
									<div class="input-group-append">
										<button type="button" class="browse btn btn-primary">Buscar...</button>
									</div>
								</div>
							</div>
							<div class="col-lg-8">
								<div class="row">
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Tipo de documento <span class="text-danger">*</span></label>
										<select name="tipo_documento" class="form-control" required>
											<option value="<?=$datos_estudiante['id_tipo_doc']?>" selected class="d-none"><?=$datos_estudiante['tipo_documento']?></option>
											<?php
											foreach ($datos_tipo_documento as $tipo) {
												$id_tipo = $tipo['id'];
												$nombre  = $tipo['nombre'];
												$activo  = $tipo['activo'];

												$ver = ($activo == 1) ? '' : 'd-none';
												?>
												<option value="<?=$id_tipo?>" class="<?=$ver?>"><?=$nombre?></option>
												<?php
											}
											?>
										</select>
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Numero de documento <span class="text-danger">*</span></label>
										<input type="text" class="form-control numeros" name="documento" value="<?=$datos_estudiante['identificacion']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Primer nombre <span class="text-danger">*</span></label>
										<input type="text" class="form-control" name="primer_nombre" value="<?=$datos_estudiante['primer_nombre']?>" required>
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Segundo nombre</label>
										<input type="text" class="form-control" name="segundo_nombre" value="<?=$datos_estudiante['segundo_nombre']?>">
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Primer apellido <span class="text-danger">*</span></label>
										<input type="text" class="form-control" name="primer_apellido" value="<?=$datos_estudiante['primer_apellido']?>" required>
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Segundo apellido</label>
										<input type="text" class="form-control" name="segundo_apellido" value="<?=$datos_estudiante['segundo_apellido']?>">
									</div>
									<div class="form-group col-lg-6">
										<label class="font-weight-bold">Genero <span class="text-danger">*</span></label>
										<select class="form-control" required name="genero">
											<option value="<?=$genero?>"><?=$genero?></option>
											<option value="M">Masculino</option>
											<option value="F">Femenino</option>
										</select>
									</div>
									<div class="col-lg-6 form-group">
										<label class="font-weight-bold">Fecha de nacimiento <span class="text-danger">*</span></label>
										<input type="date" name="fecha_nac" value="<?=$datos_estudiante['fecha_nacimiento']?>" class="form-control">
									</div>
									<div class="col-lg-12 form-group">
										<label class="font-weight-bold">Lugar de expedici&oacute;n</label>
										<input type="text" class="form-control" name="lugar_exp" value="<?=$datos_estudiante['lugar_expedicion']?>">
									</div>
								</div>
							</div>
							<div class="col-sm-12 mt-2 mb-1">
								<h5 class="font-weight-bold text-primary">
									<u>Lugar de nacimiento</u>
								</h5>
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Pais <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="pais_nac" required minlength="1" maxlength="100" value="<?=$datos_estudiante['pais_nac']?>">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Departamento <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="departamento_nac" required minlength="1" maxlength="100" value="<?=$datos_estudiante['departamento_nac']?>">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Municipio <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="municipio_nac" required minlength="1" maxlength="100" value="<?=$datos_estudiante['municipio_nac']?>">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Telefono</label>
								<input type="text" name="telefono" class="form-control numeros" maxlength="100" value="<?=$datos_estudiante['telefono']?>">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Celular</label>
								<input type="text" name="celular" class="form-control numeros"  maxlength="100" value="<?=$datos_estudiante['celular']?>">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Correo electronico <span class="text-danger">*</span></label>
								<input type="email" name="correo" class="form-control" required value="<?=$datos_estudiante['correo']?>">
							</div>

							<div class="col-sm-12 mt-2 mb-1">
								<h5 class="font-weight-bold text-primary">
									<u>Lugar de residencia</u>
								</h5>
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Pais <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="pais_res" required minlength="1" maxlength="100" value="<?=$datos_estudiante['pais_res']?>">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Departamento <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="departamento_res" required minlength="1" maxlength="100" value="<?=$datos_estudiante['departamento_res']?>">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Municipio <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="municipio_res" required minlength="1" maxlength="100" value="<?=$datos_estudiante['municipio_res']?>">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Direccion</label>
								<input type="text" class="form-control" name="direccion" value="<?=$datos_estudiante['direccion']?>">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Barrio</label>
								<input type="text" class="form-control" name="barrio" value="<?=$datos_estudiante['barrio']?>">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Tipo de vivienda</label>
								<?php $tipo_vivienda = ($datos_estudiante['tipo_vivienda'] == '') ? 'Seleccione una opcion...' : $datos_estudiante['tipo_vivienda'];?>
								<select class="form-control" name="tipo_vivienda">
									<option value="<?=$datos_estudiante['tipo_vivienda']?>" selected><?=$tipo_vivienda?></option>
									<option value="Propia">Propia</option>
									<option value="Arrendada">Arrendada</option>
									<option value="Familiar">Familiar</option>
								</select>
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Bloqueado</label>
								<?php
								$bloquedo_no = ($datos_estudiante['activo'] == 1) ? 'selected' : '';
								$bloquedo_si = ($datos_estudiante['activo'] == 0) ? 'selected' : '';
								?>
								<select class="form-control" name="bloqueado">
									<option value="1" <?=$bloquedo_no?>>No</option>
									<option value="0" <?=$bloquedo_si?>>Si</option>
								</select>
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Establecimiento de donde precede</label>
								<input type="text" class="form-control" name="esta_prece" value="<?=$datos_estudiante['estable_procede']?>">
							</div>
							<div class="form-group col-lg-4">
								<label class="font-weight-bold">Inclusion </label>
								<?php
								$inclusion_no = ($datos_estudiante['inclusion'] == 1) ? 'selected' : '';
								$inclusion_si = ($datos_estudiante['inclusion'] == 0) ? 'selected' : '';
								?>
								<select class="form-control" name="inclusion">
									<option value="1" <?=$inclusion_no?>>No</option>
									<option value="0" <?=$inclusion_si?>>Si</option>
								</select>
							</div>


							<!---------------------------Datos Adicionales ------------------------------------>
							<div class="col-lg-12 text-center mt-4 mb-4 text-primary">
								<h4 class="font-weight-bold">Datos Adicionales</h4>
								<hr>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Tipo de sangre</label>
								<select class="form-control" name="tipo_sangre">
									<?php $tipo_sangre = ($datos_estudiante['tipo_sangre'] == '') ? 'Seleccione una opcion...' : $datos_estudiante['tipo_sangre'];?>
									<option value="<?=$tipo_sangre?>" selected class="d-none"><?=$tipo_sangre?></option>
									<option value="A+">A+</option>
									<option value="B+">B+</option>
									<option value="AB+">AB+</option>
									<option value="O+">O+</option>
									<option value="O-">O-</option>
									<option value="A-">A-</option>
									<option value="B-">B-</option>
									<option value="AB-">AB-</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Religion</label>
								<input type="text" class="form-control" name="religion" value="<?=$datos_estudiante['religion']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Peso (Kg)</label>
								<input type="text" class="form-control" name="peso" value="<?=$datos_estudiante['peso']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Estatura (Cm)</label>
								<input type="text" class="form-control" name="estatura" value="<?=$datos_estudiante['estatura']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Tiene hermanos</label>
								<?php
								$hermanos_no = ($datos_estudiante['hermanos'] == 'NO') ? 'selected' : '';
								$hermanos_si = ($datos_estudiante['hermanos'] == 'SI') ? 'selected' : '';
								?>
								<select class="form-control" name="hermanos">
									<option value="NO" <?=$hermanos_no?>>NO</option>
									<option value="SI" <?=$hermanos_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hijo mayor</label>
								<?php
								$mayor_no = ($datos_estudiante['mayor'] == 'NO') ? 'selected' : '';
								$mayor_si = ($datos_estudiante['mayor'] == 'SI') ? 'selected' : '';
								?>
								<select class="form-control" name="mayor">
									<option value="NO" <?=$mayor_no?>>NO</option>
									<option value="SI" <?=$mayor_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Estado civil (Padres)</label>
								<select class="form-control" name="estado_civil">
									<?php $estado_civil = ($datos_estudiante['estado_civil'] == '') ? 'Seleccione una opcion...' : $datos_estudiante['estado_civil'];?>
									<option value="<?=$datos_estudiante['estado_civil']?>" selected><?=$estado_civil?></option>
									<option value="Casados">Casados</option>
									<option value="Divorciados">Divorciados</option>
									<option value="Separados">Separados</option>
									<option value="Union Libre">Union Libre</option>
									<option value="Viudo">Viudo</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Estrato</label>
								<select class="form-control" name="estrato">
									<?php $estrato = ($datos_estudiante['estrato'] == '') ? 'Seleccione una opcion...' : $datos_estudiante['estrato'];?>
									<option value="<?=$datos_estudiante['estrato']?>" selected><?=$estrato?></option>
									<option value="Uno">Uno</option>
									<option value="Dos">Dos</option>
									<option value="Tres">Tres</option>
									<option value="Cuatro">Cuatro</option>
									<option value="Quinto">Quinto</option>
									<option value="Sexto">Sexto</option>
									<option value="Septimo">Septimo</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">En caso de emergencia contactar a <span class="text-danger">*</span></label>
								<select class="form-control" name="contactar" required>
									<option value="<?=$datos_estudiante['id_contacto']?>" selected class="d-none"><?=$datos_estudiante['contacto']?></option>
									<?php
									foreach ($contacto as $dato) {
										$id_contacto = $dato['id'];
										$nombre      = $dato['nombre'];
										?>
										<option value="<?=$id_contacto?>"><?=$nombre?></option>
										<?php
									}
									?>
								</select>
							</div>



							<!---------------------------Datos Medicos ------------------------------------>
							<div class="col-lg-12 text-center mt-4 mb-4 text-primary">
								<h4 class="font-weight-bold">Datos Medicos</h4>
								<hr>
							</div>
							<!---------------------------Antecedente Generales ------------------------------------>
							<div class="col-lg-12 mb-2">
								<h5 class="font-weight-bold ml-2 text-primary">Antecedente Generales</h5>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Usa diariamente medicamentos con prescripción m&eacute;dica</label>
								<?php
								$descp_medica_no = ($datos_estudiante['descp_medica'] == 'NO') ? 'selected' : '';
								$descp_medica_si = ($datos_estudiante['descp_medica'] == 'SI') ? 'selected' : '';
								?>
								<select class="form-control"  name="descp_medica">
									<option value="NO" <?=$descp_medica_no?>>NO</option>
									<option value="SI" <?=$descp_medica_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Nombre</label>
								<input type="text" name="nom_descp" class="form-control" value="<?=$datos_estudiante['nom_descp']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Dosis</label>
								<input type="text" name="dosis_descp" class="form-control" value="<?=$datos_estudiante['dosis_descp']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Presenta alergias</label>
								<?php
								$alerg_medica_no = ($datos_estudiante['alerg_medica'] == 'NO') ? 'selected' : '';
								$alerg_medica_si = ($datos_estudiante['alerg_medica'] == 'SI') ? 'selected' : '';
								?>
								<select class="form-control"  name="alerg_medica">
									<option value="NO" <?=$alerg_medica_no?>>NO</option>
									<option value="SI" <?=$alerg_medica_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">¿Especifica a que?</label>
								<input type="text" name="espc_alerg" class="form-control" value="<?=$datos_estudiante['espc_alerg']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Tratamiento</label>
								<input type="text" name="trat_alerg" class="form-control" value="<?=$datos_estudiante['trat_alerg']?>">
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Alergia alimento</label>
								<?php
								$alerg_alimento_no = ($datos_estudiante['alerg_alimento'] == 'NO') ? 'selected' : '';
								$alerg_alimento_si = ($datos_estudiante['alerg_alimento'] == 'SI') ? 'selected' : '';
								?>
								<select class="form-control"  name="alerg_alimento">
									<option value="NO" <?=$alerg_alimento_no?>>NO</option>
									<option value="SI" <?=$alerg_alimento_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-6">
								<label class="font-weight-bold">Especifique cuales</label>
								<input type="text" name="especifique_alimento" class="form-control" value="<?=$datos_estudiante['especifique_alimento']?>">
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">Dieta</label>
								<?php
								$dieta_no = ($datos_estudiante['dieta'] == 'NO') ? 'selected' : '';
								$dieta_si = ($datos_estudiante['dieta'] == 'SI') ? 'selected' : '';
								?>
								<select class="form-control"  name="dieta">
									<option value="NO" <?=$dieta_no?>>NO</option>
									<option value="SI" <?=$dieta_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">¿Cual?</label>
								<input type="text" class="form-control" name="cual_dieta" value="<?=$datos_estudiante['cual_dieta']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Seguro de Accidente</label>
								<?php
								$seguro_accidente_no = ($datos_estudiante['seguro_accidente'] == 'NO') ? 'selected' : '';
								$seguro_accidente_si = ($datos_estudiante['seguro_accidente'] == 'SI') ? 'selected' : '';
								?>
								<select class="form-control" name="seguro_accidente">
									<option value="NO" <?=$seguro_accidente_no?>>NO</option>
									<option value="SI" <?=$seguro_accidente_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Antialergicos (alergias) Loratadina</label>
								<?php
								$antialergicos_no = ($datos_estudiante['antialergicos'] == 'NO') ? 'selected' : '';
								$antialergicos_si = ($datos_estudiante['antialergicos'] == 'SI') ? 'selected' : '';
								?>
								<select class="form-control" name="antialergicos">
									<option value="NO" <?=$antialergicos_no?>>NO</option>
									<option value="SI" <?=$antialergicos_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Analgesicos (dolor y fiebre) Dolex</label>
								<?php
								$analgesicos_no = ($datos_estudiante['analgesicos'] == 'NO') ? 'selected' : '';
								$analgesicos_si = ($datos_estudiante['analgesicos'] == 'SI') ? 'selected' : '';
								?>
								<select class="form-control" name="analgesicos">
									<option value="NO" <?=$analgesicos_no?>>NO</option>
									<option value="SI" <?=$analgesicos_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Antiacido Milanta</label>
								<?php
								$milanta_no = ($datos_estudiante['milanta'] == 'NO') ? 'selected' : '';
								$milanta_si = ($datos_estudiante['milanta'] == 'SI') ? 'selected' : '';
								?>
								<select class="form-control" name="milanta">
									<option value="NO" <?=$milanta_no?>>NO</option>
									<option value="SI" <?=$milanta_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Antiespasmodico (cólicos mestruales) Buscapina</label>
								<?php
								$antiespasmodico_no = ($datos_estudiante['antiespasmodico'] == 'NO') ? 'selected' : '';
								$antiespasmodico_si = ($datos_estudiante['antiespasmodico'] == 'SI') ? 'selected' : '';
								?>
								<select class="form-control" name="antiespasmodico">
									<option value="NO" <?=$antiespasmodico_no?>>NO</option>
									<option value="SI" <?=$antiespasmodico_no?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Seguro o EPS afiliada</label>
								<input type="text" class="form-control" name="eps" value="<?=$datos_estudiante['eps']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Nombre del m&eacute;dico</label>
								<input type="text" class="form-control" name="nom_medico" value="<?=$datos_estudiante['nom_medico']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Telefono del m&eacute;dico</label>
								<input type="text" class="form-control numeros" name="tel_medico" value="<?=$datos_estudiante['tel_medico']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Clinica de preferencia</label>
								<input type="text" class="form-control" name="clinica" value="<?=$datos_estudiante['clinica']?>">
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold">N&uacute;mero de afilicaci&oacute;n</label>
								<input type="text" class="form-control numeros" name="num_afilia" value="<?=$datos_estudiante['num_afilia']?>">
							</div>



							<!---------------------------Antecedente Personales ------------------------------------>
							<div class="col-lg-12 mb-2 mt-4">
								<h5 class="font-weight-bold ml-2 text-primary">Antecedente Personales</h5>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Enfermedad en los ojos</label>
								<?php
								$enf_ojos_no = ($datos_estudiante['enf_ojos'] == 'NO') ? 'selected' : '';
								$enf_ojos_si = ($datos_estudiante['enf_ojos'] == 'SI') ? 'selected' : '';
								?>
								<select name="enf_ojos" class="form-control">
									<option value="NO" <?=$enf_ojos_no?>>NO</option>
									<option value="SI" <?=$enf_ojos_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Lentes permanentes</label>
								<?php
								$lentes_perm_no = ($datos_estudiante['lentes_perm'] == 'NO') ? 'selected' : '';
								$lentes_perm_si = ($datos_estudiante['lentes_perm'] == 'SI') ? 'selected' : '';
								?>
								<select name="lentes_perm" class="form-control">
									<option value="NO" <?=$lentes_perm_no?>>NO</option>
									<option value="SI" <?=$lentes_perm_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Asma</label>
								<?php
								$asma_no = ($datos_estudiante['asma'] == 'NO') ? 'selected' : '';
								$asma_si = ($datos_estudiante['asma'] == 'SI') ? 'selected' : '';
								?>
								<select name="asma" class="form-control">
									<option value="NO" <?=$asma_no?>>NO</option>
									<option value="SI" <?=$asma_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Enf. Respiratorias</label>
								<?php
								$enf_respiratorias_no = ($datos_estudiante['enf_respiratorias'] == 'NO') ? 'selected' : '';
								$enf_respiratorias_si = ($datos_estudiante['enf_respiratorias'] == 'SI') ? 'selected' : '';
								?>
								<select name="enf_respiratorias" class="form-control">
									<option value="NO" <?=$enf_respiratorias_no?>>NO</option>
									<option value="SI" <?=$enf_respiratorias_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Enf. Card&iacute;acas</label>
								<?php
								$enf_cardiaca_no = ($datos_estudiante['enf_cardiaca'] == 'NO') ? 'selected' : '';
								$enf_cardiaca_si = ($datos_estudiante['enf_cardiaca'] == 'SI') ? 'selected' : '';
								?>
								<select name="enf_cardiaca" class="form-control">
									<option value="NO" <?=$enf_cardiaca_no?>>NO</option>
									<option value="SI" <?=$enf_cardiaca_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Enf. G&aacute;stricas</label>
								<?php
								$enf_gastricas_no = ($datos_estudiante['enf_gastricas'] == 'NO') ? 'selected' : '';
								$enf_gastricas_si = ($datos_estudiante['enf_gastricas'] == 'SI') ? 'selected' : '';
								?>
								<select name="enf_gastricas" class="form-control">
									<option value="NO" <?=$enf_gastricas_no?>>NO</option>
									<option value="SI" <?=$enf_gastricas_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Diab&eacute;tes</label>
								<?php
								$diabetes_no = ($datos_estudiante['diabetes'] == 'NO') ? 'selected' : '';
								$diabetes_si = ($datos_estudiante['diabetes'] == 'SI') ? 'selected' : '';
								?>
								<select name="diabetes" class="form-control">
									<option value="NO" <?=$diabetes_no?>>NO</option>
									<option value="SI" <?=$diabetes_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Celiaquismo</label>
								<?php
								$celiaquismo_no = ($datos_estudiante['celiaquismo'] == 'NO') ? 'selected' : '';
								$celiaquismo_si = ($datos_estudiante['celiaquismo'] == 'SI') ? 'selected' : '';
								?>
								<select name="celiaquismo" class="form-control">
									<option value="NO" <?=$celiaquismo_no?>>NO</option>
									<option value="SI" <?=$celiaquismo_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Problemas psiqui&aacute;tricos</label>
								<?php
								$pro_psiqui_no = ($datos_estudiante['pro_psiqui'] == 'NO') ? 'selected' : '';
								$pro_psiqui_si = ($datos_estudiante['pro_psiqui'] == 'SI') ? 'selected' : '';
								?>
								<select name="pro_psiqui" class="form-control">
									<option value="NO" <?=$pro_psiqui_no?>>NO</option>
									<option value="SI" <?=$pro_psiqui_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hepatitis</label>
								<?php
								$hepatitis_no = ($datos_estudiante['hepatitis'] == 'NO') ? 'selected' : '';
								$hepatitis_si = ($datos_estudiante['hepatitis'] == 'SI') ? 'selected' : '';
								?>
								<select name="hepatitis" class="form-control">
									<option value="NO" <?=$hepatitis_no?>>NO</option>
									<option value="SI" <?=$hepatitis_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Anemia</label>
								<?php
								$anemia_no = ($datos_estudiante['anemia'] == 'NO') ? 'selected' : '';
								$anemia_si = ($datos_estudiante['anemia'] == 'SI') ? 'selected' : '';
								?>
								<select name="anemia" class="form-control">
									<option value="NO" <?=$anemia_no?>>NO</option>
									<option value="SI" <?=$anemia_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hipertensi&oacute;n arterial</label>
								<?php
								$hiper_arterial_no = ($datos_estudiante['hiper_arterial'] == 'NO') ? 'selected' : '';
								$hiper_arterial_si = ($datos_estudiante['hiper_arterial'] == 'SI') ? 'selected' : '';
								?>
								<select name="hiper_arterial" class="form-control">
									<option value="NO" <?=$hiper_arterial_no?>>NO</option>
									<option value="SI" <?=$hiper_arterial_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hipotensi&oacute;n arterial</label>
								<?php
								$hipo_arterial_no = ($datos_estudiante['hipo_arterial'] == 'NO') ? 'selected' : '';
								$hipo_arterial_si = ($datos_estudiante['hipo_arterial'] == 'SI') ? 'selected' : '';
								?>
								<select name="hipo_arterial" class="form-control">
									<option value="NO" <?=$hipo_arterial_no?>>NO</option>
									<option value="SI" <?=$hipo_arterial_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Epilepsia</label>
								<?php
								$epilepsia_no = ($datos_estudiante['epilepsia'] == 'NO') ? 'selected' : '';
								$epilepsia_si = ($datos_estudiante['epilepsia'] == 'SI') ? 'selected' : '';
								?>
								<select name="epilepsia" class="form-control">
									<option value="NO" <?=$epilepsia_no?>>NO</option>
									<option value="SI" <?=$epilepsia_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hernias</label>
								<?php
								$hernias_no = ($datos_estudiante['hernias'] == 'NO') ? 'selected' : '';
								$hernias_si = ($datos_estudiante['hernias'] == 'SI') ? 'selected' : '';
								?>
								<select name="hernias" class="form-control">
									<option value="NO" <?=$hernias_no?>>NO</option>
									<option value="SI" <?=$hernias_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Migra&nacute;a</label>
								<?php
								$migrana_no = ($datos_estudiante['migrana'] == 'NO') ? 'selected' : '';
								$migrana_si = ($datos_estudiante['migrana'] == 'SI') ? 'selected' : '';
								?>
								<select name="migrana" class="form-control">
									<option value="NO" <?=$migrana_no?>>NO</option>
									<option value="SI" <?=$migrana_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Fractura / Traumas</label>
								<?php
								$frac_traum_no = ($datos_estudiante['frac_traum'] == 'NO') ? 'selected' : '';
								$frac_traum_si = ($datos_estudiante['frac_traum'] == 'SI') ? 'selected' : '';
								?>
								<select name="frac_traum" class="form-control">
									<option value="NO" <?=$frac_traum_no?>>NO</option>
									<option value="SI" <?=$frac_traum_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Artritis reumatoidea</label>
								<?php
								$artritis_no = ($datos_estudiante['artritis'] == 'NO') ? 'selected' : '';
								$artritis_si = ($datos_estudiante['artritis'] == 'SI') ? 'selected' : '';
								?>
								<select name="artritis" class="form-control">
									<option value="NO" <?=$artritis_no?>>NO</option>
									<option value="SI" <?=$artritis_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Desnutrici&oacute;n</label>
								<?php
								$desnutricion_no = ($datos_estudiante['desnutricion'] == 'NO') ? 'selected' : '';
								$desnutricion_si = ($datos_estudiante['desnutricion'] == 'SI') ? 'selected' : '';
								?>
								<select name="desnutricion" class="form-control">
									<option value="NO" <?=$desnutricion_no?>>NO</option>
									<option value="SI" <?=$desnutricion_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Obesidad</label>
								<?php
								$obesidad_no = ($datos_estudiante['obesidad'] == 'NO') ? 'selected' : '';
								$obesidad_si = ($datos_estudiante['obesidad'] == 'SI') ? 'selected' : '';
								?>
								<select name="obesidad" class="form-control">
									<option value="NO" <?=$obesidad_no?>>NO</option>
									<option value="SI" <?=$obesidad_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">C&aacute;ncer</label>
								<?php
								$cancer_no = ($datos_estudiante['cancer'] == 'NO') ? 'selected' : '';
								$cancer_si = ($datos_estudiante['cancer'] == 'SI') ? 'selected' : '';
								?>
								<select name="cancer" class="form-control">
									<option value="NO" <?=$cancer_no?>>NO</option>
									<option value="SI" <?=$cancer_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">VIH</label>
								<?php
								$vih_no = ($datos_estudiante['vih'] == 'NO') ? 'selected' : '';
								$vih_si = ($datos_estudiante['vih'] == 'SI') ? 'selected' : '';
								?>
								<select name="vih" class="form-control">
									<option value="NO" <?=$vih_no?>>NO</option>
									<option value="SI" <?=$vih_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Enfermedad renal crónica (no incluye infecciones urinarias)</label>
								<?php
								$enf_renal_no = ($datos_estudiante['enf_renal'] == 'NO') ? 'selected' : '';
								$enf_renal_si = ($datos_estudiante['enf_renal'] == 'SI') ? 'selected' : '';
								?>
								<select name="enf_renal" class="form-control">
									<option value="NO" <?=$enf_renal_no?>>NO</option>
									<option value="SI" <?=$enf_renal_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Otros</label>
								<input type="text" class="form-control" name="otros" value="<?=$datos_estudiante['otros']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Protesis</label>
								<input type="text" class="form-control" name="protesis" value="<?=$datos_estudiante['protesis']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Cirug&iacute;as</label>
								<input type="text" class="form-control" name="cirugias" value="<?=$datos_estudiante['cirugias']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">El estudiantes ha convulsionado o perdido el conocimiento?</label>
								<input type="text" class="form-control" name="convulsion" value="<?=$datos_estudiante['convulsion']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Enfermedad Actual</label>
								<input type="text" class="form-control" name="enf_actual" value="<?=$datos_estudiante['enf_actual']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Medicamentos que no puede recibir</label>
								<input type="text" class="form-control" name="medicamentos" value="<?=$datos_estudiante['medicamentos']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Condiciones especiales de salud</label>
								<input type="text" class="form-control" name="cond_salud" value="<?=$datos_estudiante['medicamentos']?>">
							</div>




							<!---------------------------Antecedentes Familiares ------------------------------------>
							<div class="col-lg-12 mb-2 mt-4">
								<h5 class="font-weight-bold ml-2 text-primary">Antecedentes Familiares</h5>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Diabetes</label>
								<?php
								$diabetes_fam_no = ($datos_estudiante['diabetes_fam'] == 'NO') ? 'selected' : '';
								$diabetes_fam_si = ($datos_estudiante['diabetes_fam'] == 'SI') ? 'selected' : '';
								?>
								<select name="diabetes_fam" class="form-control">
									<option value="NO" <?=$diabetes_fam_no?>>NO</option>
									<option value="SI" <?=$diabetes_fam_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">C&aacute;ncer</label>
								<?php
								$cancer_fam_no = ($datos_estudiante['cancer_fam'] == 'NO') ? 'selected' : '';
								$cancer_fam_si = ($datos_estudiante['cancer_fam'] == 'SI') ? 'selected' : '';
								?>
								<select name="cancer_fam" class="form-control">
									<option value="NO" <?=$cancer_fam_no?>>NO</option>
									<option value="SI" <?=$cancer_fam_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Hipertensi&oacute;n</label>
								<?php
								$hiper_fam_no = ($datos_estudiante['hiper_fam'] == 'NO') ? 'selected' : '';
								$hiper_fam_si = ($datos_estudiante['hiper_fam'] == 'SI') ? 'selected' : '';
								?>
								<select name="hiper_fam" class="form-control">
									<option value="NO" <?=$hiper_fam_no?>>NO</option>
									<option value="SI" <?=$hiper_fam_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Enf. Cardiovascular</label>
								<?php
								$cardiovascular_fam_no = ($datos_estudiante['cardiovascular_fam'] == 'NO') ? 'selected' : '';
								$cardiovascular_fam_si = ($datos_estudiante['cardiovascular_fam'] == 'SI') ? 'selected' : '';
								?>
								<select name="cardiovascular_fam" class="form-control">
									<option value="NO" <?=$cardiovascular_fam_no?>>NO</option>
									<option value="SI" <?=$cardiovascular_fam_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">Otros</label>
								<input type="text" name="otros_fam" class="form-control" value="<?=$datos_estudiante['otros_fam']?>">
							</div>
							<div class="col-lg-4 form-group">
								<label class="font-weight-bold">¿El estudiante ha sido diagnosticado con covid-19?</label>
								<?php
								$covid_no = ($datos_estudiante['covid'] == 'NO') ? 'selected' : '';
								$covid_si = ($datos_estudiante['covid'] == 'SI') ? 'selected' : '';
								?>
								<select name="covid" class="form-control">
									<option value="NO" <?=$covid_no?>>NO</option>
									<option value="SI" <?=$covid_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold">¿Algún miembro del núcleo familiar del estudiante ha sido diagnosticado con covid-19?</label>
								<?php
								$covid_diag_no = ($datos_estudiante['covid_diag'] == 'NO') ? 'selected' : '';
								$covid_diag_si = ($datos_estudiante['covid_diag'] == 'SI') ? 'selected' : '';
								?>
								<select name="covid_diag" class="form-control">
									<option value="NO" <?=$covid_diag_no?>>NO</option>
									<option value="SI" <?=$covid_diag_si?>>SI</option>
								</select>
							</div>
							<div class="col-lg-6">
								<label class="font-weight-bold">Etnia</label>
								<input type="text" class="form-control" name="etnia" value="<?=$datos_estudiante['etnia']?>">
							</div>
							<div class="form-group col-lg-12 mt-4">
								<button class="btn btn-secondary btn-sm float-left" type="button">
									<i class="fa fa-print"></i>
									&nbsp;
									Imprimir
								</button>
								<button class="btn btn-success btn-sm float-right" type="submit">
									<i class="fa fa-save"></i>
									&nbsp;
									Guardar cambios
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="observacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-primary font-weight-bold">Enviar observacion</h5>
			</div>
			<form method="POST">
				<input type="hidden" name="id_estudiante" value="<?=$id_estudiante?>">
				<input type="hidden" name="id_log" value="<?=$id_log?>">
				<input type="hidden" name="nombre_completo" value="<?=$datos_estudiante['nombre'] . ' ' . $datos_estudiante['apellido']?>">
				<div class="modal-body border-0">
					<div class="row p-2">
						<div class="col-lg-12 form-group">
							<label class="font-weight-bold">Correo destino <span class="text-danger">*</span></label>
							<input type="text" name="correo_destino" class="form-control" value="<?=$datos_estudiante['correo']?>" required>
						</div>
						<div class="col-lg-12 form-group">
							<label class="font-weight-bold">Asunto <span class="text-danger">*</span></label>
							<select name="asunto" class="form-control" required>
								<option value="" selected>Seleccione una opcion...</option>
								<option value="Fotos">Fotos</option>
								<option value="Informacion del estudiante">Informaci&oacute;n del estudiante</option>
								<option value="Informacion del familiar">Informaci&oacute;n del familiar</option>
								<option value="Datos medicos">Datos medicos</option>
							</select>
						</div>
						<div class="col-lg-12 form-group">
							<label class="font-weight-bold">Mensaje <span class="text-danger">*</span></label>
							<textarea class="form-control" name="mensaje" maxlength="1500" required></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
						<i class="fa fa-times"></i>
						&nbsp;
						Cerrar
					</button>
					<button type="submit" class="btn btn-success btn-sm">
						<i class="fas fa-paper-plane"></i>
						&nbsp;
						Enviar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_estudiante'])) {
	$instancia->editarInformacionEstudianteControl();
}

if (isset($_POST['correo_destino'])) {
	$instancia->enviarObservacionControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/fileInput.js"></script>
