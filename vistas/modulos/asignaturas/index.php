<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';
require_once CONTROL_PATH . 'cursos' . DS . 'ControlCurso.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuario.php';

$instancia          = ControlAreas::singleton_areas();
$instancia_curso    = ControlCursos::singleton_cursos();
$instancia_usuarios = ControlUsuario::singleton_usuario();

$cursos_completos      = $instancia_curso->mostrarTodosCursosControl();
$datos_usuarios        = $instancia_usuarios->mostrarTodosUsuariosControl();
$asignaturas_completos = $instancia->mostrarTodosAsignaturasControl();
$areas_completos       = $instancia->mostrarTodosAreasControl();

$permisos = $instancia_permiso->permisosUsuarioControl(1, 8, 1, $id_perfil);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						<a href="<?=BASE_URL?>inicio" class="text-decoration-none">
							<i class="fa fa-arrow-left text-primary"></i>
						</a>
						&nbsp;
						Asignaturas
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
							<div class="dropdown-header">Acciones:</div>
							<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_asignatura">Agregar asignaturas</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8 form-inline">
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar">
									<div class="input-group-prepend">
										<span class="input-group-text rounded-right" id="basic-addon1">
											<i class="fa fa-search"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No.</th>
									<th scope="col">Nombre</th>
									<th scope="col">Area</th>
								</tr>
							</thead>
							<tbody class="buscar text-uppercase">
								<?php
								foreach ($asignaturas_completos as $asignaturas) {
									$id_asignatura = $asignaturas['id'];
									$nombre        = $asignaturas['nombre'];
									$area          = $asignaturas['nom_area'];
									$activo        = $asignaturas['activo'];

									if ($activo == 0) {
										$icon  = '<i class="fa fa-check"></i>';
										$title = 'Activar';
										$clase = 'btn-success activar_asignatura';
									} else {
										$icon  = '<i class="fa fa-times"></i>';
										$title = 'Inactivar';
										$clase = 'btn-danger inactivar_asignatura';
									}

									$listado = $instancia->listadoAsignaturasControl($id_asignatura);

									?>
									<tr class="text-center">
										<td><?=$id_asignatura?></td>
										<td><?=$nombre?></td>
										<td><?=$area?></td>
										<td>
											<div class="btn-group btn-group-sm" role="group">
												<button class="btn btn-success btn-sm" data-tooltip="tooltip" title="Asignar curso" data-placement="bottom" data-toggle="modal" data-target="#asignar<?=$id_asignatura?>">
													<i class="fa fa-plus"></i>
												</button>
												<button class="btn btn-info btn-sm" data-tooltip="tooltip" title="Ver asignaciones" data-placement="bottom" data-toggle="modal" data-target="#listado<?=$id_asignatura?>">
													<i class="fa fa-eye"></i>
												</button>
												<button class="btn btn-sm <?=$clase?>" id="<?=$id_asignatura?>" data-log="<?=$id_log?>" data-tooltip="tooltip" data-placement="bottom" title="<?=$title?>" >
													<?=$icon?>
												</button>
											</div>
										</td>
									</tr>


									<!-- Modal -->
									<div class="modal fade" id="asignar<?=$id_asignatura?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-primary" id="exampleModalLabel">Academic</h5>
												</div>
												<form method="POST">
													<input type="hidden" name="id_log" value="<?=$id_log?>">
													<input type="hidden" name="id_asignatura" value="<?=$id_asignatura?>">
													<div class="modal-body">
														<div class="row p-2">
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Curso <span class="text-danger">*</span></label>
																<select class="form-control" name="curso" required>
																	<option value="" selected>Seleccione una opcion...</option>
																	<?php
																	foreach ($cursos_completos as $curso) {
																		$id_curso  = $curso['id'];
																		$nom_curso = $curso['nombre'];
																		$activo    = $curso['activo'];

																		$ver = ($activo == 0) ? 'd-none' : '';
																		?>
																		<option value="<?=$id_curso?>" class="<?=$ver?>"><?=$nom_curso?></option>
																		<?php
																	}
																	?>
																</select>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Profesor <span class="text-danger">*</span></label>
																<select name="profesor" class="form-control">
																	<option value="" selected>Seleccione una opcion...</option>
																	<?php
																	foreach ($datos_usuarios as $usuario) {
																		$id_user  = $usuario['id_user'];
																		$nom_prof = $usuario['nombre'] . ' ' . $usuario['apellido'];
																		$perfil   = $usuario['perfil'];
																		$activo   = $usuario['activo'];

																		$ver = ($activo == 1 && $perfil == 3) ? '' : 'd-none';
																		?>
																		<option value="<?=$id_user?>" class="<?=$ver?>"><?=$nom_prof?></option>
																		<?php
																	}
																	?>
																</select>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Horas a la semana <span class="text-danger">*</span></label>
																<input type="text" class="form-control numeros" name="horas" required>
															</div>
														</div>
													</div>
													<div class="modal-footer border-0">
														<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
															<i class="fa fa-times"></i>
															&nbsp;
															Cerrar
														</button>
														<button type="submit" class="btn btn-success btn-sm">
															<i class="fa fa-save"></i>
															&nbsp;
															Guardar
														</button>
													</div>
												</form>
											</div>
										</div>
									</div>



									<!-- Modal -->
									<div class="modal fade" id="listado<?=$id_asignatura?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">

												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
													<button type="button" class="btn btn-primary">Save changes</button>
												</div>
											</div>
										</div>
									</div>

									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'asignaturas' . DS . 'agregarAsignatura.php';

if (isset($_POST['area'])) {
	$instancia->registrarAsignaturaControl();
}

if (isset($_POST['curso'])) {
	$instancia->asignarCursoControl();
}
?>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/asignaturas/funcionesAsignatura.js"></script>