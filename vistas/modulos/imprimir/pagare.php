<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'fpdf' . DS . 'fpdf.php';
require_once CONTROL_PATH . 'estudiante' . DS . 'ControlEstudiante.php';

$instancia = ControlEstudiante::singleton_estudiante();

if (isset($_GET['prematricula'])) {
    $id_prematricula = base64_decode($_GET['prematricula']);

    $datos_prematricula = $instancia->mostrarDatosPrematriculaControl($id_prematricula);

    class PDF extends FPDF
    {
        const LINE_PRECISION = 5;
        const DEBUG_PDF      = 0;
        const BORDER         = 0;

        public function reducirT($tam, $alt, $string, $tamanio, $salto, $ali)
        {
            if ($string == "--" && !PDF::DEBUG_PDF) {
                return;
            }
            if (!isset($string)) {
                $string = "";
            }
            $tamanio = $tamanio * 9;

            $this->SetFont('Arial', '', $tamanio); // Set the new font size

            while ($this->GetStringWidth(utf8_decode($string)) > $tam * 1.1) {
                $tamanio--; // Decrease the variable which holds the font size
                $this->SetFont('Arial', '', $tamanio); // Set the new font size
            }
            $this->Cell($tam, $alt, trim(utf8_decode($string)), PDF::BORDER, $salto, $ali);

            if (!isset($string)) {
                $string = "";
            }
        }

        private function grilla()
        {
            $this->SetDrawColor(255, 0, 0);
            if (PDF::DEBUG_PDF) {
                for ($i = 0; $i < 300; $i += PDF::LINE_PRECISION) {
                    $this->setXY(0, $i);
                    $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                    $this->SetLineWidth(0.005);
                    $this->Line(0, $i, 500, $i);
                }
                for ($i = 0; $i < 2000; $i += PDF::LINE_PRECISION) {
                    $this->setXY($i, 0);
                    $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                    $this->SetLineWidth(0.005);
                    $this->Line($i, 0, $i, 2000);
                }
            }
            $this->SetDrawColor(0, 0, 255);
        }

        public function generar($datos)
        {

            $nombre_padre      = $datos['nombre_padre'];
            $documento_padre   = $datos['documento_padre'];
            $nombre_madre      = $datos['nombre_madre'];
            $documento_madre   = $datos['documento_madre'];
            $nombre_estudiante = $datos['nombre_estudiante'];
            $curso             = $datos['curso'];
            $lectivo_inicio    = $datos['anio_lectivo_i'];
            $lectivo_fin       = $datos['anio_lectivo_f'];
            $direccion         = $datos['direccion'];
            $telefono          = $datos['telefono'];
            $expedido_padre    = $datos['expedido_padre'];
            $expedido_madre    = $datos['expedido_madre'];

            $this->SetAutoPageBreak(false);
            $this->AddPage();
            $this->pagina1($nombre_padre, $documento_padre, $nombre_madre, $documento_madre, $nombre_estudiante, $curso, $lectivo_inicio, $lectivo_fin, $direccion, $telefono, $expedido_madre, $expedido_padre);
        }

        private function pagina1($nombre_padre, $documento_padre, $nombre_madre, $documento_madre, $nombre_estudiante, $curso, $lectivo_inicio, $lectivo_fin, $direccion, $telefono, $expedido_madre, $expedido_padre)
        {
            $this->Image(PUBLIC_PATH . 'img/pdfs/Pagare_page-0001.jpg', '0', '0', '210', '297', 'JPG');
            $this->SetFont('Arial', '', 8);
            $this->grilla();

            // NOMBRE PADRE
            $this->SetXY(25, 71);
            $this->reducirT(48, 3.5, $nombre_padre, 1, 0, 'C');

            // NOMBRE MADRE
            $this->SetXY(77, 71);
            $this->reducirT(48, 3.5, $nombre_madre, 1, 0, 'C');

            // ESTUDIANTE
            $this->SetXY(65, 77.5);
            $this->reducirT(48, 3.5, $nombre_estudiante, 1, 0, 'C');

            // DIRECCION
            $this->SetXY(124, 74);
            $this->reducirT(25, 3.5, $direccion, 1, 0, 'C');

            // TELEFONO
            $this->SetXY(164, 74);
            $this->reducirT(27, 3.5, $telefono, 1, 0, 'C');

            // CURSO
            $this->SetXY(28, 80.5);
            $this->reducirT(32, 3.5, $curso, 1, 0, 'C');

            // ANIO LECTIVO
            $this->SetXY(19, 87);
            $this->reducirT(10, 3.5, $lectivo_inicio, 1, 0, 'C');

            // ANIO LECTIVO 2
            $this->SetXY(33, 87);
            $this->reducirT(12, 3.5, $lectivo_fin, 1, 0, 'C');

            // PAGARE
            $this->SetXY(110, 164);
            $this->reducirT(15, 3.5, '', 1, 0, 'C');

            // NOMBRE DEL PADRE
            $this->SetXY(20, 179.5);
            $this->reducirT(52, 3.5, $nombre_padre, 1, 0, 'C');

            // NOMBRE DE LA MADRE
            $this->SetXY(80, 179.5);
            $this->reducirT(52, 3.5, $nombre_madre, 1, 0, 'C');

            // CEDULA PADRE
            $this->SetXY(83, 183);
            $this->reducirT(25, 3.5, $documento_padre, 1, 0, 'C');

            // EXPEDIDA EN
            $this->SetXY(126, 182.8);
            $this->reducirT(23, 3.5, $expedido_padre, 1, 0, 'C');

            // CEDULA MADRE
            $this->SetXY(157, 182.5);
            $this->reducirT(20, 3.5, $documento_madre, 1, 0, 'C');

            // EXPEDIDA EN
            $this->SetXY(15, 185.8);
            $this->reducirT(25, 3.5, $expedido_madre, 1, 0, 'C');

            // ALUMNO
            $this->SetXY(138, 186.1);
            $this->reducirT(53, 3, $nombre_estudiante, 1, 0, 'C');

            // SUMA TOTAL DE (LETRAS)
            $this->SetXY(19, 195);
            $this->reducirT(85, 3.5, '', 1, 0, 'C');

            // SUMA TOTAL DE (NUMEROS)
            $this->SetXY(110, 195);
            $this->reducirT(23, 3.5, '', 1, 0, 'C');

            // DIA DEL MES
            $this->SetXY(24.5, 198.5);
            $this->reducirT(7, 3, '', 1, 0, 'C');

            // MES
            $this->SetXY(49, 198.5);
            $this->reducirT(14, 3, '', 1, 0, 'C');

            // ANIO
            $this->SetXY(75, 198.5);
            $this->reducirT(18, 3, '', 1, 0, 'C');

            // VENCE DIA
            $this->SetXY(127, 198.5);
            $this->reducirT(8, 3, '', 1, 0, 'C');

            // VENCE MES
            $this->SetXY(153, 198.5);
            $this->reducirT(15, 3, '', 1, 0, 'C');

            // VENCE ANIO
            $this->SetXY(180, 198.5);
            $this->reducirT(15, 3, '', 1, 0, 'C');

            // CUOTAS (LETRAS)
            $this->SetXY(65, 201.5);
            $this->reducirT(78, 3, '', 1, 0, 'C');

            // CUOTAS (NUMEROS)
            $this->SetXY(148, 201.5);
            $this->reducirT(20, 3, '', 1, 0, 'C');

            // DIA  (FINAL)
            $this->SetXY(117, 222.5);
            $this->reducirT(8, 3.5, '', 1, 0, 'C');

            // MES  (FINAL)
            $this->SetXY(143, 222.5);
            $this->reducirT(18, 3.5, '', 1, 0, 'C');

            // ANIO  (FINAL)
            $this->SetXY(173, 222.5);
            $this->reducirT(18, 3.5, '', 1, 0, 'C');
        }
    }

    $pdf = new PDF();
    $pdf->SetFont('Arial', '', 8);
    $pdf->SetTitle("Pagare matricula", true);
    $pdf->generar($datos_prematricula);
    $pdf->Output('I', 'Pagare.pdf');
    $pdf->Output('F', PUBLIC_PATH_ARCH . 'upload' . DS . 'pagare_' . md5($datos_prematricula['prematricula']) . '.pdf');
}
