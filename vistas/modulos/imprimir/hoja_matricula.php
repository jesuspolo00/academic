<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'fpdf' . DS . 'fpdf.php';
require_once CONTROL_PATH . 'estudiante' . DS . 'ControlEstudiante.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlEstudiante::singleton_estudiante();

if (isset($_GET['prematricula'])) {
    $id_prematricula = base64_decode($_GET['prematricula']);

    $datos_prematricula = $instancia->mostrarDatosPrematriculaControl($id_prematricula);

    class PDF extends FPDF
    {
        const LINE_PRECISION = 5;
        const DEBUG_PDF      = 0;
        const BORDER         = 0;

        public function reducirT($tam, $alt, $string, $tamanio, $salto, $ali)
        {
            if ($string == "--" && !PDF::DEBUG_PDF) {
                return;
            }
            if (!isset($string)) {
                $string = "";
            }
            $tamanio = $tamanio * 9;

            $this->SetFont('Arial', '', $tamanio); // Set the new font size

            while ($this->GetStringWidth(utf8_decode($string)) > $tam * 1.1) {
                $tamanio--; // Decrease the variable which holds the font size
                $this->SetFont('Arial', '', $tamanio); // Set the new font size
            }
            $this->Cell($tam, $alt, trim(utf8_decode($string)), PDF::BORDER, $salto, $ali);

            if (!isset($string)) {
                $string = "";
            }
        }

        public function reducirMultiT($tam, $alt, $string, $tamanio, $ali)
        {
            if ($string == "--" && !PDF::DEBUG_PDF) {
                return;
            }
            if (!isset($string)) {
                $string = "";
            }
            $tamanio = $tamanio * 10;

            $this->SetFont('Arial', '', $tamanio); // Set the new font size

            while ($this->GetStringWidth(utf8_decode($string)) > $tam * 1.1) {
                $tamanio--; // Decrease the variable which holds the font size
                $this->SetFont('Arial', '', $tamanio); // Set the new font size
            }
            $this->MultiCell($tam, $alt, trim(utf8_decode($string)), PDF::BORDER, $ali);

            if (!isset($string)) {
                $string = "";
            }
        }

        private function grilla()
        {
            $this->SetDrawColor(255, 0, 0);
            if (PDF::DEBUG_PDF) {
                for ($i = 0; $i < 300; $i += PDF::LINE_PRECISION) {
                    $this->setXY(0, $i);
                    $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                    $this->SetLineWidth(0.005);
                    $this->Line(0, $i, 500, $i);
                }
                for ($i = 0; $i < 2000; $i += PDF::LINE_PRECISION) {
                    $this->setXY($i, 0);
                    $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                    $this->SetLineWidth(0.005);
                    $this->Line($i, 0, $i, 2000);
                }
            }
            $this->SetDrawColor(0, 0, 255);
        }

        public function generar($datos)
        {
            $prematricula      = $datos['prematricula'];
            $nombre_padre      = $datos['nombre_padre'];
            $documento_padre   = $datos['documento_padre'];
            $nombre_madre      = $datos['nombre_madre'];
            $documento_madre   = $datos['documento_madre'];
            $nombre_estudiante = $datos['nombre_estudiante'];
            $curso             = $datos['curso'];
            $lectivo_inicio    = $datos['anio_lectivo_i'];
            $lectivo_fin       = $datos['anio_lectivo_f'];
            $direccion         = $datos['direccion'];
            $telefono          = $datos['telefono'];
            $expedido_padre    = $datos['expedido_padre'];
            $expedido_madre    = $datos['expedido_madre'];
            $fecha_nacimiento  = $datos['fecha_nacimiento'];

            $this->SetAutoPageBreak(false);
            $this->AddPage();
            $this->pagina1($prematricula, $nombre_estudiante, $fecha_nacimiento, $curso, $nombre_padre, $nombre_madre);
        }

        private function pagina1($prematricula, $nombre_estudiante, $fecha_nacimiento, $curso, $nombre_padre, $nombre_madre)
        {
            $this->Image(PUBLIC_PATH . 'img/pdfs/hoja_matricula_page-0001.jpg', '0', '0', '210', '297', 'JPG');
            $this->SetFont('Arial', '', 8);
            $this->grilla();

            // NUMERO DE MATRICULA
            $this->SetXY(162, 70);
            $this->reducirT(30, 4, $prematricula, 1, 0, 'L');

            // LUGAR Y FECHA
            $this->SetXY(57, 75);
            $this->reducirT(30, 4, date('Y-m-d'), 1, 0, 'L');

            // ESTUDIANTE
            $this->SetXY(52, 83);
            $this->reducirT(50, 4, $nombre_estudiante, 1, 0, 'L');

            // LUAGR Y FECHA DE NACIMIENTO
            $this->SetXY(57, 90.5);
            $this->reducirT(50, 4, $fecha_nacimiento, 1, 0, 'L');

            // EDAD
            $this->SetXY(26, 98);
            $this->reducirT(30, 4, calculaedad($fecha_nacimiento) . ' Años', 1, 0, 'L');

            // SE MATRICULA
            $this->SetXY(40, 106);
            $this->reducirT(30, 4, $curso, 1, 0, 'L');

            // NOMBRE PADRE
            $this->SetXY(17, 141);
            $this->reducirT(60, 4, $nombre_padre, 1, 0, 'L');

            // NOMBRE MADRE
            $this->SetXY(17, 145.5);
            $this->reducirT(60, 4, $nombre_madre, 1, 0, 'L');

            // OBSERVACION
            $this->SetXY(17, 234);
            $this->reducirMultiT(178, 7, '', 1, 'L');
        }
    }

    $pdf = new PDF();
    $pdf->SetFont('Arial', '', 8);
    $pdf->SetTitle("Hoja matricula", true);
    $pdf->generar($datos_prematricula);
    $pdf->Output('I', 'Hoja_matricula.pdf');
    $pdf->Output('F', PUBLIC_PATH_ARCH . 'upload' . DS . 'hoja_matricula_' . md5($datos_prematricula['prematricula']) . '.pdf');

}
