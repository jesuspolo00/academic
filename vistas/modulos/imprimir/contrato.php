<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'fpdf' . DS . 'fpdf.php';
require_once CONTROL_PATH . 'estudiante' . DS . 'ControlEstudiante.php';

$instancia = ControlEstudiante::singleton_estudiante();

if (isset($_GET['prematricula'])) {
    $id_prematricula = base64_decode($_GET['prematricula']);

    $datos_prematricula = $instancia->mostrarDatosPrematriculaControl($id_prematricula);

    class PDF extends FPDF
    {
        const LINE_PRECISION = 5;
        const DEBUG_PDF      = 0;
        const BORDER         = 0;

        public function reducirT($tam, $alt, $string, $tamanio, $salto, $ali)
        {
            if ($string == "--" && !PDF::DEBUG_PDF) {
                return;
            }
            if (!isset($string)) {
                $string = "";
            }
            $tamanio = $tamanio * 9;

            $this->SetFont('Arial', '', $tamanio); // Set the new font size

            while ($this->GetStringWidth(utf8_decode($string)) > $tam * 1.1) {
                $tamanio--; // Decrease the variable which holds the font size
                $this->SetFont('Arial', '', $tamanio); // Set the new font size
            }
            $this->Cell($tam, $alt, trim(utf8_decode($string)), PDF::BORDER, $salto, $ali);

            if (!isset($string)) {
                $string = "";
            }
        }

        public function grilla()
        {
            $this->SetDrawColor(255, 0, 0);
            if (PDF::DEBUG_PDF) {
                for ($i = 0; $i < 300; $i += PDF::LINE_PRECISION) {
                    $this->setXY(0, $i);
                    $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                    $this->SetLineWidth(0.005);
                    $this->Line(0, $i, 500, $i);
                }
                for ($i = 0; $i < 2000; $i += PDF::LINE_PRECISION) {
                    $this->setXY($i, 0);
                    $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                    $this->SetLineWidth(0.005);
                    $this->Line($i, 0, $i, 2000);
                }
            }
            $this->SetDrawColor(0, 0, 255);
        }

        public function generar($datos)
        {

            $nombre_padre      = $datos['nombre_padre'];
            $documento_padre   = $datos['documento_padre'];
            $nombre_madre      = $datos['nombre_madre'];
            $documento_madre   = $datos['documento_madre'];
            $nombre_estudiante = $datos['nombre_estudiante'];
            $curso             = $datos['curso'];
            $lectivo_inicio    = $datos['anio_lectivo_i'];
            $lectivo_fin       = $datos['anio_lectivo_f'];

            $this->SetAutoPageBreak(false);
            $this->AddPage();
            $this->pagina1($nombre_padre, $documento_padre, $nombre_madre, $documento_madre, $nombre_estudiante, $curso, $lectivo_inicio, $lectivo_fin);
        }

        public function pagina1($nombre_padre, $documento_padre, $nombre_madre, $documento_madre, $nombre_estudiante, $curso, $lectivo_inicio, $lectivo_fin)
        {
            $this->Image(PUBLIC_PATH . 'img/pdfs/contrato_page-0001.jpg', '0', '0', '210', '297', 'JPG');
            $this->SetFont('Arial', '', 8);
            $this->grilla();

            // NOMBRE PADRE
            $this->SetXY(62, 70);
            $this->reducirT(65, 3.5, $nombre_padre, 1, 0, 'C');

            // NOMBRE MADRE
            $this->SetXY(133, 70);
            $this->reducirT(48, 3.5, $nombre_madre, 1, 0, 'C');

            // NOMBRE ESTUDIANTE
            $this->SetXY(137, 73.5);
            $this->reducirT(58, 3.5, $nombre_estudiante, 1, 0, 'C');

            // GRADO
            $this->SetXY(60, 76.8);
            $this->reducirT(30, 3.5, $curso, 1, 0, 'C');

            // AÑO LECTIVO
            $this->SetXY(143, 77.5);
            $this->reducirT(5, 3.5, $lectivo_inicio, 1, 0, 'C');

            // AÑO LECTIVO 2
            $this->SetXY(165, 77.5);
            $this->reducirT(5, 3.5, $lectivo_fin, 1, 0, 'C');

            // GRADO
            $this->SetXY(22, 108.2);
            $this->reducirT(20, 3.5, $curso, 1, 0, 'C');

            // COSTO DEL CONTRATO
            $this->SetXY(57, 160.6);
            $this->reducirT(22, 3.5, '', 1, 0, 'C');

            // COSTO DE MATRICULA
            $this->SetXY(49, 163.6);
            $this->reducirT(20, 3.5, '', 1, 0, 'C');

            // COSTO DE SALDO
            $this->SetXY(86, 163.6);
            $this->reducirT(19, 3.5, '', 1, 0, 'C');

            // AÑO LECTIVO
            $this->SetXY(115, 160.6);
            $this->reducirT(5, 4, $lectivo_inicio, 1, 0, 'C');

            // AÑO LECTIVO 2
            $this->SetXY(153.5, 160.6);
            $this->reducirT(5, 4, $lectivo_fin, 1, 0, 'C');

            // AÑO LECTIVO
            $this->SetXY(143, 210.5);
            $this->reducirT(5, 4, $lectivo_inicio, 1, 0, 'C');

            // AÑO LECTIVO 2
            $this->SetXY(165, 210.5);
            $this->reducirT(5, 4, $lectivo_fin, 1, 0, 'C');

            // DIAS
            $this->SetXY(106.5, 235.5);
            $this->reducirT(5, 3.5, '', 1, 0, 'C');

            // MES
            $this->SetXY(124.5, 235.5);
            $this->reducirT(5, 3.5, '', 1, 0, 'C');

            // ANIO
            $this->SetXY(142.5, 235.8);
            $this->reducirT(5, 3.5, $lectivo_inicio, 1, 0, 'C');
        }
    }

    $pdf = new PDF();
    $pdf->SetFont('Arial', '', 8);
    $pdf->SetTitle("Contrato matricula", true);
    $pdf->generar($datos_prematricula);
    $pdf->Output('I', 'contrato.pdf');
    $pdf->Output('F', PUBLIC_PATH_ARCH . 'upload' . DS . 'contrato_' . md5($datos_prematricula['prematricula']) . '.pdf');
}
