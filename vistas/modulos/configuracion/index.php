<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:login?er=' . $error);
	exit();
}

include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';

$permisos = $instancia_permiso->permisosUsuarioControl(1, 1, 1, $id_perfil);

if (!$permisos) {
	include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
	exit();
}
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3">
					<h4 class="m-0 font-weight-bold text-primary">Modulos</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<?php
						$permisos = $instancia_permiso->permisosUsuarioControl(1, 2, 1, $id_perfil);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>usuarios/index">
								<div class="card border-left-success shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Usuarios</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-users fa-2x text-success"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosUsuarioControl(1, 4, 1, $id_perfil);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>estudiante/index">
								<div class="card border-left-purple shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Estudiantes</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-user-graduate fa-2x text-purple"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosUsuarioControl(1, 5, 1, $id_perfil);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>lectivo/index">
								<div class="card border-left-orange shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">A&ntilde;o lectivo</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-school fa-2x text-orange"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosUsuarioControl(1, 6, 1, $id_perfil);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>cursos/index">
								<div class="card border-left-brown shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Cursos</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-graduation-cap fa-2x text-brown"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosUsuarioControl(1, 8, 1, $id_perfil);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>areas/index">
								<div class="card border-left-green-dark shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Areas</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-layer-group fa-2x text-green-dark"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosUsuarioControl(1, 9, 1, $id_perfil);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>asignaturas/index">
								<div class="card border-left-warning shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Asignaturas</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-clipboard-list  fa-2x text-warning"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosUsuarioControl(1, 10, 1, $id_perfil);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>periodos/index">
								<div class="card border-left-danger shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Periodos</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-clock fa-2x text-danger"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosUsuarioControl(1, 11, 1, $id_perfil);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>procesos/index">
								<div class="card border-left-info shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Procesos</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-border-all fa-2x text-info"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosUsuarioControl(1, 12, 1, $id_perfil);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>notas/index">
								<div class="card border-left-pink shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Notas</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-tasks fa-2x text-pink"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosUsuarioControl(1, 3, 1, $id_perfil);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>permisos/index">
								<div class="card border-left-secondary shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Permisos</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-user-lock fa-2x text-secondary"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>