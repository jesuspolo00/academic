<!-- Modal -->
<div class="modal fade" id="agregar_periodo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Agregar Periodo</h5>
      </div>
      <form method="POST">
        <input type="hidden" name="id_log" value="<?=$id_log?>">
        <div class="modal-body">
          <div class="row p-2">
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Descripcion <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="descripcion" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Fecha inicio <span class="text-danger">*</span></label>
              <input type="date" class="form-control" name="fecha_inicio" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Fecha fin <span class="text-danger">*</span></label>
              <input type="date" class="form-control" name="fecha_fin" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Porcentaje <span class="text-danger">*</span></label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">%</span>
                </div>
                <input type="text" class="form-control numeros" name="porcentaje" required>
              </div>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">A&ntilde;o lectivo <span class="text-danger">*</span></label>
              <select name="lectivo" id="" class="form-control" required>
                <option value="" selected>Seleccione una opcion...</option>
                <?php
                foreach ($datos_lectivo as $lectivo) {
                  $id_lectivo = $lectivo['id'];
                  $nombre     = $lectivo['nombre'];
                  $activo     = $lectivo['activo'];

                  $select = ($activo == 1) ? 'selected' : '';

                  ?>
                  <option value="<?=$id_lectivo?>" <?=$select?>><?=$nombre?></option>
                  <?php
                }
                ?>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer border-0">
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
            <i class="fa fa-times"></i>
            &nbsp;
            Cerrar
          </button>
          <button type="submit" class="btn btn-success btn-sm">
            <i class="fa fa-save"></i>
            &nbsp;
            Guardar
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
