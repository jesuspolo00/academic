<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia        = ControlPermisos::singleton_permisos();
$instancia_perfil = ControlPerfil::singleton_perfil();

$perfiles_completos = $instancia_perfil->mostrarTodosPerfilesControl();
$datos_modulos      = $instancia->mostrarModulosControl();

$permisos = $instancia_permiso->permisosUsuarioControl(1, 3, 1, $id_perfil);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						<a href="<?=BASE_URL;?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-primary"></i>
						</a>
						&nbsp;
						Permisos
					</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8 form-inline">
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar">
									<div class="input-group-prepend">
										<span class="input-group-text rounded-right" id="basic-addon1">
											<i class="fa fa-search"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Codigo</th>
									<th scope="col">Perfil</th>
									<th scope="col">Modulos Habilitados</th>
								</tr>
							</thead>
							<tbody class="buscar text-lowercase">
								<?php
								foreach ($perfiles_completos as $perfil) {
									$id_perfil  = $perfil['id_perfil'];
									$nom_perfil = $perfil['nombre'];
									$activo     = $perfil['activo'];

									$modulos_activos = $instancia->modulosActivosPerfilControl($id_perfil);

									$count = ($modulos_activos['modulos'] == '') ? '0' : $modulos_activos['modulos'];

									?>
									<tr class="text-center">
										<td><?=$id_perfil;?></td>
										<td class="text-uppercase"><?=$nom_perfil;?></td>
										<td><?=$count?></td>
										<td>
											<button class="btn btn-success btn-sm" data-tooltip="tooltip" title="Asignar permiso" data-placement="bottom" data-toggle="modal" data-target="#permisos_<?=$id_perfil;?>">
												<i class="fa fa-plus"></i>
											</button>
										</td>
									</tr>

									<!-- Modal -->
									<div class="modal fade" id="permisos_<?=$id_perfil;?>" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-primary" id="exampleModalLabel">Permisos (<?=$nom_perfil;?>) - Modulos</h5>
													<a href="<?=BASE_URL?>permisos/index?pagina=1" class="btn btn-sm" aria-label="Close" data-tooltip="tooltip" title="Cerrar">
														<i class="fa fa-times" aria-hidden="true"></i>
													</a>
												</div>
												<div class="modal-body border-0">
													<div class="row p-2">
														<?php
														foreach ($datos_modulos as $modulo) {
															$id_opcion = $modulo['id'];
															$opcion    = $modulo['nombre'];
															$id_modulo = $modulo['id_modulo'];

															$modulos_activos_perfil = $instancia->modulosIdActivosPerfilControl($id_perfil, $id_opcion);

															if ($modulos_activos_perfil['id'] != "") {
																$activo = 'active';
																$icon   = '<i class="fa fa-times float-right"></i>';
																$class  = 'lista_inactivar';
															} else {
																$activo = '';
																$icon   = '<i class="fa fa-check float-right"></i>';
																$class  = 'lista_permiso';
															}

															?>
															<div class="col-lg-12 mb-2">
																<div class="list-group">
																	<a href="#" class="list-group-item list-group-item-action <?=$class?>  <?=$activo;?>" data-perfil="<?=$id_perfil;?>" data-modulo="<?=$id_modulo;?>" data-user="<?=$id_log;?>" id="<?=$id_opcion;?>" data-nombre="<?=$opcion?>">
																		<?=$opcion;?>
																		<?=$icon;?>
																	</a>
																</div>
															</div>
															<?php
														}
														?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?=PUBLIC_PATH?>/js/permisos/funcionesPermisos.js"></script>