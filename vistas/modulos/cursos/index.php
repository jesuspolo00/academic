<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'cursos' . DS . 'ControlCurso.php';

$instancia = ControlCursos::singleton_cursos();

$cursos_completos = $instancia->mostrarTodosCursosControl();
$datos_niveles    = $instancia->mostrarTodosNivelesControl();

$permisos = $instancia_permiso->permisosUsuarioControl(1, 6, 1, $id_perfil);

if (!$permisos) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?=BASE_URL?>inicio" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Cursos
                    </h4>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
                            <div class="dropdown-header">Acciones:</div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_curso">Agregar Curso</a>
                            <!-- <a class="dropdown-item" href="<?=BASE_URL?>niveles/index?pagina=1">Niveles educativos</a> -->
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 form-inline">
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control filtro" placeholder="Buscar">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text rounded-right" id="basic-addon1">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">No.</th>
                                    <th scope="col">Curso</th>
                                    <th scope="col">Nivel educativo</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-lowercase">
                                <?php
                                foreach ($cursos_completos as $curso) {
                                    $id_curso = $curso['id'];
                                    $nombre   = $curso['nombre'];
                                    $nivel    = $curso['nivel'];

                                    if ($curso['activo'] == 0) {
                                        $icon  = '<i class="fa fa-check"></i>';
                                        $title = 'Activar';
                                        $clase = 'btn-success activar_nivel';
                                    } else {
                                        $icon  = '<i class="fa fa-times"></i>';
                                        $title = 'Inactivar';
                                        $clase = 'btn-danger inactivar_nivel';
                                    }

                                    $listado = $instancia->mostrarListadoAsignaturasControl($id_curso);

                                    ?>
                                    <tr class="text-center text-uppercase">
                                        <td><?=$id_curso?></td>
                                        <td><?=$nombre?></td>
                                        <td><?=$nivel?></td>
                                        <td>
                                            <div class="btn-group btn-group-sm" role="group">
                                                <button class="btn btn-sm btn-primary" data-tooltip="tooltip" title="Editar" data-placement="bottom">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button class="btn btn-info btn-sm" data-tooltip="tooltip" title="Ver detalles" data-placement="bottom" data-toggle="modal" data-target="#listado<?=$id_curso?>">
                                                    <i class="fa fa-eye"></i>
                                                </button>
                                                <button class="btn btn-sm <?=$clase?>" data-tooltip="tooltip" title="<?=$title?>" data-placement="bottom" id="<?=$id_curso?>" data-user="<?=$id_log?>">
                                                    <?=$icon?>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>


                                    <!-- Modal -->
                                    <div class="modal fade" id="listado<?=$id_curso?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Listado de asignaciones (<?=$nivel?> - <?=$nombre?>)</h5>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <?php
                                                        foreach ($listado as $datos) {
                                                            $id_listado    = $datos['id'];
                                                            $nombre        = $datos['nom_curso'];
                                                            $id_asignatura = $datos['id_asignatura'];
                                                            $horas         = $datos['horas'];
                                                            ?>
                                                            <div class="col-lg-12 mb-2 listado<?=$id_listado?>">
                                                                <div class="list-group">
                                                                    <a href="#" class="list-group-item list-group-item-action curso_asignado" id="<?=$id_listado?>" data-curso="<?=$id_curso?>" data-asignatura="<?=$id_asignatura?>">
                                                                        <?=$nombre?> (<?=$horas?> Horas)
                                                                        <i class="fa fa-times float-right mt-1"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'cursos' . DS . ' agregarCurso.php';

if (isset($_POST['nombre'])) {
    $instancia->agregarCursoControl();
}
?>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/asignaturas/funcionesAsignatura.js"></script>