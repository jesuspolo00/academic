<!-- Agregar usuario -->
<div class="modal fade" id="agregar_curso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Agregar Curso</h5>
      </div>
      <form method="POST" id="registro">
        <input type="hidden" name="id_log" value="<?=$id_log?>">
        <div class="modal-body border-0 p-3">
          <div class="row p-3">
            <div class="col-lg-12 form-group">
              <label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="nombre" minlength="1" maxlength="50" required>
            </div>
            <div class="col-lg-12 form-group">
              <label class="font-weight-bold">Nivel educativo <span class="text-danger">*</span></label>
              <select class="form-control" name="nivel" required>
                <option value="" selected>Seleccione una opcion...</option>
                <?php
                foreach ($datos_niveles as $nivel) {
                  $id_nivel    = $nivel['id'];
                  $nombre      = $nivel['nombre'];
                  $abreviacion = $nivel['abreviatura'];
                  ?>
                  <option value="<?=$id_nivel?>"><?=$nombre?></option>
                  <?php
                }
                ?>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer border-0">
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
            <i class="fa fa-times"></i>
            &nbsp;
            Cerrar
          </button>
          <button type="submit" class="btn btn-success btn-sm">
            <i class="fa fa-save"></i>
            &nbsp;
            Registrar
          </button>
        </div>
      </form>
    </div>
  </div>
</div>