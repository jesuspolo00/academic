<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlAreas::singleton_areas();

$asignaturas_completos = $instancia->mostrarTodosAsignaturasControl();
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						<a href="<?=BASE_URL?>inicio" class="text-decoration-none">
							<i class="fa fa-arrow-left text-primary"></i>
						</a>
						&nbsp;
						Notas
					</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8 form-inline">
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar">
									<div class="input-group-prepend">
										<span class="input-group-text rounded-right" id="basic-addon1">
											<i class="fa fa-search"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No.</th>
									<th scope="col">Asignatura</th>
									<th scope="col">Area</th>
								</tr>
							</thead>
							<tbody class="buscar text-uppercase">
								<?php
								foreach ($asignaturas_completos as $asignaturas) {
									$id_asignatura = $asignaturas['id'];
									$nombre        = $asignaturas['nombre'];
									$area          = $asignaturas['nom_area'];
									$activo        = $asignaturas['activo'];

									$ver = ($activo == 0) ? 'd-none' : '';
									?>
									<tr class="text-center <?=$ver?>">
										<td><?=$id_asignatura?></td>
										<td><?=$nombre?></td>
										<td><?=$area?></td>
										<td>
											<div class="btn-group btn-group-sm" role="group">
												<a href="<?=BASE_URL?>notas/cursos?asignatura=<?=base64_encode($id_asignatura)?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Ver listado de cursos" data-placement="bottom">
													<i class="fa fa-eye"></i>
												</a>
											</div>
										</td>
									</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>