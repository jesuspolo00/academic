<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia = ControlAreas::singleton_areas();

if (isset($_GET['asignatura'])) {

	$id_asignatura    = base64_decode($_GET['asignatura']);
	$datos_cursos     = $instancia->listadoAsignaturasControl($id_asignatura);
	$datos_asignatura = $instancia->mostrarDatosAsignaturaControl($id_asignatura);

	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-primary">
							<a href="<?=BASE_URL?>notas/index" class="text-decoration-none">
								<i class="fa fa-arrow-left text-primary"></i>
							</a>
							&nbsp;
							Notas - asignatura (<?=$datos_asignatura['nombre']?>)
						</h4>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-8 form-inline">
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<div class="input-group mb-3">
										<input type="text" class="form-control filtro" placeholder="Buscar">
										<div class="input-group-prepend">
											<span class="input-group-text rounded-right" id="basic-addon1">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="table-responsive mt-2">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center font-weight-bold">
										<th scope="col">Curso</th>
										<th scope="col">Profesor</th>
									</tr>
								</thead>
								<tbody class="buscar text-uppercase">
									<?php
									foreach ($datos_cursos as $curso) {
										$id_asignacion = $curso['id'];
										$nom_curso     = $curso['nom_curso'];
										$profesor      = $curso['nom_profesor'];
										?>
										<tr class="text-center">
											<td><?=$nom_curso?></td>
											<td><?=$profesor?></td>
											<td>
												<a href="<?=BASE_URL?>notas/estudiantes?curso=<?=base64_encode($curso['id_curso'])?>" class="btn btn-primary btn-sm" data-tooltip="tooltip" title="Listado de estudiantes" data-placement="bottom">
													<i class="fas fa-clipboard-list"></i>
												</a>
											</td>
										</tr>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';
}
?>