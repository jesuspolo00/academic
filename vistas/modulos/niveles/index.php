<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'cursos' . DS . 'ControlCurso.php';

$instancia = ControlCursos::singleton_cursos();

$niveles_completos = $instancia->mostrarTodosNivelesControl();

$permisos = $instancia_permiso->permisosUsuarioControl(1, 7, 1, $id_perfil);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						<a href="<?=BASE_URL?>inicio" class="text-decoration-none">
							<i class="fa fa-arrow-left text-primary"></i>
						</a>
						&nbsp;
						Niveles Educativos
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
							<div class="dropdown-header">Acciones:</div>
							<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_nivel">Agregar Nivel</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8 form-inline">
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar">
									<div class="input-group-prepend">
										<span class="input-group-text rounded-right" id="basic-addon1">
											<i class="fa fa-search"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive mt-2">
						<table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No.</th>
									<th scope="col">Nombre</th>
									<th scope="col">Abreviatura</th>
								</tr>
							</thead>
							<tbody class="buscar text-lowercase">
								<?php
								foreach ($niveles_completos as $nivel) {
									$id_nivel = $nivel['id'];
									$nombre   = $nivel['nombre'];
									$abrev    = $nivel['abreviatura'];

									if ($nivel['activo'] == 0) {
										$icon  = '<i class="fa fa-check"></i>';
										$title = 'Activar';
										$clase = 'btn-success activar_nivel';
									} else {
										$icon  = '<i class="fa fa-times"></i>';
										$title = 'Inactivar';
										$clase = 'btn-danger inactivar_nivel';
									}

									?>
									<tr class="text-center">
										<td><?=$id_nivel?></td>
										<td><?=$nombre?></td>
										<td class="text-uppercase"><?=$abrev?></td>
										<td>
											<button class="btn btn-sm btn-primary" data-tooltip="tooltip" title="Editar" data-placement="bottom">
												<i class="fa fa-edit"></i>
											</button>
										</td>
										<td>
											<button class="btn btn-sm <?=$clase?>" data-tooltip="tooltip" title="<?=$title?>" data-placement="bottom" id="<?=$id_nivel?>" data-user="<?=$id_log?>">
												<?=$icon?>
											</button>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'niveles' . DS . 'agregarNivel.php';

if (isset($_POST['nombre'])) {
	$instancia->agregarNivelControl();
}
?>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/cursos/funcionesNivel.js"></script>