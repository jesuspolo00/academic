<!-- Agregar usuario -->
<div class="modal fade" id="agregar_nivel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Agregar Nivel</h5>
      </div>
      <form method="POST" id="registro">
        <input type="hidden" name="id_log" value="<?=$id_log?>">
        <div class="modal-body border-0 p-3">
          <div class="row p-3">
            <div class="col-lg-12 form-group">
              <label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="nombre" minlength="1" maxlength="50" required>
            </div>
            <div class="col-lg-12 form-group">
              <label class="font-weight-bold">Abreviatura</label>
              <input type="text" class="form-control" name="abreviatura" minlength="1" maxlength="50">
            </div>
          </div>
        </div>
        <div class="modal-footer border-0">
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
            <i class="fa fa-times"></i>
            &nbsp;
            Cerrar
          </button>
          <button type="submit" class="btn btn-success btn-sm">
            <i class="fa fa-save"></i>
            &nbsp;
            Registrar
          </button>
        </div>
      </form>
    </div>
  </div>
</div>