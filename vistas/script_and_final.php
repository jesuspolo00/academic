  <!-- Bootstrap core JavaScript-->
  <script src="<?=PUBLIC_PATH?>vendor/jquery/jquery.min.js"></script>
  <script src="<?=PUBLIC_PATH?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=PUBLIC_PATH?>js/bootstrapClockPicker.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?=PUBLIC_PATH?>vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?=PUBLIC_PATH?>js/sb-admin-2.min.js"></script>

  <!-- Alertas--->
  <script src="<?=PUBLIC_PATH?>js/ohsnap.js"></script>

  <!---- scripts creados----->
  <script src="<?=PUBLIC_PATH?>js/main.js"></script>
  <script src="<?=PUBLIC_PATH?>js/clockPicker.js"></script>