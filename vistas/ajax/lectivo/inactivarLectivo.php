<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'lectivo' . DS . 'ControlLectivo.php';

$objetClass = ControlLectivo::singleton_lectivo();
$rs         = $objetClass->inactivarLectivoControl();

$alerta = ($rs == true) ? 'ok' : 'error';

echo json_encode(['alerta' => $alerta]);
