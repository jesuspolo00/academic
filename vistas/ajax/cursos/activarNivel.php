<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'cursos' . DS . 'ControlCurso.php';

$objetClass = ControlCursos::singleton_cursos();
$rs         = $objetClass->activarNivelControl();

if ($rs == true) {
    $alerta = 'ok';
} else {
    $alerta = 'no';
}

echo json_encode(['alerta' => $alerta]);
