<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'estudiante' . DS . 'ControlEstudiante.php';
require_once CONTROL_PATH . 'lectivo' . DS . 'ControlLectivo.php';

$instancia_lectivo = ControlLectivo::singleton_lectivo();
$instancia         = ControlEstudiante::singleton_estudiante();

$datos_tipo_documento = $instancia_lectivo->mostrarTipoDocumentoControl();
$contacto             = $instancia->mostrarContactoControl();

$rs = $instancia->consultarDocumentoControl();

if (empty($rs[0]['documento'])) {
	$datos_visibles    = '';
	$datos_registrados = 'd-none';
	$alerta            = 'd-none';
}
if (isset($rs[0]['documento']) && !empty($rs[0]['documento'])) {
	$datos_visibles    = 'd-none';
	$datos_registrados = '';
	$alerta            = 'd-none';
	$nombre_completo   = $rs[0]['primer_nombre'] . ' ' . $rs[0]['segundo_nombre'] . ' ' . $rs[0]['primer_apellido'] . ' ' . $rs[0]['segundo_apellido'];
}
if (isset($rs[0]['documento']) && !empty($rs[0]['documento'])) {

	$validar_asignacion = $instancia->validarAsignacionControl($_POST['id_estudiante'], $rs[0]['id']);
	if ($validar_asignacion != false) {
		$datos_visibles    = 'd-none';
		$datos_registrados = 'd-none';
		$alerta            = '';
		$nombre_completo   = $rs[0]['primer_nombre'] . ' ' . $rs[0]['segundo_nombre'] . ' ' . $rs[0]['primer_apellido'] . ' ' . $rs[0]['segundo_apellido'];
	}
}

$informacion = $instancia->consultarInformcacionEstudianteControl($_POST['id_estudiante']);

if ($informacion['asignar_familiar'] == 'listo') {
	$disabled = '';
	$url      = BASE_URL . 'estudiante/listado?pagina=1';
} else {
	$disabled = 'disabled';
	$url      = '#';
}

?>

<div class="col-lg-12 form-group text-center <?=$datos_registrados?>">
	<p class="lead"><span class="font-weight-bold"><?=$nombre_completo?></span> usted ya se encuentra registrado(a) en el sistema como:
		<ul>
			<?php
			foreach ($rs as $listado) {
				$tipo       = $listado['tipo_familiar'];
				$estudiante = $listado['estudiante'];
				$ver        = ($estudiante == '') ? 'd-none' : '';
				?>
				<li class="<?=$ver?>"><span class="font-weight-bold"><?=$tipo?></span> de <span class="font-weight-bold"><?=$estudiante?></span></li>
				<?php
			}
			?>
		</ul>
	</p>
	<form method="POST">
		<div class="row">
			<input type="hidden" value="<?=$_POST['id_log']?>" name="id_log">
			<input type="hidden" value="<?=$_POST['id_estudiante']?>" name="id_estudiante">
			<input type="hidden" name="id_familiar" value="<?=$rs[0]['id']?>">
			<div class="col-lg-4 form-group">
				<label class="font-weight-bold">Agregarme como <span class="text-danger">*</span></label>
				<select name="tipo_familiar_reg" class="form-control" required>
					<option value="" selected>Seleccione una opcion...</option>
					<?php
					foreach ($contacto as $dato) {
						$id_contacto = $dato['id'];
						$nombre      = $dato['nombre'];
						?>
						<option value="<?=$id_contacto?>"><?=$nombre?></option>
						<?php
					}
					?>
				</select>
			</div>
			<div class="col-lg-4 form-group">
				<label class="font-weight-bold">Vive con el estudiante</label>
				<select name="vive_est_reg" class="form-control">
					<option value="No" selected>No</option>
					<option value="Si">Si</option>
				</select>
			</div>
			<div class="col-lg-4 form-group">
				<label class="font-weight-bold">Responsable economico <span class="text-danger">*</span></label>
				<select name="resp_economico" class="form-control" required>
					<option value="" selected>Seleccione una opcion...</option>
					<option value="No">No</option>
					<option value="Si">Si</option>
				</select>
			</div>
			<div class="col-lg-12 form-group mt-4 text-right">
				<button class="btn btn-success btn-sm" type="submit">
					<i class="fa fa-save"></i>
					&nbsp;
					Agregar familiar
				</button>
				<a class="btn btn-primary btn-sm disabled" href="#">
					<i class="fas fa-door-open"></i>
					&nbsp;
					Finalizar
				</a>
			</div>
		</div>
	</form>
</div>

<!-- Registro familiar -->

<form method="POST" enctype="multipart/form-data">
	<input type="hidden" value="<?=$_POST['id_log']?>" name="id_log">
	<input type="hidden" value="<?=$_POST['id_estudiante']?>" name="id_estudiante">
	<div class="row <?=$datos_visibles?>">
		<div class="col-lg-12 text-center mb-3 text-primary">
			<h4 class="font-weight-bold">Datos Basicos</h4>
			<hr>
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Foto del familiar</label>
			<div class="col-sm-12 text-center mt-4">
				<img src="https://placehold.it/220x220" id="preview" class="img-thumbnail img-fluid">
			</div>
			<input type="file" name="foto_familiar" class="file2" accept="image/*">
			<div class="input-group my-3 mt-4">
				<input type="text" class="form-control" disabled id="file">
				<div class="input-group-append">
					<button type="button" class="browse btn btn-primary">Buscar...</button>
				</div>
			</div>
		</div>
		<div class="col-lg-8">
			<div class="row">
				<div class="col-lg-6 form-group">
					<label class="font-weight-bold">Identificaci&oacute;n <span class="text-danger">*</span></label>
					<input type="text" class="form-control numeros" readonly name="documento" maxlength="50" required value="<?=$_POST['documento']?>">
				</div>
				<div class="col-lg-6 form-group">
					<label class="font-weight-bold">Tipo de identificaci&oacute;n <span class="text-danger">*</span></label>
					<select name="tipo_documento" class="form-control" required>
						<option value="" selected>Seleccione una opcion...</option>
						<?php
						foreach ($datos_tipo_documento as $tipo) {
							$id_tipo = $tipo['id'];
							$nombre  = $tipo['nombre'];
							$activo  = $tipo['activo'];

							$ver = ($activo == 1) ? '' : 'd-none';

							?>
							<option value="<?=$id_tipo?>" class="<?=$ver?>"><?=$nombre?></option>
							<?php
						}
						?>
					</select>
				</div>
				<div class="col-lg-6 form-group">
					<label class="font-weight-bold">Primer nombre <span class="text-danger">*</span></label>
					<input type="text" class="form-control letras" name="primer_nombre" required maxlength="50">
				</div>
				<div class="col-lg-6 form-group">
					<label class="font-weight-bold">Segundo nombre</label>
					<input type="text" class="form-control letras" name="segundo_nombre">
				</div>
				<div class="col-lg-6 form-group">
					<label class="font-weight-bold">Primer apellido <span class="text-danger">*</span></label>
					<input type="text" class="form-control letras" name="primer_apellido" required maxlength="50">
				</div>
				<div class="col-lg-6 form-group">
					<label class="font-weight-bold">Segundo apellido</label>
					<input type="text" class="form-control letras" name="segundo_apellido">
				</div>
				<div class="col-lg-6 form-group">
					<label class="font-weight-bold">Familiaridad <span class="text-danger">*</span></label>
					<select class="form-control" name="familiaridad" required>
						<option value="" selected>Seleccione una opcion...</option>
						<?php
						foreach ($contacto as $dato) {
							$id_contacto = $dato['id'];
							$nombre      = $dato['nombre'];
							?>
							<option value="<?=$id_contacto?>"><?=$nombre?></option>
							<?php
						}
						?>
					</select>
				</div>
				<div class="col-lg-6 form-group">
					<label class="font-weight-bold">Vive con el estudiante <span class="text-danger">*</span></label>
					<select name="vive_estudiante" class="form-control" required>
						<option value="No" selected>No</option>
						<option value="Si">Si</option>
						<option value="Fallecido">Fallecido</option>
					</select>
				</div>
			</div>
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Documento expedidio en</label>
			<input type="text" class="form-control" name="expedido" maxlength="50">
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Correo</label>
			<input type="text" class="form-control" name="correo">
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Celular <span class="text-danger">*</span></label>
			<input type="text" class="form-control numeros" name="celular" required>
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Pais</label>
			<input type="text" class="form-control" name="pais">
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Departamento</label>
			<input type="text" class="form-control" name="departamento">
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Municipio</label>
			<input type="text" class="form-control" name="municipio">
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Fecha de nacimiento</label>
			<input type="date" class="form-control" name="fecha_nac">
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Dir. Residencia</label>
			<input type="text" class="form-control" name="dir_residencia">
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Dir. Oficina</label>
			<input type="text" class="form-control" name="dir_oficina">
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Tel. Residencia</label>
			<input type="text" class="form-control" name="tel_residencia">
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Tel. Oficina</label>
			<input type="text" class="form-control" name="tel_oficina">
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Tipo de trabajo <span class="text-danger">*</span></label>
			<select class="form-control" name="tipo_trabajo" required>
				<option value="" selected>Seleccione una opcion...</option>
				<option value="Empresario">Empresario</option>
				<option value="Empleado">Empleado</option>
				<option value="Autoempleado">Autoempleado</option>
				<option value="Inversionista">Inversionista</option>
				<option value="Retirado">Retirado</option>
				<option value="No aplica">No aplica</option>
			</select>
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Profesi&oacute;n</label>
			<input type="text" name="profesion" class="form-control">
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Empresa</label>
			<input type="text" name="empresa" class="form-control">
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Cargo</label>
			<input type="text" name="cargo" class="form-control">
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">¿Ex alumno?</label>
			<select name="ex_alumno" class="form-control">
				<option value="No" selected>No</option>
				<option value="Si">Si</option>
			</select>
		</div>
		<div class="col-lg-4 form-group">
			<label class="font-weight-bold">Responsable economico</label>
			<select name="resp_economico" class="form-control" required>
				<option value="" selected>Seleccione una opcion...</option>
				<option value="No">No</option>
				<option value="Si">Si</option>
			</select>
		</div>
		<div class="col-lg-12 form-group mt-4 text-right">
			<button class="btn btn-success btn-sm" type="submit">
				<i class="fa fa-save"></i>
				&nbsp;
				Agregar familiar
			</button>
			<a class="btn btn-primary btn-sm <?=$disabled?>" href="#">
				<i class="fas fa-door-open"></i>
				&nbsp;
				Finalizar
			</a>
		</div>
	</div>
</form>

<div class="alert alert-red <?=$alerta?>" role="alert">
	<strong class="text-uppercase"><?=$nombre_completo?></strong> ya se encuentra asignado a este estudiante.
</div>


<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?=PUBLIC_PATH?>js/fileInput.js"></script>