<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'estudiante' . DS . 'ControlEstudiante.php';

$instancia = ControlEstudiante::singleton_estudiante();
$resultado = $instancia->removerFamiliarControl();

if ($resultado == true) {
    $mensaje = 'ok';
} else {
    $mensaje = 'no';
}

echo json_encode(['mensaje' => $mensaje]);
