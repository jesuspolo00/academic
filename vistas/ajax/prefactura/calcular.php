<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';

$subtotal  = 0;
$descuento = $_POST['descuento'];
$iva       = $_POST['iva'];

$array_totales = [];

$array_valor = array();
$array_valor = $_POST['valor'];

$array_cantidad = array();
$array_cantidad = $_POST['cantidad'];

$array_id_residuo = array();

$it = new MultipleIterator();
$it->attachIterator(new ArrayIterator($array_valor));
$it->attachIterator(new ArrayIterator($array_cantidad));
$it->attachIterator(new ArrayIterator($array_id_residuo));

foreach ($it as $datos) {
    $total_unidad = ($datos[0] * $datos[1]);
    $subtotal += $total_unidad;

    $array_totales[] = array('total_unidad' => $total_unidad, 'id_residuo' => $datos[2]);
}

$total_descuento = ($subtotal * $descuento) / 100;
$total_subtotal  = ($subtotal - $total_descuento);
$total_iva       = ($total_subtotal * $iva) / 100;
$total           = ($total_subtotal + $total_iva);

echo json_encode(['totales' => $array_totales, 'subtotal' => $subtotal, 'total_final' => $total]);
