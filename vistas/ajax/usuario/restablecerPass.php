<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuario.php';

$objetClass = ControlUsuario::singleton_usuario();
$rs = $objetClass->restablecerPassControl();

if($rs == TRUE){
	$alerta = 'ok';
}else{
	$alerta = 'no';
}

echo json_encode(['alerta' => $alerta]);