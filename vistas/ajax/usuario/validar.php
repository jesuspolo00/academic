<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuario.php';

$instancia = ControlUsuario::singleton_usuario();
$dato = $instancia->verificarUsuarioControl();

if($dato['id_user'] == ""){
	$alerta = 'no';
}else{
	$alerta = 'si';
}

echo json_encode(['alerta' => $alerta]);