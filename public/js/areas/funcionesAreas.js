$(document).ready(function() {
    $(".activar_area").click(function() {
        var id = $(this).attr('id');
        var id_log = $(this).attr('data-log');
        activarArea(id, id_log);
    });
    $(".inactivar_area").click(function() {
        var id = $(this).attr('id');
        var id_log = $(this).attr('data-log');
        inactivarArea(id, id_log);
    });

    function activarArea(id, id_log) {
        try {
            $.ajax({
                url: '../vistas/ajax/areas/activarArea.php',
                type: 'POST',
                data: {
                    'id': id,
                    'id_log': id_log
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.alerta == 'ok') {
                        ohSnap("Activado correctamente!", {
                            color: "yellow",
                            'duration': '1000'
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("ha ocurrido un error!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function inactivarArea(id, id_log) {
        try {
            $.ajax({
                url: '../vistas/ajax/areas/inactivarArea.php',
                type: 'POST',
                data: {
                    'id': id,
                    'id_log': id_log
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.alerta == 'ok') {
                        ohSnap("Inactivado correctamente!", {
                            color: "yellow",
                            'duration': '1000'
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("ha ocurrido un error!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace('index?pagina=1');
    }
});