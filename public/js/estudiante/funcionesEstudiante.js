$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    $("#campo_1").on('change', function() {
        if ($(this).is(':checked')) {
            $(".lectivo").removeClass('d-none');
            $(".grupo").removeClass('d-none');
            $(".grado").removeClass('d-none');
            $(".tipo_mat").addClass('d-none');
        }
    });
    $("#campo_2").on('change', function() {
        if ($(this).is(':checked')) {
            $(".lectivo").removeClass('d-none');
            $(".grupo").addClass('d-none');
            $(".grado").removeClass('d-none');
            $(".tipo_mat").removeClass('d-none');
        }
    });
    $("#validar").on(tipoEvento, function() {
        $("#registro_familiar").html('');
        var id = $("#documento").val();
        var id_log = $("#id_log").val();
        var id_estudiante = $("#id_estudiante").val();
        if (id != '') {
            $("#finalizar").hide();
            consultarDocumento(id, id_log, id_estudiante);
        } else {
            $("#documento").focus();
        }
    });
    $(".remover_asig").on(tipoEvento, function() {
        var id = $(this).attr('id');
        removerAsignacion(id);
    });

    function consultarDocumento(id, id_log, id_estudiante) {
        try {
            $.ajax({
                url: '../vistas/ajax/estudiante/consultarDocumento.php',
                method: 'POST',
                data: {
                    'documento': id,
                    'id_log': id_log,
                    'id_estudiante': id_estudiante
                },
                cache: false,
                success: function(resultado) {
                    if (resultado != '') {
                        $("#registro_familiar").html(resultado);
                    } else {
                        $("#finalizar").show();
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function removerAsignacion(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/estudiante/removerFamiliar.php',
                method: 'POST',
                data: {
                    'id': id
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje = 'ok') {
                        $(".familiar" + id).fadeOut();
                        ohSnap("Desasignado correctamente!", {
                            color: "yellow",
                            "duration": "1000"
                        });
                        $("#finalizar").hide();
                    } else {}
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
});