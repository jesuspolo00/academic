$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    $(".lista_permiso").on(tipoEvento, function(e) {
        e.preventDefault();
        var opcion = $(this).attr('id');
        var modulo = $(this).attr('data-modulo');
        var user = $(this).attr('data-user');
        var perfil = $(this).attr('data-perfil');
        var nombre = $(this).attr('data-nombre');
        activarPermiso(modulo, opcion, perfil, user, nombre);
    });
    $(".lista_inactivar").on(tipoEvento, function(e) {
        e.preventDefault();
        var opcion = $(this).attr('id');
        var modulo = $(this).attr('data-modulo');
        var user = $(this).attr('data-user');
        var perfil = $(this).attr('data-perfil');
        var nombre = $(this).attr('data-nombre');
        inactivarPermiso(modulo, opcion, perfil, user, nombre);
    });

    function activarPermiso(modulo, opcion, perfil, user, nombre) {
        try {
            $.ajax({
                url: '../vistas/ajax/permisos/activarPermiso.php',
                type: 'POST',
                data: {
                    'modulo': modulo,
                    'opcion': opcion,
                    'perfil': perfil,
                    'user': user
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        $("#" + opcion)
                        ohSnap("Permiso concedido", {
                            color: "green"
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error al conceder permiso", {
                            color: "red"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function inactivarPermiso(modulo, opcion, perfil, user, nombre) {
        try {
            $.ajax({
                url: '../vistas/ajax/permisos/inactivarPermiso.php',
                type: 'POST',
                data: {
                    'modulo': modulo,
                    'opcion': opcion,
                    'perfil': perfil,
                    'user': user
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        ohSnap("Permiso removido", {
                            color: "yellow"
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("Error al remover permiso", {
                            color: "red"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace('index?pagina=1');
    }
});