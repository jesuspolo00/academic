$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    $(".activar_lectivo").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var id_log = $(this).attr('data-user');
        activarLectivo(id, id_log);
    });
    $(".inactivar_lectivo").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var id_log = $(this).attr('data-user');
        inactivarLectivo(id, id_log);
    });

    function activarLectivo(id, id_log) {
        try {
            $.ajax({
                url: '../vistas/ajax/lectivo/activarLectivo.php',
                type: 'POST',
                data: {
                    'id': id,
                    'id_log': id_log
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.alerta == 'ok') {
                        ohSnap("Activado correctamente", {
                            color: "green",
                            'duration': '1000'
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {}
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function inactivarLectivo(id, id_log) {
        try {
            $.ajax({
                url: '../vistas/ajax/lectivo/inactivarLectivo.php',
                type: 'POST',
                data: {
                    'id': id,
                    'id_log': id_log
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.alerta == 'ok') {
                        ohSnap("Inactivado correctamente", {
                            color: "yellow",
                            'duration': '1000'
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {}
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace('index?pagina=1');
    }
});