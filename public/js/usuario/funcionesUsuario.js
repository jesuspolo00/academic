$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    $("#registrar_user").on(tipoEvento, function(e) {
        e.preventDefault();
        let user = $("#usuario").val();
        validarUsuario(user);
    });
    $(".inactivar_user").on(tipoEvento, function() {
        var id = $(this).attr('id');
        inactivarUsuario(id);
    });
    $(".activar_user").on(tipoEvento, function() {
        var id = $(this).attr('id');
        activarUsuario(id);
    });
    $(".restart_pass").on(tipoEvento, function() {
        var id = $(this).attr('id');
        restablecerPass(id);
    });

    function validarUsuario(user) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuario/validar.php',
                type: 'POST',
                data: {
                    'usuario': user
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.alerta == 'si') {
                        ohSnap("Usuario ya registrado", {
                            color: "red"
                        });
                        $("#usuario").focus();
                    } else {
                        $("#registro").submit();
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function inactivarUsuario(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuario/inactivarUsuario.php',
                method: 'POST',
                data: {
                    'id_user': id
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.alerta == 'ok') {
                        $('#' + id).removeAttr('title');
                        $('#' + id + '').removeClass('btn btn-danger btn-sm inactivar_user').addClass('btn btn-success btn-sm activar_user');
                        $('#' + id + ' i').removeClass('fa-times').addClass('fa-check');
                        ohSnap("Inactivado correctamente!", {
                            color: "yellow",
                            'duration': '1000'
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("ha ocurrido un error!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function activarUsuario(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuario/activarUsuario.php',
                method: 'POST',
                data: {
                    'id_user': id
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.alerta == 'ok') {
                        $('#' + id + '').removeAttr('title');
                        $('#' + id + '').removeClass('btn btn-success btn-sm activar_user').addClass('btn btn-danger btn-sm inactivar_user');
                        $('#' + id + ' i').removeClass('fa-times').addClass('fa-times');
                        ohSnap("Activado correctamente!", {
                            color: "yellow",
                            'duration': '1000'
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("ha ocurrido un error!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function restablecerPass(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuario/restablecerPass.php',
                method: 'POST',
                data: {
                    'id_user': id
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.alerta == 'ok') {
                        ohSnap("Se ha restablecido correctamente!", {
                            color: "green",
                            'duration': '1000'
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("ha ocurrido un error!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace('index?pagina=1');
    }
});