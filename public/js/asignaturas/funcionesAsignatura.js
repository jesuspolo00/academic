$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*---------------------------------------------*/
    $(".curso_asignado").on(tipoEvento, function() {
        var id = $(this).attr('id');
        removerCursoAsignado(id);
    });
    /*--------------------------------*/
    function removerCursoAsignado(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/asignatura/removerCurso.php',
                type: 'POST',
                data: {
                    'id': id,
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.alerta == 'ok') {
                        $(".listado" + id).fadeOut();
                        ohSnap("Curso removido!", {
                            color: "green"
                        });
                    } else {
                        ohSnap("Error!", {
                            color: "red"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
});