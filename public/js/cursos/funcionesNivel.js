$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    $(".inactivar_nivel").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var id_log = $(this).attr('data-user');
        inactivarNivel(id, id_log);
    });
    $(".activar_nivel").on(tipoEvento, function() {
        var id = $(this).attr('id');
        var id_log = $(this).attr('data-user');
        activarNivel(id, id_log);
    });

    function inactivarNivel(id, id_log) {
        try {
            $.ajax({
                url: '../vistas/ajax/cursos/inactivarNivel.php',
                type: 'POST',
                data: {
                    'id': id,
                    'id_log': id_log
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.alerta == 'ok') {
                        $('#' + id + '').removeAttr('title');
                        $('#' + id + '').removeClass('btn btn-sm btn-success activar_nivel').addClass('btn btn-sm btn-danger inactivar_nivel');
                        $('#' + id + ' i').removeClass('fa-check').addClass('fa-times');
                        ohSnap("Inactivado correctamente!", {
                            color: "yellow",
                            'duration': '1000'
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("ha ocurrido un error!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function activarNivel(id, id_log) {
        try {
            $.ajax({
                url: '../vistas/ajax/cursos/activarNivel.php',
                type: 'POST',
                data: {
                    'id': id,
                    'id_log': id_log
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.alerta == 'ok') {
                        $('#' + id + '').removeAttr('title');
                        $('#' + id + '').removeClass('btn btn-sm btn-danger inactivar_nivel').addClass('btn btn-sm btn-success activar_nivel');
                        $('#' + id + ' i').removeClass('fa-times').addClass('fa-check');
                        ohSnap("Activado correctamente!", {
                            color: "green",
                            'duration': '1000'
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("ha ocurrido un error!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace('index?pagina=1');
    }
});