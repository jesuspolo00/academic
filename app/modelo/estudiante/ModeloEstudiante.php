<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloEstudiante extends conexion
{

    public static function registroEstudianteModel($datos)
    {
        $tabla  = 'estudiantes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO estudiantes (
        identificacion,
        id_tipo_doc,
        lugar_expedicion,
        genero,
        primer_nombre,
        segundo_nombre,
        primer_apellido,
        segundo_apellido,
        fecha_nacimiento,
        pais_nac,
        departamento_nac,
        municipio_nac,
        telefono,
        celular,
        correo,
        pais_res,
        departamento_res,
        municipio_res,
        direccion,
        barrio,
        tipo_vivienda,
        estable_procede,
        inclusion,
        id_lectivo,
        id_curso,
        user_log
        )
        VALUES
        (
        '" . $datos['identificacion'] . "',
        '" . $datos['id_tipo_doc'] . "',
        '" . $datos['lugar_expedicion'] . "',
        '" . $datos['genero'] . "',
        '" . $datos['primer_nombre'] . "',
        '" . $datos['segundo_nombre'] . "',
        '" . $datos['primer_apellido'] . "',
        '" . $datos['segundo_apellido'] . "',
        '" . $datos['fecha_nacimiento'] . "',
        '" . $datos['pais_nac'] . "',
        '" . $datos['departamento_nac'] . "',
        '" . $datos['municipio_nac'] . "',
        '" . $datos['telefono'] . "',
        '" . $datos['celular'] . "',
        '" . $datos['correo'] . "',
        '" . $datos['pais_res'] . "',
        '" . $datos['departamento_res'] . "',
        '" . $datos['municipio_res'] . "',
        '" . $datos['direccion'] . "',
        '" . $datos['barrio'] . "',
        '" . $datos['tipo_vivienda'] . "',
        '" . $datos['estable_procede'] . "',
        '" . $datos['inclusion'] . "',
        '" . $datos['anio_lectivo'] . "',
        '" . $datos['id_curso'] . "',
        '" . $datos['user_log'] . "'
        );
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarTipoRegistroModel($datos)
    {
        $tabla  = 'pre_matricula';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_estudiante, id_user_estudiante, anio_lectivo, id_log, id_curso) VALUES (:ie, :iue, :tr, :il, :ic)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ie', $datos['id_estudiante']);
            $preparado->bindParam(':iue', $datos['id_user_estudiante']);
            $preparado->bindParam(':tr', $datos['anio_lectivo']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ic', $datos['id_grado']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarEstudianteModel($inicio, $fin, $datos)
    {
        $tabla  = 'estudiantes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT e.*, u.*,
        (SELECT c.nombre FROM cursos c WHERE c.id = e.id_curso) AS curso,
        (SELECT a.nombre FROM anio_lectivo a WHERE a.id = e.id_lectivo) AS lectivo
        FROM estudiantes e
        LEFT JOIN pre_matricula t ON e.id = t.id_estudiante
        LEFT JOIN usuarios u ON t.id_user_estudiante = u.id_user
        WHERE CONCAT(e.identificacion, e.primer_nombre, e.segundo_nombre, e.primer_apellido, e.segundo_apellido) LIKE '%" . $datos['estudiante'] . "%'
        " . $datos['lectivo'] . " " . $datos['curso'] . " LIMIT :inicio, :fin;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':inicio', $inicio, PDO::PARAM_INT);
            $preparado->bindValue(':fin', $fin, PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodosEstudiantesModel($consulta)
    {
        $tabla  = 'pre_matricula';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        t.id AS prematricula,
        e.*,
        u.*,
        (SELECT c.nombre FROM cursos c WHERE c.id = e.id_curso) AS curso,
        (SELECT a.nombre FROM anio_lectivo a WHERE a.id = e.id_lectivo) AS lectivo
        FROM " . $tabla . " t
        LEFT JOIN estudiantes e ON e.id = t.id_estudiante
        LEFT JOIN usuarios u ON t.id_user_estudiante = u.id_user
        WHERE " . $consulta . ";";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarEstudianteIdModel($id)
    {
        $tabla  = 'estudiantes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT e.*, u.*, inf.*, d.*,
        (SELECT t.nombre FROM tipo_documento t WHERE t.id = e.id_tipo_doc) AS tipo_documento,
        (SELECT c.nombre FROM cursos c WHERE c.id = e.id_curso) AS curso,
        (SELECT a.nombre FROM anio_lectivo a WHERE a.id = e.id_lectivo) AS lectivo,
        (SELECT c.nombre FROM contacto c WHERE c.id = inf.id_contacto) AS contacto,
        (SELECT fe.nombre FROM foto_estudiante fe WHERE fe.id_estudiante = e.id ORDER BY fe.id DESC LIMIT 1) AS imagen
        FROM estudiantes e
        LEFT JOIN pre_matricula t ON e.id = t.id_estudiante
        LEFT JOIN usuarios u ON t.id_user_estudiante = u.id_user
        LEFT JOIN info_adicional inf ON inf.id_estudiante = e.id
        LEFT JOIN datos_medicos d ON d.id_estudiante = e.id
        WHERE e.id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id, PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarContactoModel()
    {
        $tabla  = 'contacto';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla;
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function informacionAdicionalModel($datos)
    {
        $tabla  = 'info_adicional';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
        id_estudiante,
        tipo_sangre,
        religion,
        peso,
        estatura,
        hermanos,
        mayor,
        estado_civil,
        estrato,
        id_contacto,
        id_log
        )
        VALUES
        (:ide, :ts, :rl, :ps, :est, :hm, :m, :ec, :et ,:ic, :idl);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ide', $datos['id_estudiante']);
            $preparado->bindParam(':ts', $datos['tipo_sangre']);
            $preparado->bindParam(':rl', $datos['religion']);
            $preparado->bindParam(':ps', $datos['peso']);
            $preparado->bindParam(':est', $datos['estatura']);
            $preparado->bindParam(':hm', $datos['hermanos']);
            $preparado->bindParam(':m', $datos['mayor']);
            $preparado->bindParam(':ec', $datos['estado_civil']);
            $preparado->bindParam(':et', $datos['estrato']);
            $preparado->bindParam(':ic', $datos['contactar']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function informacionMedicaEstudianteModel($datos)
    {
        $tabla  = 'datos_medicos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
        id_estudiante,
        descp_medica,
        nom_descp,
        dosis_descp,
        alerg_medica,
        espc_alerg,
        trat_alerg,
        alerg_alimento,
        especifique_alimento,
        dieta,
        cual_dieta,
        seguro_accidente,
        antialergicos,
        analgesicos,
        milanta,
        antiespasmodico,
        eps,
        nom_medico,
        tel_medico,
        clinica,
        num_afilia,
        enf_ojos,
        lentes_perm,
        asma,
        enf_respiratorias,
        enf_cardiaca,
        enf_gastricas,
        diabetes,
        celiaquismo,
        pro_psiqui,
        hepatitis,
        anemia,
        hiper_arterial,
        hipo_arterial,
        epilepsia,
        hernias,
        migrana,
        frac_traum,
        artritis,
        desnutricion,
        obesidad,
        cancer,
        vih,
        enf_renal,
        otros,
        protesis,
        cirugias,
        convulsion,
        enf_actual,
        medicamentos,
        cond_salud,
        diabetes_fam,
        cancer_fam,
        hiper_fam,
        cardiovascular_fam,
        otros_fam,
        covid,
        covid_diag,
        etnia,
        id_log
        )
        VALUES
        (
        '" . $datos['id_estudiante'] . "',
        '" . $datos['descp_medica'] . "',
        '" . $datos['nom_descp'] . "',
        '" . $datos['dosis_descp'] . "',
        '" . $datos['alerg_medica'] . "',
        '" . $datos['espc_alerg'] . "',
        '" . $datos['trat_alerg'] . "',
        '" . $datos['alerg_alimento'] . "',
        '" . $datos['especifique_alimento'] . "',
        '" . $datos['dieta'] . "',
        '" . $datos['cual_dieta'] . "',
        '" . $datos['seguro_accidente'] . "',
        '" . $datos['antialergicos'] . "',
        '" . $datos['analgesicos'] . "',
        '" . $datos['milanta'] . "',
        '" . $datos['antiespasmodico'] . "',
        '" . $datos['eps'] . "',
        '" . $datos['nom_medico'] . "',
        '" . $datos['tel_medico'] . "',
        '" . $datos['clinica'] . "',
        '" . $datos['num_afilia'] . "',
        '" . $datos['enf_ojos'] . "',
        '" . $datos['lentes_perm'] . "',
        '" . $datos['asma'] . "',
        '" . $datos['enf_respiratorias'] . "',
        '" . $datos['enf_cardiaca'] . "',
        '" . $datos['enf_gastricas'] . "',
        '" . $datos['diabetes'] . "',
        '" . $datos['celiaquismo'] . "',
        '" . $datos['pro_psiqui'] . "',
        '" . $datos['hepatitis'] . "',
        '" . $datos['anemia'] . "',
        '" . $datos['hiper_arterial'] . "',
        '" . $datos['hipo_arterial'] . "',
        '" . $datos['epilepsia'] . "',
        '" . $datos['hernias'] . "',
        '" . $datos['migrana'] . "',
        '" . $datos['frac_traum'] . "',
        '" . $datos['artritis'] . "',
        '" . $datos['desnutricion'] . "',
        '" . $datos['obesidad'] . "',
        '" . $datos['cancer'] . "',
        '" . $datos['vih'] . "',
        '" . $datos['enf_renal'] . "',
        '" . $datos['otros'] . "',
        '" . $datos['protesis'] . "',
        '" . $datos['cirugias'] . "',
        '" . $datos['convulsion'] . "',
        '" . $datos['enf_actual'] . "',
        '" . $datos['medicamentos'] . "',
        '" . $datos['cond_salud'] . "',
        '" . $datos['diabetes_fam'] . "',
        '" . $datos['cancer_fam'] . "',
        '" . $datos['hiper_fam'] . "',
        '" . $datos['cardiovascular_fam'] . "',
        '" . $datos['otros_fam'] . "',
        '" . $datos['covid'] . "',
        '" . $datos['covid_diag'] . "',
        '" . $datos['etnia'] . "',
        '" . $datos['id_log'] . "'
        );

        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarFotoEstudianteModel($datos)
    {
        $tabla  = 'foto_estudiante';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_estudiante, nombre, id_log) VALUES (:ide, :n, :idl)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ide', $datos['id_estudiante']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function consultarDocumentoModel($id, $id_estudiante)
    {
        $tabla  = 'familiar';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT f.*,
        CONCAT(e.primer_nombre, ' ' , e.segundo_nombre, ' ', e.primer_apellido, ' ', e.segundo_apellido) AS estudiante,
        (SELECT c.nombre FROM contacto c WHERE c.id = fe.tipo_familiar) AS tipo_familiar
        FROM familiar  f
        LEFT JOIN familiar_estudiante fe ON f.id = fe.id_familiar
        LEFT JOIN estudiantes e ON fe.id_estudiante = e.id
        WHERE f.documento = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function informacionFamiliarModel($datos)
    {
        $tabla  = 'familiar';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
        documento,
        tipo_documento,
        primer_nombre,
        segundo_nombre,
        primer_apellido,
        segundo_apellido,
        familiaridad,
        vive_estudiante,
        expedido,
        correo,
        celular,
        pais,
        departamento,
        municipio,
        fecha_nac,
        dir_residencia,
        tel_residencia,
        tel_oficina,
        tipo_trabajo,
        profesion,
        empresa,
        cargo,
        ex_alumno,
        id_log
        )
        VALUES
        (
        '" . $datos['documento'] . "',
        '" . $datos['tipo_documento'] . "',
        '" . $datos['primer_nombre'] . "',
        '" . $datos['segundo_nombre'] . "',
        '" . $datos['primer_apellido'] . "',
        '" . $datos['segundo_apellido'] . "',
        '" . $datos['familiaridad'] . "',
        '" . $datos['vive_estudiante'] . "',
        '" . $datos['expedido'] . "',
        '" . $datos['correo'] . "',
        '" . $datos['celular'] . "',
        '" . $datos['pais'] . "',
        '" . $datos['departamento'] . "',
        '" . $datos['municipio'] . "',
        '" . $datos['fecha_nac'] . "',
        '" . $datos['dir_residencia'] . "',
        '" . $datos['tel_residencia'] . "',
        '" . $datos['tel_oficina'] . "',
        '" . $datos['tipo_trabajo'] . "',
        '" . $datos['profesion'] . "',
        '" . $datos['empresa'] . "',
        '" . $datos['cargo'] . "',
        '" . $datos['ex_alumno'] . "',
        '" . $datos['id_log'] . "'
        );

        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('id' => $id, 'guardar' => true);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarFotoFamiliarModel($datos)
    {
        $tabla  = 'foto_familiar';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_familiar, nombre, id_log) VALUES (:ide, :n, :idl)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ide', $datos['id_familiar']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function asignarFamiliarEstudianteModel($datos)
    {
        $tabla  = 'familiar_estudiante';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_estudiante,id_familiar,vive_estudiante,tipo_familiar,id_log, resp_economico) VALUES(:ide, :idf, :ve, :tf, :idl, :r);
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ide', $datos['id_estudiante']);
            $preparado->bindParam(':idf', $datos['id_familiar']);
            $preparado->bindParam(':ve', $datos['vive_estudiante']);
            $preparado->bindParam(':tf', $datos['tipo_familiar']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':r', $datos['resp_economico']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarFamiliaresModel($id)
    {
        $tabla  = 'familiar';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT *,
        (SELECT fm.nombre FROM foto_familiar fm WHERE fm.id_familiar = f.id ORDER BY fm.id DESC LIMIT 1) AS imagen,
        (SELECT fe.id FROM familiar_estudiante fe WHERE fe.id_familiar = f.id AND fe.id_estudiante = :id) AS id_relacion,
        (SELECT fe.resp_economico FROM familiar_estudiante fe WHERE fe.id_familiar = f.id AND fe.id_estudiante = :id) AS resp_economico,
        (SELECT c.nombre FROM contacto c WHERE c.id IN(SELECT fe.tipo_familiar FROM familiar_estudiante fe WHERE fe.id_familiar = f.id AND fe.id_estudiante = :id)) AS tipo_familiar,
        (SELECT c.id FROM contacto c WHERE c.id IN(SELECT fe.tipo_familiar FROM familiar_estudiante fe WHERE fe.id_familiar = f.id AND fe.id_estudiante = :id)) AS id_tipo_familiar,
        (SELECT i.id_contacto FROM info_adicional i WHERE i.id_estudiante = :id) AS contacto
        FROM familiar f
        WHERE f.id IN(SELECT fe.id_familiar FROM familiar_estudiante fe WHERE fe.id_estudiante = :id);
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id, PDO::PARAM_INT);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function consultarInformcacionEstudianteModel($id)
    {
        $tabla  = 'estudiantes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        IF(d.id IS NULL, 'falta', 'listo') AS datos_medicos,
        IF(f.id IS NULL, 'falta', 'listo') AS asignar_familiar
        FROM " . $tabla . " e
        LEFT JOIN datos_medicos d ON e.id = d.id_estudiante
        LEFT JOIN familiar_estudiante f ON e.id = f.id_estudiante
        WHERE e.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function validarAsignacionModel($estudiante, $id)
    {
        $tabla  = 'familiar_estudiante';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM familiar_estudiante WHERE id_estudiante = :ide AND id_familiar = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':ide', $estudiante);
            $preparado->bindValue(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarInformacionEstudianteModel($datos)
    {
        $tabla  = 'estudiantes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE
        " . $tabla . "
        SET
        identificacion = '" . $datos['identificacion'] . "',
        id_tipo_doc = '" . $datos['id_tipo_doc'] . "',
        lugar_expedicion = '" . $datos['lugar_expedicion'] . "',
        genero = '" . $datos['genero'] . "',
        primer_nombre = '" . $datos['primer_nombre'] . "',
        segundo_nombre = '" . $datos['segundo_nombre'] . "',
        primer_apellido = '" . $datos['primer_apellido'] . "',
        segundo_apellido = '" . $datos['segundo_apellido'] . "',
        fecha_nacimiento = '" . $datos['fecha_nacimiento'] . "',
        pais_nac = '" . $datos['pais_nac'] . "',
        departamento_nac = '" . $datos['departamento_nac'] . "',
        municipio_nac = '" . $datos['municipio_nac'] . "',
        telefono = '" . $datos['telefono'] . "',
        celular = '" . $datos['celular'] . "',
        correo = '" . $datos['correo'] . "',
        pais_res = '" . $datos['pais_res'] . "',
        departamento_res = '" . $datos['departamento_res'] . "',
        municipio_res = '" . $datos['municipio_res'] . "',
        direccion = '" . $datos['direccion'] . "',
        barrio = '" . $datos['barrio'] . "',
        tipo_vivienda = '" . $datos['tipo_vivienda'] . "',
        estable_procede = '" . $datos['estable_procede'] . "',
        inclusion = '" . $datos['inclusion'] . "',
        user_log = '" . $datos['user_log'] . "',
        fecha_update = '" . $datos['fecha_update'] . "'
        WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_estudiante']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function bloquearEstudianteModel($datos)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = :b, fecha_update = :fu WHERE id_user IN(SELECT p.id_user_estudiante FROM pre_matricula p WHERE p.id_estudiante = :id);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id_estudiante']);
            $preparado->bindValue(':fu', $datos['fecha_update']);
            $preparado->bindValue(':b', $datos['bloqueado']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarDatosAdicionalesModel($datos)
    {
        $tabla  = 'info_adicional';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE
        " . $tabla . "
        SET
        tipo_sangre = '" . $datos['tipo_sangre'] . "',
        religion = '" . $datos['religion'] . "',
        peso = '" . $datos['peso'] . "',
        estatura = '" . $datos['estatura'] . "',
        hermanos = '" . $datos['hermanos'] . "',
        mayor = '" . $datos['mayor'] . "',
        estado_civil = '" . $datos['estado_civil'] . "',
        estrato = '" . $datos['estrato'] . "',
        id_contacto = '" . $datos['id_contacto'] . "',
        id_log = '" . $datos['id_log'] . "',
        fecha_update = '" . $datos['fecha_update'] . "'
        WHERE id_estudiante = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id_estudiante']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarDatosMedicosModel($datos)
    {
        $tabla  = 'datos_medicos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE
        " . $tabla . "
        SET
        descp_medica = '" . $datos['descp_medica'] . "',
        nom_descp = '" . $datos['nom_descp'] . "',
        dosis_descp = '" . $datos['dosis_descp'] . "',
        alerg_medica = '" . $datos['alerg_medica'] . "',
        espc_alerg = '" . $datos['espc_alerg'] . "',
        trat_alerg = '" . $datos['trat_alerg'] . "',
        alerg_alimento = '" . $datos['alerg_alimento'] . "',
        especifique_alimento = '" . $datos['especifique_alimento'] . "',
        dieta = '" . $datos['dieta'] . "',
        cual_dieta = '" . $datos['cual_dieta'] . "',
        seguro_accidente = '" . $datos['seguro_accidente'] . "',
        antialergicos = '" . $datos['antialergicos'] . "',
        analgesicos = '" . $datos['analgesicos'] . "',
        milanta = '" . $datos['milanta'] . "',
        antiespasmodico = '" . $datos['antiespasmodico'] . "',
        eps = '" . $datos['eps'] . "',
        nom_medico = '" . $datos['nom_medico'] . "',
        tel_medico = '" . $datos['tel_medico'] . "',
        clinica = '" . $datos['clinica'] . "',
        num_afilia = '" . $datos['num_afilia'] . "',
        enf_ojos = '" . $datos['enf_ojos'] . "',
        lentes_perm = '" . $datos['lentes_perm'] . "',
        asma = '" . $datos['asma'] . "',
        enf_respiratorias = '" . $datos['enf_respiratorias'] . "',
        enf_cardiaca = '" . $datos['enf_cardiaca'] . "',
        enf_gastricas = '" . $datos['enf_gastricas'] . "',
        diabetes = '" . $datos['diabetes'] . "',
        celiaquismo = '" . $datos['celiaquismo'] . "',
        pro_psiqui = '" . $datos['pro_psiqui'] . "',
        hepatitis = '" . $datos['hepatitis'] . "',
        anemia = '" . $datos['anemia'] . "',
        hiper_arterial = '" . $datos['hiper_arterial'] . "',
        hipo_arterial = '" . $datos['hipo_arterial'] . "',
        epilepsia = '" . $datos['epilepsia'] . "',
        hernias = '" . $datos['hernias'] . "',
        migrana = '" . $datos['migrana'] . "',
        frac_traum = '" . $datos['frac_traum'] . "',
        artritis = '" . $datos['artritis'] . "',
        desnutricion = '" . $datos['desnutricion'] . "',
        obesidad = '" . $datos['obesidad'] . "',
        cancer = '" . $datos['cancer'] . "',
        vih = '" . $datos['vih'] . "',
        enf_renal = '" . $datos['enf_renal'] . "',
        otros = '" . $datos['otros'] . "',
        protesis = '" . $datos['protesis'] . "',
        cirugias = '" . $datos['cirugias'] . "',
        convulsion = '" . $datos['convulsion'] . "',
        enf_actual = '" . $datos['enf_actual'] . "',
        medicamentos = '" . $datos['medicamentos'] . "',
        cond_salud = '" . $datos['cond_salud'] . "',
        diabetes_fam = '" . $datos['diabetes_fam'] . "',
        cancer_fam = '" . $datos['cancer_fam'] . "',
        hiper_fam = '" . $datos['hiper_fam'] . "',
        cardiovascular_fam = '" . $datos['cardiovascular_fam'] . "',
        otros_fam = '" . $datos['otros_fam'] . "',
        covid = '" . $datos['covid'] . "',
        covid_diag = '" . $datos['covid_diag'] . "',
        etnia = '" . $datos['etnia'] . "',
        id_log = '" . $datos['id_log'] . "',
        fecha_update = '" . $datos['fecha_update'] . "'
        WHERE id_estudiante = :id;
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id_estudiante']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function enviarObservacionModel($datos)
    {
        $tabla  = 'info_observacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
        id_persona,
        tipo_persona,
        asunto,
        correo_destino,
        mensaje,
        id_log
    ) VALUES (:id, :tp, :as, :cd, :m, :idl);";
    try {
        $preparado = $cnx->preparar($cmdsql);
        $preparado->bindParam(':id', $datos['id_persona']);
        $preparado->bindParam(':tp', $datos['tipo_persona']);
        $preparado->bindParam(':as', $datos['asunto']);
        $preparado->bindParam(':cd', $datos['correo_destino']);
        $preparado->bindParam(':m', $datos['mensaje']);
        $preparado->bindParam(':idl', $datos['id_log']);
        $preparado->setFetchMode(PDO::FETCH_ASSOC);
        if ($preparado->execute()) {
            return true;
        } else {
            return false;
        }
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage();
    }
    $cnx->closed();
    $cnx = null;
}

public static function documentosSolicitadosModel($datos)
{
    $tabla  = 'documentos_sol';
    $cnx    = conexion::singleton_conexion();
    $cmdsql = "INSERT INTO " . $tabla . " (
    nombre,
    tipo_doc,
    id_persona,
    tipo_persona,
    id_log,
    prematricula
    )
    VALUES(:n, :td, :id, :tp, :idl, :p);";
    try {
        $preparado = $cnx->preparar($cmdsql);
        $preparado->bindParam(':id', $datos['id_persona']);
        $preparado->bindParam(':tp', $datos['tipo_persona']);
        $preparado->bindParam(':idl', $datos['id_log']);
        $preparado->bindParam(':n', $datos['nombre']);
        $preparado->bindParam(':td', $datos['tipo_doc']);
        $preparado->bindParam(':p', $datos['prematricula']);
        $preparado->setFetchMode(PDO::FETCH_ASSOC);
        if ($preparado->execute()) {
            return true;
        } else {
            return false;
        }
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage();
    }
    $cnx->closed();
    $cnx = null;
}

public static function verificarDocumentosSubidosModel($id)
{
    $tabla  = 'documentos_sol';
    $cnx    = conexion::singleton_conexion();
    $cmdsql = "SELECT COUNT(id) as cant FROM " . $tabla . " WHERE id_persona = :id AND tipo_persona = :t AND tipo_doc IN (1,2,3);";
    try {
        $preparado = $cnx->preparar($cmdsql);
        $preparado->bindValue(':id', $id, PDO::PARAM_INT);
        $preparado->bindValue(':t', 1, PDO::PARAM_INT);
        $preparado->setFetchMode(PDO::FETCH_ASSOC);
        if ($preparado->execute()) {
            return $preparado->fetch();
        } else {
            return false;
        }
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage();
    }
    $cnx->closed();
    $cnx = null;
}

public static function verificarDocumentosInstSubidosModel($id)
{
    $tabla  = 'documentos_sol';
    $cnx    = conexion::singleton_conexion();
    $cmdsql = "SELECT COUNT(id) AS cant FROM " . $tabla . " WHERE id_persona = :id AND tipo_persona = :t AND tipo_doc IN (4,5,6);";
    try {
        $preparado = $cnx->preparar($cmdsql);
        $preparado->bindValue(':id', $id, PDO::PARAM_INT);
        $preparado->bindValue(':t', 1, PDO::PARAM_INT);
        $preparado->setFetchMode(PDO::FETCH_ASSOC);
        if ($preparado->execute()) {
            return $preparado->fetch();
        } else {
            return false;
        }
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage();
    }
    $cnx->closed();
    $cnx = null;
}

public static function verificarObligatorioDocInstModel()
{
    $tabla  = 'documentos';
    $cnx    = conexion::singleton_conexion();
    $cmdsql = "SELECT id FROM documentos WHERE obligatorio = 1 AND nombre LIKE '%contrato%';";
    try {
        $preparado = $cnx->preparar($cmdsql);
        $preparado->setFetchMode(PDO::FETCH_ASSOC);
        if ($preparado->execute()) {
            return $preparado->fetch();
        } else {
            return false;
        }
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage();
    }
    $cnx->closed();
    $cnx = null;
}

public static function mostrarDatosPrematriculaModel($id)
{
    $tabla  = 'pre_matricula';
    $cnx    = conexion::singleton_conexion();
    $cmdsql = "SELECT
    p.id AS prematricula,
    e.id AS id_estudiante,
    CONCAT(e.primer_nombre, ' ', e.segundo_nombre, ' ', e.primer_apellido, ' ', e.segundo_apellido) AS nombre_estudiante,
    (SELECT (SELECT CONCAT(f.primer_nombre, ' ', f.segundo_nombre, ' ', f.primer_apellido, ' ', f.segundo_apellido)
    FROM familiar f WHERE f.id = fe.id_familiar) FROM familiar_estudiante fe WHERE fe.id_estudiante = p.id_estudiante AND fe.tipo_familiar = 2) AS nombre_madre,
    (SELECT (SELECT f.documento FROM familiar f WHERE f.id = fe.id_familiar)
    FROM familiar_estudiante fe WHERE fe.id_estudiante = p.id_estudiante AND fe.tipo_familiar = 2) AS documento_madre,
    (SELECT (SELECT f.expedido FROM familiar f WHERE f.id = fe.id_familiar)
    FROM familiar_estudiante fe WHERE fe.id_estudiante = p.id_estudiante AND fe.tipo_familiar = 2) AS expedido_madre,
    (SELECT (SELECT CONCAT(f.primer_nombre, ' ', f.segundo_nombre, ' ', f.primer_apellido, ' ', f.segundo_apellido)
    FROM familiar f WHERE f.id = fe.id_familiar) FROM familiar_estudiante fe WHERE fe.id_estudiante = p.id_estudiante AND fe.tipo_familiar = 1) AS nombre_padre,
    (SELECT (SELECT f.documento FROM familiar f WHERE f.id = fe.id_familiar)
    FROM familiar_estudiante fe WHERE fe.id_estudiante = p.id_estudiante AND fe.tipo_familiar = 1) AS documento_padre,
    (SELECT (SELECT f.expedido FROM familiar f WHERE f.id = fe.id_familiar)
    FROM familiar_estudiante fe WHERE fe.id_estudiante = p.id_estudiante AND fe.tipo_familiar = 1) AS expedido_padre,
    (SELECT c.nombre FROM cursos c WHERE c.id = p.id_curso) AS curso,
    (SELECT SUBSTRING_INDEX( l.nombre,  '-' , 1 ) AS Subcategoria FROM anio_lectivo l WHERE l.id = p.anio_lectivo) AS anio_lectivo_i,
    (SELECT SUBSTRING_INDEX( l.nombre,  '-' , -1 ) AS Subcategoria FROM anio_lectivo l WHERE l.id = p.anio_lectivo) AS anio_lectivo_f,
    e.direccion,
    e.telefono,
    e.fecha_nacimiento
    FROM pre_matricula p
    LEFT JOIN estudiantes e ON e.id = p.id_estudiante
    WHERE p.id = :id;";
    try {
        $preparado = $cnx->preparar($cmdsql);
        $preparado->bindParam(':id', $id);
        $preparado->setFetchMode(PDO::FETCH_ASSOC);
        if ($preparado->execute()) {
            return $preparado->fetch();
        } else {
            return false;
        }
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage();
    }
    $cnx->closed();
    $cnx = null;
}

}
