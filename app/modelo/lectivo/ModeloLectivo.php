<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloLectivo extends conexion
{

    public static function agregarLectivoModel($datos)
    {
        $tabla  = 'anio_lectivo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_log) VALUES (:iu, :il)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':iu', $datos['nombre']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarLectivoModel($inicio, $final)
    {
        $tabla  = 'anio_lectivo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " LIMIT :inicio, :final";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':inicio', $inicio, PDO::PARAM_INT);
            $preparado->bindParam(':final', $final, PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodosLectivoModel()
    {
        $tabla  = 'anio_lectivo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla;
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTipoDocumentoModel()
    {
        $tabla  = 'tipo_documento';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla;
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarLectivoModel($datos)
    {
        $tabla  = 'anio_lectivo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE anio_lectivo SET activo = 1, fecha_activo = :f, user_update = :il WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id']);
            $preparado->bindParam(':il', $datos['user_update']);
            $preparado->bindParam(':f', $datos['fecha_activo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarLectivoModel($datos)
    {
        $tabla  = 'anio_lectivo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE anio_lectivo SET activo = 0, fecha_inactivo = :f, user_update = :il WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id']);
            $preparado->bindParam(':il', $datos['user_update']);
            $preparado->bindParam(':f', $datos['fecha_inactivo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
