<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloAreas extends conexion
{

    public static function agregarAreaModel($datos)
    {
        $tabla  = 'areas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_log) VALUES (:n, :il)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodosAreasModel()
    {
        $tabla  = 'areas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla;
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAreasModel($inicio, $final)
    {
        $tabla  = 'areas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " LIMIT :inicio, :final";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':inicio', $inicio, PDO::PARAM_INT);
            $preparado->bindParam(':final', $final, PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarAreaModel($datos)
    {
        $tabla  = 'areas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 1 WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarAreaModel($datos)
    {
        $tabla  = 'areas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0, id_log = :idl, fecha_inactivo = :fn WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id']);
            $preparado->bindValue(':idl', $datos['id_log']);
            $preparado->bindValue(':fn', $datos['fecha_inactivo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    /*----------------------------------------------------------------------------*/
    public static function mostrarTodosAsignaturasModel()
    {
        $tabla  = 'asignaturas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        a.*,
        (SELECT ar.nombre FROM areas ar WHERE ar.id = a.id_area) AS nom_area
        FROM " . $tabla . " a;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarAsignaturasModel($inicio, $final)
    {
        $tabla  = 'asignaturas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        an.*,
        (SELECT a.nombre FROM areas a WHERE a.id = an.id) AS area
        FROM " . $tabla . " an LIMIT :inicio, :final";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':inicio', $inicio, PDO::PARAM_INT);
            $preparado->bindParam(':final', $final, PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosAsignaturaModel($id)
    {
        $tabla  = 'asignaturas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        an.*,
        (SELECT a.nombre FROM areas a WHERE a.id = an.id) AS area
        FROM " . $tabla . " an WHERE an.id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarAsignaturaModel($datos)
    {
        $tabla  = 'asignaturas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_area, id_log) VALUES (:n, :ida, :idl)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function asignarCursoModel($datos)
    {
        $tabla  = 'asignatura_curso';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_asignatura, id_curso, horas, id_log, id_profesor) VALUES (:ids, :idc, :h, :idl, :idp)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ids', $datos['id_asignatura']);
            $preparado->bindParam(':idc', $datos['curso']);
            $preparado->bindParam(':h', $datos['horas']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':idp', $datos['profesor']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function listadoAsignaturasModel($id)
    {
        $tabla  = 'asignatura_curso';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        a.*,
        (SELECT an.nombre FROM asignaturas an WHERE an.id = a.id_asignatura) AS nom_asignatura,
        (SELECT c.nombre FROM cursos c WHERE c.id = a.id_curso) AS nom_curso,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = a.id_profesor) as nom_profesor
        FROM asignatura_curso a WHERE a.id_asignatura = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function removerCursoAsignadoModel($id)
    {
        $tabla  = 'asignatura_curso';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
    /*----------------------------------------------------------------------------*/
}
