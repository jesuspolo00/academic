<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloPeriodo extends conexion
{

    public static function agregarPeriodoModel($datos)
    {
        $tabla  = 'periodos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (descripcion, fecha_inicio, fecha_fin, porcentaje, id_lectivo, id_log) VALUES (:d, :fi, :ff, :p, :idl, :udl)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':d', $datos['descripcion']);
            $preparado->bindParam(':fi', $datos['fecha_inicio']);
            $preparado->bindParam(':ff', $datos['fecha_fin']);
            $preparado->bindParam(':p', $datos['porcentaje']);
            $preparado->bindParam(':idl', $datos['id_lectivo']);
            $preparado->bindParam(':udl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarPeriodosModel()
    {
        $tabla  = 'periodos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        *,
        (SELECT l.nombre FROM anio_lectivo l WHERE l.id = id_lectivo) AS lectivo,
        (SELECT l.activo FROM anio_lectivo l WHERE l.id = id_lectivo) AS activo_lectivo
        FROM " . $tabla;
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function sumaPorcentajesModel($id)
    {
        $tabla  = 'periodos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SUM(porcentaje) AS suma FROM periodos WHERE id_lectivo = :id AND activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
