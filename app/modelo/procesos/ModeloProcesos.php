<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloProceso extends conexion
{

    public function agregarProcesoModel($datos)
    {
        $tabla  = 'procesos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO procesos (descripcion, porcentaje, id_periodo, id_log) VALUES (:d, :p, :idp, :idl);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':d', $datos['descripcion']);
            $preparado->bindParam(':p', $datos['porcentaje']);
            $preparado->bindParam(':idp', $datos['periodo']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public function mostrarProcesosModel()
    {
        $tabla  = 'procesos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        p.*,
        (SELECT pr.descripcion FROM periodos pr WHERE pr.id = p.`id_periodo`) AS periodo,
        (SELECT (SELECT l.nombre FROM anio_lectivo l WHERE l.id = pr.id_lectivo) FROM periodos pr WHERE pr.id = p.`id_periodo`) AS anio_periodo
        FROM procesos p;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public function sumaPorcentajesProcesoModel($id)
    {
        $tabla  = 'procesos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SUM(porcentaje) AS suma FROM procesos WHERE id_periodo = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

}
