<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloCursos extends conexion
{

    public static function agregarCursoModel($datos)
    {
        $tabla  = 'nivel_educativo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO cursos (nombre, id_nivel, id_log) VALUES (:n, :idn, :idl);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idn', $datos['nivel']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCursosModel($inicio, $final)
    {
        $tabla  = 'cursos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.*,
        (SELECT n.nombre FROM nivel_educativo n WHERE n.id = c.id_nivel) AS nivel
        FROM " . $tabla . " c LIMIT :inicio, :final;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':inicio', $inicio, PDO::PARAM_INT);
            $preparado->bindParam(':final', $final, PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCursoIdModel($id)
    {
        $tabla  = 'cursos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.*,
        (SELECT n.nombre FROM nivel_educativo n WHERE n.id = c.id_nivel) AS nivel
        FROM " . $tabla . " c WHERE c.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodosCursosModel()
    {
        $tabla  = 'cursos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.*,
        (SELECT n.nombre FROM nivel_educativo n WHERE n.id = c.id_nivel) AS nivel
        FROM " . $tabla . " c;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarListadoAsignaturasModel($id)
    {
        $tabla  = 'asignatura_curso';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        a.*,
        (SELECT c.nombre FROM asignaturas c WHERE c.id = a.id_asignatura) AS nom_curso
        FROM asignatura_curso a WHERE a.id_curso = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    /*---------------------------------------------------------------------*/

    public static function agregarNivelModel($datos)
    {
        $tabla  = 'nivel_educativo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, abreviatura, id_log) VALUES (:n, :ab, :il)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':ab', $datos['abreviatura']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarNivelesModel($inicio, $final)
    {
        $tabla  = 'nivel_educativo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " LIMIT :inicio, :final";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':inicio', $inicio, PDO::PARAM_INT);
            $preparado->bindParam(':final', $final, PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodosNivelesModel()
    {
        $tabla  = 'nivel_educativo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla;
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarNivelModel($datos)
    {
        $tabla  = 'nivel_educativo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0, id_log = :idl WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id']);
            $preparado->bindValue(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarNivelModel($datos)
    {
        $tabla  = 'nivel_educativo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 1 WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
    /*----------------------------------------------------------------------------*/
}
