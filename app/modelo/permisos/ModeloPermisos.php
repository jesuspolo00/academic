<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloPermisos extends conexion
{

    public static function permisosUsuarioModel($id_modulo, $id_opcion, $id_accion, $id_perfil)
    {
        $tabla  = 'aca_permisos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT 1 FROM " . $tabla . " p WHERE p.id_modulo = :im
        AND p.id_opcion = :ip
        AND p.id_accion = :ia
        AND p.id_perfil = :iu
        AND p.activo = 1
        ORDER BY p.id DESC LIMIT 1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':im', $id_modulo);
            $preparado->bindParam(':ip', $id_opcion);
            $preparado->bindParam(':ia', $id_accion);
            $preparado->bindParam(':iu', $id_perfil);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function modulosActivosPerfilModel($id_perfil)
    {
        $tabla  = 'aca_permisos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT COUNT(id) AS modulos FROM " . $tabla . " WHERE id_perfil = :id AND activo = 1 GROUP BY id_modulo;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id_perfil);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarModulosModel()
    {
        $tabla  = 'aca_opciones';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE activo = 1 ORDER BY nombre ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function modulosIdActivosPerfilModel($id_perfil, $id_opcion)
    {
        $tabla  = 'aca_permisos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_perfil = :id AND id_opcion = :io AND activo = 1 LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id_perfil);
            $preparado->bindParam(':io', $id_opcion);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarPermisoModel($datos)
    {
        $tabla  = 'aca_permisos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO aca_permisos (id_modulo, id_opcion, id_accion, id_perfil, user_log) VALUES (:m, :o, :a, :p, :ul);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':m', $datos['modulo']);
            $preparado->bindParam(':o', $datos['opcion']);
            $preparado->bindValue(':a', '1');
            $preparado->bindValue(':p', $datos['perfil']);
            $preparado->bindParam(':ul', $datos['user']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarPermisoModel($datos)
    {
        $tabla  = 'aca_permisos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE id_modulo = :m AND id_opcion = :o AND id_accion = :a AND id_perfil = :p AND user_log = :ul AND activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':m', $datos['modulo']);
            $preparado->bindValue(':o', $datos['opcion']);
            $preparado->bindValue(':a', '1');
            $preparado->bindValue(':p', $datos['perfil']);
            $preparado->bindValue(':ul', $datos['user']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
