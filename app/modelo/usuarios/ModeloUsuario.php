<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloUsuario extends conexion
{

    public static function registrarUsuarioModel($datos)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (documento, nombre, apellido, user, pass, perfil, tipo_user, correo, telefono, user_log, activo)
        VALUES (:d, :n, :a, :us, :p, :pr, :tu, :c, :t, :idl, :act);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':d', $datos['identificacion']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':a', $datos['apellido']);
            $preparado->bindParam(':us', $datos['usuario']);
            $preparado->bindParam(':p', $datos['pass']);
            $preparado->bindParam(':pr', $datos['perfil']);
            $preparado->bindParam(':tu', $datos['tipo_usuario']);
            $preparado->bindParam(':c', $datos['email']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':act', $datos['activo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarUsuarioModel($datos)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE
        usuarios
        SET
        documento = :d,
        nombre = :n,
        apellido = :a,
        correo = :c,
        telefono = :t,
        perfil = :pf,
        tipo_user = :tu,
        user_log = :ul,
        fecha_update = :fu
        WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':d', $datos['identificacion']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':a', $datos['apellido']);
            $preparado->bindParam(':c', $datos['email']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':pf', $datos['perfil']);
            $preparado->bindParam(':tu', $datos['tipo_usuario']);
            $preparado->bindValue(':fu', $datos['fecha_update']);
            $preparado->bindParam(':ul', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_user']);
            //$preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodosUsuariosModel()
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT u.*,
        (SELECT p.nombre FROM perfiles p WHERE p.id_perfil = u.perfil) AS nom_perfil,
        (SELECT t.nombre FROM tipo_usuario t WHERE t.id = u.tipo_user) AS tipo_usuario
        FROM " . $tabla . " u";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarUsuariosModel($inicio, $final)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT u.*,
        (SELECT p.nombre FROM perfiles p WHERE p.id_perfil = u.perfil) AS nom_perfil,
        (SELECT t.nombre FROM tipo_usuario t WHERE t.id = u.tipo_user) AS tipo_usuario
        FROM " . $tabla . " u LIMIT :inicio, :final;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':inicio', $inicio, PDO::PARAM_INT);
            $preparado->bindParam(':final', $final, PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarUsuariosIdModel($id)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT u.*,
        (SELECT p.nombre FROM perfiles p WHERE p.id_perfil = u.perfil) AS nom_perfil,
        (SELECT t.nombre FROM tipo_usuario t WHERE t.id = u.tipo_user) AS tipo_usuario
        FROM " . $tabla . " u WHERE u.id_user = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarUsuarioModel($usuario)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT id_user FROM " . $tabla . " WHERE user LIKE '" . $usuario . "%' ORDER BY id_user DESC LIMIT 1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarUsuarioModel($datos)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET fecha_inactivo = :fi, activo = 0 WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id_user']);
            $preparado->bindValue(':fi', $datos['fecha']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarUsuarioModel($datos)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET fecha_activo = :fa, activo = 1 WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id_user']);
            $preparado->bindValue(':fa', $datos['fecha']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function restablecerPassModel($datos)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET pass = :p, fecha_update = :fu WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id_user']);
            $preparado->bindValue(':fu', $datos['fecha']);
            $preparado->bindValue(':p', $datos['pass']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function ultimaConexionModel($id)
    {
        $tabla  = 'session';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_user = :id ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

}
