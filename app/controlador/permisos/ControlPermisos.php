<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'permisos' . DS . 'ModeloPermisos.php';
require_once CONTROL_PATH . 'hash.php';

class ControlPermisos
{

    private static $instancia;

    public static function singleton_permisos()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function permisosUsuarioControl($id_modulo, $id_opcion, $id_accion, $id_perfil)
    {
        $mostrar = ModeloPermisos::permisosUsuarioModel($id_modulo, $id_opcion, $id_accion, $id_perfil);
        return $mostrar;
    }

    public function modulosActivosPerfilControl($id_perfil)
    {
        $mostrar = ModeloPermisos::modulosActivosPerfilModel($id_perfil);
        return $mostrar;
    }

    public function mostrarModulosControl()
    {
        $mostrar = ModeloPermisos::mostrarModulosModel();
        return $mostrar;
    }

    public function modulosIdActivosPerfilControl($id_perfil, $id_opcion)
    {
        $mostrar = ModeloPermisos::modulosIdActivosPerfilModel($id_perfil, $id_opcion);
        return $mostrar;
    }

    public function activarPermisoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['modulo']) &&
            !empty($_POST['modulo']) &&
            isset($_POST['opcion']) &&
            !empty($_POST['opcion']) &&
            isset($_POST['perfil']) &&
            !empty($_POST['perfil']) &&
            isset($_POST['user']) &&
            !empty($_POST['user'])
        ) {

            $datos = array(
                'modulo' => $_POST['modulo'],
                'opcion' => $_POST['opcion'],
                'perfil' => $_POST['perfil'],
                'user'   => $_POST['user'],
            );

            $activar = ModeloPermisos::activarPermisoModel($datos);
            return $activar;
        }
    }

    public function inactivarPermisoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['modulo']) &&
            !empty($_POST['modulo']) &&
            isset($_POST['opcion']) &&
            !empty($_POST['opcion']) &&
            isset($_POST['perfil']) &&
            !empty($_POST['perfil']) &&
            isset($_POST['user']) &&
            !empty($_POST['user'])
        ) {

            $datos = array(
                'modulo' => $_POST['modulo'],
                'opcion' => $_POST['opcion'],
                'perfil' => $_POST['perfil'],
                'user'   => $_POST['user'],
            );

            $activar = ModeloPermisos::inactivarPermisoModel($datos);
            return $activar;
        }
    }
}
