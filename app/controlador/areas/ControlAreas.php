<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'areas' . DS . 'ModeloAreas.php';

class ControlAreas
{

    private static $instancia;

    public static function singleton_areas()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarTodosAreasControl()
    {
        $mostrar = ModeloAreas::mostrarTodosAreasModel();
        return $mostrar;
    }

    public function mostrarAreasControl($inicio, $final)
    {
        $mostrar = ModeloAreas::mostrarAreasModel($inicio, $final);
        return $mostrar;
    }

    public function agregarAreaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['nombre']) &&
            !empty($_POST['nombre'])
        ) {
            $datos = array(
                'nombre' => $_POST['nombre'],
                'id_log' => $_POST['id_log'],
            );

            $guardar = ModeloAreas::agregarAreaModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function inactivarAreaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $id     = $_POST['id'];
            $id_log = $_POST['id_log'];

            $datos = array(
                'id'             => $id,
                'id_log'         => $id_log,
                'fecha_inactivo' => date('Y-m-d H:i:s'),
            );

            $buscar = ModeloAreas::inactivarAreaModel($datos);
            return $buscar;
        }
    }

    public function activarAreaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $id     = $_POST['id'];
            $id_log = $_POST['id_log'];

            $datos = array(
                'id'     => $id,
                'id_log' => $id_log,
            );

            $buscar = ModeloAreas::activarAreaModel($datos);
            return $buscar;
        }
    }

    /*----------------------------------------------------------------------------------*/
    public function mostrarTodosAsignaturasControl()
    {
        $mostrar = ModeloAreas::mostrarTodosAsignaturasModel();
        return $mostrar;
    }

    public function listadoAsignaturasControl($id)
    {
        $mostrar = ModeloAreas::listadoAsignaturasModel($id);
        return $mostrar;
    }

    public function mostrarDatosAsignaturaControl($id)
    {
        $mostrar = ModeloAreas::mostrarDatosAsignaturaModel($id);
        return $mostrar;
    }

    public function mostrarAsignaturasControl($inicio, $final)
    {
        $mostrar = ModeloAreas::mostrarAsignaturasModel($inicio, $final);
        return $mostrar;
    }

    public function registrarAsignaturaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_log'  => $_POST['id_log'],
                'nombre'  => $_POST['nombre'],
                'id_area' => $_POST['area'],
            );

            $guardar = ModeloAreas::registrarAsignaturaModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function asignarCursoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_log'        => $_POST['id_log'],
                'id_asignatura' => $_POST['id_asignatura'],
                'horas'         => $_POST['horas'],
                'curso'         => $_POST['curso'],
                'profesor'      => $_POST['profesor'],
            );

            $guardar = ModeloAreas::asignarCursoModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function removerCursoAsignadoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $remover = ModeloAreas::removerCursoAsignadoModel($_POST['id']);
            return $remover;
        }
    }
    /*----------------------------------------------------------------------------------*/
}
