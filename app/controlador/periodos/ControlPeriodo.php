<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'periodos' . DS . 'ModeloPeriodo.php';

class ControlPeriodo
{

	private static $instancia;

	public static function singleton_periodo()
	{
		if (!isset(self::$instancia)) {
			$miclase         = __CLASS__;
			self::$instancia = new $miclase;
		}
		return self::$instancia;
	}

	public function mostrarPeriodosControl()
	{
		$mostrar = ModeloPeriodo::mostrarPeriodosModel();
		return $mostrar;
	}

	public function agregarPeriodoControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_log']) &&
			!empty($_POST['id_log'])
		) {

			$suma = ModeloPeriodo::sumaPorcentajesModel($_POST['lectivo']);

			$total = $suma['suma'] + $_POST['porcentaje'];

			if ($suma['suma'] == 100) {
				echo '
				<script>
				ohSnap("Ya existe el 100% para este a&ntilde;o", {color: "red"});
				</script>
				';
			} else if ($total <= 100) {

				$datos = array(
					'descripcion'  => $_POST['descripcion'],
					'fecha_inicio' => $_POST['fecha_inicio'],
					'fecha_fin'    => $_POST['fecha_fin'],
					'porcentaje'   => $_POST['porcentaje'],
					'id_log'       => $_POST['id_log'],
					'id_lectivo'   => $_POST['lectivo'],
				);

				$guardar = ModeloPeriodo::agregarPeriodoModel($datos);

				if ($guardar == true) {
					echo '
					<script>
					ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
					setTimeout(recargarPagina,1050);

					function recargarPagina(){
						window.location.replace("index");
					}
					</script>
					';
				} else {
					echo '
					<script>
					ohSnap("Ha ocurrido un error", {color: "red"});
					</script>
					';
				}
			}
		}
	}
}
