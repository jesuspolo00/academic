<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'procesos' . DS . 'ModeloProcesos.php';

class ControlProcesos
{

	private static $instancia;

	public static function singleton_procesos()
	{
		if (!isset(self::$instancia)) {
			$miclase         = __CLASS__;
			self::$instancia = new $miclase;
		}
		return self::$instancia;
	}

	public function mostrarProcesosControl()
	{
		$mostrar = ModeloProceso::mostrarProcesosModel();
		return $mostrar;
	}

	public function agregarProcesoControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_log']) &&
			!empty($_POST['id_log'])
		) {

			$suma = ModeloProceso::sumaPorcentajesProcesoModel($_POST['periodo']);

			$total = $suma['suma'] + $_POST['porcentaje'];

			if ($suma['suma'] == 100) {
				echo '
				<script>
				ohSnap("Ya existe el 100% para este periodo", {color: "red"});
				</script>
				';
			} else if ($total <= 100) {

				$datos = array(
					'id_log'      => $_POST['id_log'],
					'descripcion' => $_POST['descripcion'],
					'porcentaje'  => $_POST['porcentaje'],
					'periodo'     => $_POST['periodo'],
				);

				$guardar = ModeloProceso::agregarProcesoModel($datos);

				if ($guardar == true) {
					echo '
					<script>
					ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
					setTimeout(recargarPagina,1050);

					function recargarPagina(){
						window.location.replace("index");
					}
					</script>
					';
				} else {
					echo '
					<script>
					ohSnap("Ha ocurrido un error", {color: "red"});
					</script>
					';
				}
			}

		}
	}

}
