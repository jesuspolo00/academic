<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'lectivo' . DS . 'ModeloLectivo.php';

class ControlLectivo
{

    private static $instancia;

    public static function singleton_lectivo()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarLectivoControl($inicio, $final)
    {
        $mostrar = ModeloLectivo::mostrarLectivoModel($inicio, $final);
        return $mostrar;
    }

    public function mostrarTodosLectivoControl()
    {
        $mostrar = ModeloLectivo::mostrarTodosLectivoModel();
        return $mostrar;
    }


    public function mostrarTipoDocumentoControl()
    {
        $mostrar = ModeloLectivo::mostrarTipoDocumentoModel();
        return $mostrar;
    }

    public function agregarLectivoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['nombre']) &&
            !empty($_POST['nombre']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'nombre' => $_POST['nombre'],
                'id_log' => $_POST['id_log'],
            );

            $guardar = ModeloLectivo::agregarLectivoModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index?pagina=1");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function activarLectivoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $id     = $_POST['id'];
            $id_log = $_POST['id_log'];
            $fecha  = date('Y-m-d H:i:s');

            $datos = array(
                'id'           => $id,
                'user_update'  => $id_log,
                'fecha_activo' => $fecha,
            );

            $buscar = ModeloLectivo::activarLectivoModel($datos);
            return $buscar;
        }
    }

    public function inactivarLectivoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $id     = $_POST['id'];
            $id_log = $_POST['id_log'];
            $fecha  = date('Y-m-d H:i:s');

            $datos = array(
                'id'             => $id,
                'user_update'    => $id_log,
                'fecha_inactivo' => $fecha,
            );

            $buscar = ModeloLectivo::inactivarLectivoModel($datos);
            return $buscar;
        }
    }

}
