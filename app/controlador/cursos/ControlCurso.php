<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'cursos' . DS . 'ModeloCurso.php';

class ControlCursos
{

    private static $instancia;

    public static function singleton_cursos()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarNivelesControl($inicio, $final)
    {
        $mostrar = ModeloCursos::mostrarNivelesModel($inicio, $final);
        return $mostrar;
    }

    public function mostrarTodosNivelesControl()
    {
        $mostrar = ModeloCursos::mostrarTodosNivelesModel();
        return $mostrar;
    }

    public function mostrarListadoAsignaturasControl($id)
    {
        $mostrar = ModeloCursos::mostrarListadoAsignaturasModel($id);
        return $mostrar;
    }

    public function mostrarCursosControl($inicio, $final)
    {
        $mostrar = ModeloCursos::mostrarCursosModel($inicio, $final);
        return $mostrar;
    }

    public function mostrarCursoIdControl($id)
    {
        $mostrar = ModeloCursos::mostrarCursoIdModel($id);
        return $mostrar;
    }

    public function mostrarTodosCursosControl()
    {
        $mostrar = ModeloCursos::mostrarTodosCursosModel();
        return $mostrar;
    }

    public function agregarCursoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['nombre']) &&
            !empty($_POST['nombre']) &&
            isset($_POST['nivel']) &&
            !empty($_POST['nivel'])
        ) {
            $datos = array(
                'nombre' => $_POST['nombre'],
                'nivel'  => $_POST['nivel'],
                'id_log' => $_POST['id_log'],
            );

            $guardar = ModeloCursos::agregarCursoModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    /**---------------------------------------------------------------------------------------*/
    public function agregarNivelControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['nombre']) &&
            !empty($_POST['nombre']) &&
            isset($_POST['abreviatura']) &&
            !empty($_POST['abreviatura'])
        ) {
            $datos = array(
                'nombre'      => $_POST['nombre'],
                'abreviatura' => $_POST['abreviatura'],
                'id_log'      => $_POST['id_log'],
            );

            $guardar = ModeloCursos::agregarNivelModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function inactivarNivelControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $id     = $_POST['id'];
            $id_log = $_POST['id_log'];

            $datos = array(
                'id'     => $id,
                'id_log' => $id_log,
            );

            $buscar = ModeloCursos::inactivarNivelModel($datos);
            return $buscar;
        }
    }

    public function activarNivelControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $id     = $_POST['id'];
            $id_log = $_POST['id_log'];

            $datos = array(
                'id'     => $id,
                'id_log' => $id_log,
            );

            $buscar = ModeloCursos::activarNivelModel($datos);
            return $buscar;
        }
    }
    /**---------------------------------------------------------------------------------------*/

    /**---------------------------------------------------------------------------------------*/

    /**---------------------------------------------------------------------------------------*/
}
