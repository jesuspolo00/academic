<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'usuarios' . DS . 'ModeloUsuario.php';
require_once CONTROL_PATH . 'hash.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreo.php';

class ControlUsuario
{

    private static $instancia;

    public static function singleton_usuario()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function ultimaConexionControl($id)
    {
        $mostrar = ModeloUsuario::ultimaConexionModel($id);
        return $mostrar;
    }

    public function mostrarTodosUsuariosControl()
    {
        $mostrar = ModeloUsuario::mostrarTodosUsuariosModel();
        return $mostrar;
    }

    public function mostrarUsuariosControl($inicio, $final)
    {
        $mostrar = ModeloUsuario::mostrarUsuariosModel($inicio, $final);
        return $mostrar;
    }

    public function mostrarUsuariosIdControl($id)
    {
        $mostrar = ModeloUsuario::mostrarUsuariosIdModel($id);
        return $mostrar;
    }

    public function registrarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['nombre']) &&
            !empty($_POST['nombre']) &&
            isset($_POST['apellido']) &&
            !empty($_POST['apellido']) &&
            isset($_POST['identificacion']) &&
            !empty($_POST['identificacion']) &&
            isset($_POST['usuario']) &&
            !empty($_POST['usuario']) &&
            isset($_POST['pass']) &&
            !empty($_POST['pass']) &&
            isset($_POST['perfil']) &&
            !empty($_POST['perfil']) &&
            isset($_POST['tipo_usuario']) &&
            !empty($_POST['tipo_usuario'])
        ) {

            $pass = Hash::hashpass($_POST['pass']);

            $datos = array(
                'id_log'         => $_POST['id_log'],
                'nombre'         => $_POST['nombre'],
                'apellido'       => $_POST['apellido'],
                'identificacion' => $_POST['identificacion'],
                'usuario'        => $_POST['usuario'],
                'pass'           => $pass,
                'perfil'         => $_POST['perfil'],
                'tipo_usuario'   => $_POST['tipo_usuario'],
                'email'          => $_POST['email'],
                'telefono'       => $_POST['telefono'],
                'activo'         => 1,
            );

            $guardar = ModeloUsuario::registrarUsuarioModel($datos);

            if ($guardar['guardar'] == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index?pagina=1");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        } else {
            echo '
            <script>
            ohSnap("Llenar campos obligatorios", {color: "red"});
            </script>
            ';
        }
    }

    public function editarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user']) &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['doc_edit']) &&
            !empty($_POST['doc_edit']) &&
            isset($_POST['nom_edit']) &&
            !empty($_POST['nom_edit']) &&
            isset($_POST['apel_edit']) &&
            !empty($_POST['apel_edit']) &&
            isset($_POST['perfil_edit']) &&
            !empty($_POST['perfil_edit']) &&
            isset($_POST['tipo_user_edit']) &&
            !empty($_POST['tipo_user_edit'])
        ) {

            $datos = array(
                'id_log'         => $_POST['id_log'],
                'nombre'         => $_POST['nom_edit'],
                'apellido'       => $_POST['apel_edit'],
                'identificacion' => $_POST['doc_edit'],
                'perfil'         => $_POST['perfil_edit'],
                'tipo_usuario'   => $_POST['tipo_user_edit'],
                'email'          => $_POST['correo_edit'],
                'telefono'       => $_POST['telefono_edit'],
                'id_user'        => $_POST['id_user'],
                'fecha_update'   => date('Y-m-d H:i:s'),
            );

            $guardar = ModeloUsuario::editarUsuarioModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Editado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index?pagina=1");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        } else {
            echo '
            <script>
            ohSnap("Llenar campos obligatorios", {color: "red"});
            </script>
            ';
        }
    }

    public function verificarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['usuario']) &&
            !empty($_POST['usuario'])
        ) {
            $usuario = $_POST['usuario'];

            $buscar = ModeloUsuario::verificarUsuarioModel($usuario);
            return $buscar;
        }
    }

    public function inactivarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {
            $id_user = $_POST['id_user'];
            $fecha   = date('Y-m-d H:i:s');

            $datos = array('id_user' => $id_user, 'fecha' => $fecha);

            $buscar = ModeloUsuario::inactivarUsuarioModel($datos);
            return $buscar;
        }
    }

    public function activarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {
            $id_user = $_POST['id_user'];
            $fecha   = date('Y-m-d H:i:s');

            $datos = array('id_user' => $id_user, 'fecha' => $fecha);

            $buscar = ModeloUsuario::activarUsuarioModel($datos);
            return $buscar;
        }
    }

    public function restablecerPassControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {
            $id_user   = $_POST['id_user'];
            $fecha     = date('Y-m-d H:i:s');
            $pass_rand = rand(100, 1000) . $id_user . '123@#.';
            $pass      = Hash::hashpass($pass_rand);

            $datos = array(
                'id_user' => $id_user,
                'fecha'   => $fecha,
                'pass'    => $pass,
            );

            $buscar = ModeloUsuario::restablecerPassModel($datos);

            if ($buscar == true) {

                $datos_usuario = $this->mostrarUsuariosIdControl($id_user);

                $mensaje = '<div>
                <p>
                Querido usuario ' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . ' el siguiente correo es para informarle que se ha restbalecido la contraseña de su control academic.
                </p>
                <ul>
                <li>
                Usuario: ' . $datos_usuario['user'] . '
                </li>
                <li>
                Contraseña: ' . $pass_rand . '
                </li>
                </ul>
                <p>
                Por favor una vez ingrese recuerde cambiar su contraseña en el apartado de perfil, muchas gracias.
                </p>
                </div>';

                $datos_correo = array(
                    'asunto'  => 'Ingreso control academic',
                    //'correo'  => 'jesuspolo00@gmail.com',
                    'correo'  => $datos_usuario['correo'],
                    'user'    => $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'],
                    'mensaje' => $mensaje,
                );

                $enviar = Correo::enviarCorreoModel($datos_correo);
            }
            return $buscar;
        }
    }

}
