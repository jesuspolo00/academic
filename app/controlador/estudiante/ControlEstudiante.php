<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'usuarios' . DS . 'ModeloUsuario.php';
require_once MODELO_PATH . 'estudiante' . DS . 'ModeloEstudiante.php';
require_once CONTROL_PATH . 'hash.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreo.php';

class ControlEstudiante
{

    private static $instancia;

    public static function singleton_estudiante()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function validarAsignacionControl($estudiante, $id)
    {
        $mostrar = ModeloEstudiante::validarAsignacionModel($estudiante, $id);
        return $mostrar;
    }

    public function mostrarContactoControl()
    {
        $mostrar = ModeloEstudiante::mostrarContactoModel();
        return $mostrar;
    }

    public function consultarInformcacionEstudianteControl($id)
    {
        $mostrar = ModeloEstudiante::consultarInformcacionEstudianteModel($id);
        return $mostrar;
    }

    public function mostrarFamiliaresControl($id)
    {
        $mostrar = ModeloEstudiante::mostrarFamiliaresModel($id);
        return $mostrar;
    }

    public function mostrarEstudianteIdControl($id)
    {
        $mostrar = ModeloEstudiante::mostrarEstudianteIdModel($id);
        return $mostrar;
    }

    public function mostrarTodosEstudiantesControl()
    {

        $lectivo    = $_POST['lectivo'];
        $curso      = $_POST['curso'];
        $estudiante = $_POST['estudiante'];

        $consulta = ($curso == 0) ? 't.anio_lectivo = ' . $lectivo : 't.anio_lectivo = ' . $lectivo . ' AND t.id_curso = ' . $curso;
        $consulta = ($estudiante == '') ? $consulta : 't.anio_lectivo = ' . $lectivo . ' AND t.id_curso = ' . $curso . ' AND e.primer_nombre LIKE "%' . $estudiante . '%"
        OR e.segundo_nombre LIKE "%' . $estudiante . '%"
        OR e.primer_apellido LIKE "%' . $estudiante . '%"
        OR e.segundo_apellido LIKE "%' . $estudiante . '%"';

        $mostrar = ModeloEstudiante::mostrarTodosEstudiantesModel($consulta);
        return $mostrar;
    }

    public function mostrarEstudiantesControl($inicio, $fin, $datos)
    {
        $mostrar = ModeloEstudiante::mostrarEstudianteModel($inicio, $fin, $datos);
        return $mostrar;
    }

    public function verificarDocumentosSubidosControl($id)
    {
        $mostrar = ModeloEstudiante::verificarDocumentosSubidosModel($id);
        return $mostrar;
    }

    public function verificarDocumentosInstSubidosControl($id)
    {
        $mostrar = ModeloEstudiante::verificarDocumentosInstSubidosModel($id);
        return $mostrar;
    }

    public function verificarObligatorioDocInstControl()
    {
        $mostrar = ModeloEstudiante::verificarObligatorioDocInstModel();
        return $mostrar;
    }

    public function mostrarDatosPrematriculaControl($id)
    {
        $mostrar = ModeloEstudiante::mostrarDatosPrematriculaModel($id);
        return $mostrar;
    }

    public function registroEstudianteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['documento']) &&
            !empty($_POST['documento']) &&
            isset($_POST['tipo_documento']) &&
            !empty($_POST['tipo_documento']) &&
            isset($_POST['lugar_exp']) &&
            !empty($_POST['lugar_exp']) &&
            isset($_POST['genero']) &&
            !empty($_POST['genero']) &&
            isset($_POST['primer_nombre']) &&
            !empty($_POST['primer_nombre']) &&
            isset($_POST['primer_apellido']) &&
            !empty($_POST['primer_apellido']) &&
            isset($_POST['fecha_nac']) &&
            !empty($_POST['fecha_nac']) &&
            isset($_POST['pais_nac']) &&
            !empty($_POST['pais_nac']) &&
            isset($_POST['departamento_nac']) &&
            !empty($_POST['departamento_nac']) &&
            isset($_POST['municipio_nac']) &&
            !empty($_POST['municipio_nac']) &&
            isset($_POST['pais_resi']) &&
            !empty($_POST['pais_resi']) &&
            isset($_POST['departamento_resi']) &&
            !empty($_POST['departamento_resi']) &&
            isset($_POST['municipio_resi']) &&
            !empty($_POST['municipio_resi'])
        ) {

            $datos = array(
                'identificacion'   => $_POST['documento'],
                'id_tipo_doc'      => $_POST['tipo_documento'],
                'lugar_expedicion' => $_POST['lugar_exp'],
                'genero'           => $_POST['genero'],
                'primer_nombre'    => $_POST['primer_nombre'],
                'segundo_nombre'   => $_POST['segundo_nombre'],
                'primer_apellido'  => $_POST['primer_apellido'],
                'segundo_apellido' => $_POST['segundo_apellido'],
                'fecha_nacimiento' => $_POST['fecha_nac'],
                'pais_nac'         => $_POST['pais_nac'],
                'departamento_nac' => $_POST['departamento_nac'],
                'municipio_nac'    => $_POST['municipio_nac'],
                'telefono'         => $_POST['telefono'],
                'celular'          => $_POST['celular'],
                'correo'           => $_POST['correo'],
                'pais_res'         => $_POST['pais_resi'],
                'departamento_res' => $_POST['departamento_resi'],
                'municipio_res'    => $_POST['municipio_resi'],
                'direccion'        => $_POST['direccion'],
                'barrio'           => $_POST['barrio'],
                'tipo_vivienda'    => $_POST['tipo_vivienda'],
                'estable_procede'  => $_POST['esta_prece'],
                'inclusion'        => $_POST['inclusion'],
                'anio_lectivo'     => $_POST['anio_lectivo'],
                'id_curso'         => $_POST['id_grado'],
                'user_log'         => $_POST['id_log'],
            );

            $guardar = ModeloEstudiante::registroEstudianteModel($datos);

            if ($guardar['guardar'] == true) {

                $pass_rand = rand(100, 1000) . $_POST['documento'] . '123@#.';
                $pass      = Hash::hashpass($pass_rand);

                $datos_usuarios = array(
                    'identificacion' => $_POST['documento'],
                    'nombre'         => $_POST['primer_nombre'] . ' ' . $_POST['segundo_nombre'],
                    'apellido'       => $_POST['primer_apellido'] . ' ' . $_POST['segundo_apellido'],
                    'usuario'        => $_POST['documento'],
                    'pass'           => $pass,
                    'perfil'         => 2,
                    'tipo_usuario'   => 2,
                    'email'          => $_POST['correo'],
                    'telefono'       => $_POST['telefono'],
                    'id_log'         => $_POST['id_log'],
                    'activo'         => $_POST['bloqueado'],
                );

                $guardar_usuario = ModeloUsuario::registrarUsuarioModel($datos_usuarios);

                if ($guardar_usuario['guardar'] == true) {

                    $datos_tipo_registro = array(
                        'id_estudiante'      => $guardar['id'],
                        'id_user_estudiante' => $guardar_usuario['id'],
                        'anio_lectivo'       => $_POST['anio_lectivo'],
                        'id_log'             => $_POST['id_log'],
                        'id_grado'           => $_POST['id_grado'],
                    );

                    $guardar_tipo_registrar = ModeloEstudiante::registrarTipoRegistroModel($datos_tipo_registro);

                    if ($guardar_tipo_registrar == true) {
                        echo '
                        <script>
                        ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                        setTimeout(recargarPagina,1050);

                        function recargarPagina(){
                            window.location.replace("index");
                        }
                        </script>
                        ';

                        $mensaje = '<div>
                        <p>
                        Querido usuario ' . $_POST['primer_nombre'] . ' ' . $_POST['segundo_nombre'] . ' ' . $_POST['primer_apellido'] . ' ' . $_POST['segundo_apellido'] . ' el siguiente correo es para informarle que se ha creado un usuario para su control academic.
                        </p>
                        <ul>
                        <li>
                        Usuario: ' . $_POST['documento'] . '
                        </li>
                        <li>
                        Contraseña: ' . $pass_rand . '
                        </li>
                        </ul>
                        <p style="color: red;">
                        Por favor una vez ingrese recuerde cambiar su contraseña en el apartado de perfil, muchas gracias.
                        </p>
                        </div>';

                        $datos_correo = array(
                            'asunto'  => 'Ingreso control academic',
                            'correo'  => 'jesus.polo@royalschool.edu.co',
                            //'correo'  => $datos_usuario['correo'],
                            'user'    => $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'],
                            'mensaje' => $mensaje,
                        );

                        $enviar = Correo::enviarCorreoModel($datos_correo);

                    } else {
                        echo '
                        <script>
                        ohSnap("Ha ocurrido un error", {color: "red"});
                        </script>
                        ';
                    }

                } else {
                    echo '
                    <script>
                    ohSnap("Ha ocurrido un error", {color: "red"});
                    </script>
                    ';
                }

            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }

        }
    }

    public function informacionAdicionalControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante'])
        ) {

            $datos_adicionales = array(
                'id_log'        => $_POST['id_log'],
                'id_estudiante' => $_POST['id_estudiante'],
                'tipo_sangre'   => $_POST['tipo_sangre'],
                'religion'      => $_POST['religion'],
                'peso'          => $_POST['peso'],
                'estatura'      => $_POST['estatura'],
                'hermanos'      => $_POST['hermanos'],
                'mayor'         => $_POST['mayor'],
                'estado_civil'  => $_POST['estado_civil'],
                'estrato'       => $_POST['estrato'],
                'contactar'     => $_POST['contactar'],
            );

            $guardar_datos_adicionales = ModeloEstudiante::informacionAdicionalModel($datos_adicionales);

            if ($guardar_datos_adicionales == true) {

                $datos_medicos = array(
                    'id_log'               => $_POST['id_log'],
                    'id_estudiante'        => $_POST['id_estudiante'],
                    'descp_medica'         => $_POST['descp_medica'],
                    'nom_descp'            => $_POST['nom_descp'],
                    'dosis_descp'          => $_POST['dosis_descp'],
                    'alerg_medica'         => $_POST['alerg_medica'],
                    'espc_alerg'           => $_POST['espc_alerg'],
                    'trat_alerg'           => $_POST['trat_alerg'],
                    'alerg_alimento'       => $_POST['alerg_alimento'],
                    'especifique_alimento' => $_POST['especifique_alimento'],
                    'dieta'                => $_POST['dieta'],
                    'cual_dieta'           => $_POST['cual_dieta'],
                    'seguro_accidente'     => $_POST['seguro_accidente'],
                    'antialergicos'        => $_POST['antialergicos'],
                    'analgesicos'          => $_POST['analgesicos'],
                    'milanta'              => $_POST['milanta'],
                    'antiespasmodico'      => $_POST['antiespasmodico'],
                    'eps'                  => $_POST['eps'],
                    'nom_medico'           => $_POST['nom_medico'],
                    'tel_medico'           => $_POST['tel_medico'],
                    'clinica'              => $_POST['clinica'],
                    'num_afilia'           => $_POST['num_afilia'],
                    'enf_ojos'             => $_POST['enf_ojos'],
                    'lentes_perm'          => $_POST['lentes_perm'],
                    'asma'                 => $_POST['asma'],
                    'enf_respiratorias'    => $_POST['enf_respiratorias'],
                    'enf_cardiaca'         => $_POST['enf_cardiaca'],
                    'enf_gastricas'        => $_POST['enf_gastricas'],
                    'diabetes'             => $_POST['diabetes'],
                    'celiaquismo'          => $_POST['celiaquismo'],
                    'pro_psiqui'           => $_POST['pro_psiqui'],
                    'hepatitis'            => $_POST['hepatitis'],
                    'anemia'               => $_POST['anemia'],
                    'hiper_arterial'       => $_POST['hiper_arterial'],
                    'hipo_arterial'        => $_POST['hipo_arterial'],
                    'epilepsia'            => $_POST['epilepsia'],
                    'hernias'              => $_POST['hernias'],
                    'migrana'              => $_POST['migrana'],
                    'frac_traum'           => $_POST['frac_traum'],
                    'artritis'             => $_POST['artritis'],
                    'desnutricion'         => $_POST['desnutricion'],
                    'obesidad'             => $_POST['obesidad'],
                    'cancer'               => $_POST['cancer'],
                    'vih'                  => $_POST['vih'],
                    'enf_renal'            => $_POST['enf_renal'],
                    'otros'                => $_POST['otros'],
                    'protesis'             => $_POST['protesis'],
                    'cirugias'             => $_POST['cirugias'],
                    'convulsion'           => $_POST['convulsion'],
                    'enf_actual'           => $_POST['enf_actual'],
                    'medicamentos'         => $_POST['medicamentos'],
                    'cond_salud'           => $_POST['cond_salud'],
                    'diabetes_fam'         => $_POST['diabetes_fam'],
                    'cancer_fam'           => $_POST['cancer_fam'],
                    'hiper_fam'            => $_POST['hiper_fam'],
                    'cardiovascular_fam'   => $_POST['cardiovascular_fam'],
                    'otros_fam'            => $_POST['otros_fam'],
                    'covid'                => $_POST['covid'],
                    'covid_diag'           => $_POST['covid_diag'],
                    'etnia'                => $_POST['etnia'],
                );

                $guardar_datos_medicos = ModeloEstudiante::informacionMedicaEstudianteModel($datos_medicos);

                if ($guardar_datos_medicos == true) {

                    echo '
    <script>
    ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
    setTimeout(recargarPagina,1050);

    function recargarPagina(){
        window.location.replace("familiar?estudiante=' . base64_encode($_POST['id_estudiante']) . '");
    }
    </script>
    ';

                    if (!empty($_FILES['foto_estudiante']['name'])) {

                        $datos_foto = array(
                            'id_estudiante' => $_POST['id_estudiante'],
                            'id_log'        => $_POST['id_log'],
                            'archivo'       => $_FILES['foto_estudiante']['name'],
                            'tipo'          => 'foto_estudiante',
                        );

                        $guardo_foto = $this->guardarFotoEstudianteControl($datos_foto);

                    }
                }
            }
        }
    }

    public function informacionFamiliarControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante'])
        ) {

            $datos = array(
                'id_log'           => $_POST['id_log'],
                'id_estudiante'    => $_POST['id_estudiante'],
                'documento'        => $_POST['documento'],
                'tipo_documento'   => $_POST['tipo_documento'],
                'primer_nombre'    => $_POST['primer_nombre'],
                'segundo_nombre'   => $_POST['segundo_nombre'],
                'primer_apellido'  => $_POST['primer_apellido'],
                'segundo_apellido' => $_POST['segundo_apellido'],
                'familiaridad'     => $_POST['familiaridad'],
                'vive_estudiante'  => $_POST['vive_estudiante'],
                'expedido'         => $_POST['expedido'],
                'correo'           => $_POST['correo'],
                'celular'          => $_POST['celular'],
                'pais'             => $_POST['pais'],
                'departamento'     => $_POST['departamento'],
                'municipio'        => $_POST['municipio'],
                'fecha_nac'        => $_POST['fecha_nac'],
                'dir_residencia'   => $_POST['dir_residencia'],
                'tel_residencia'   => $_POST['tel_residencia'],
                'tel_oficina'      => $_POST['tel_oficina'],
                'tipo_trabajo'     => $_POST['tipo_trabajo'],
                'profesion'        => $_POST['profesion'],
                'empresa'          => $_POST['empresa'],
                'cargo'            => $_POST['cargo'],
                'ex_alumno'        => $_POST['ex_alumno'],
            );

            $guardar_familiar = ModeloEstudiante::informacionFamiliarModel($datos);

            if ($guardar_familiar['guardar'] == true) {

                $datos_asignar = array(
                    'id_familiar'     => $guardar_familiar['id'],
                    'id_estudiante'   => $_POST['id_estudiante'],
                    'id_log'          => $_POST['id_log'],
                    'vive_estudiante' => $_POST['vive_estudiante'],
                    'tipo_familiar'   => $_POST['familiaridad'],
                    'resp_economico'  => $_POST['resp_economico'],

                );

                $asignar_familiar = ModeloEstudiante::asignarFamiliarEstudianteModel($datos_asignar);

                if ($asignar_familiar == true) {
                    echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("familiar?estudiante=' . base64_encode($_POST['id_estudiante']) . '");
                }
                </script>
                ';

                    if (!empty($_FILES['foto_familiar']['name'])) {

                        $datos_foto = array('id_familiar' =>
                            $guardar_familiar['id'], 'id_log' =>
                            $_POST['id_log'], 'archivo' =>
                            $_FILES['foto_familiar']['name'], 'tipo' =>
                            'foto_familiar');

                        $guardo_foto = $this->guardarFotoFamiliarControl($datos_foto);

                    }
                }
            }

        }
    }

    public function asignarFamiliarEstudianteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante']) &&
            isset($_POST['id_familiar']) &&
            !empty($_POST['id_familiar'])
        ) {
            $datos_asignar = array(
                'id_familiar'     => $_POST['id_familiar'],
                'id_estudiante'   => $_POST['id_estudiante'],
                'id_log'          => $_POST['id_log'],
                'vive_estudiante' => $_POST['vive_est_reg'],
                'tipo_familiar'   => $_POST['tipo_familiar_reg'],
                'resp_economico'  => $_POST['resp_economico'],
            );

            $asignar_familiar = ModeloEstudiante::asignarFamiliarEstudianteModel($datos_asignar);

            if ($asignar_familiar == true) {
                echo '
            <script>
            ohSnap("Asignado correctamente!", {color: "green", "duration": "1000"});
            setTimeout(recargarPagina,1050);

            function recargarPagina(){
                window.location.replace("familiar?estudiante=' . base64_encode($_POST['id_estudiante']) . '");
            }
            </script>
            ';
            }
        }
    }

    public function editarInformacionEstudianteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante'])
        ) {

            $datos_estudiante = array(
                'id_log'           => $_POST['id_log'],
                'id_estudiante'    => $_POST['id_estudiante'],
                'identificacion'   => $_POST['documento'],
                'id_tipo_doc'      => $_POST['tipo_documento'],
                'lugar_expedicion' => $_POST['lugar_exp'],
                'genero'           => $_POST['genero'],
                'primer_nombre'    => $_POST['primer_nombre'],
                'segundo_nombre'   => $_POST['segundo_nombre'],
                'primer_apellido'  => $_POST['primer_apellido'],
                'segundo_apellido' => $_POST['segundo_apellido'],
                'fecha_nacimiento' => $_POST['fecha_nac'],
                'pais_nac'         => $_POST['pais_nac'],
                'departamento_nac' => $_POST['departamento_nac'],
                'municipio_nac'    => $_POST['municipio_nac'],
                'telefono'         => $_POST['telefono'],
                'celular'          => $_POST['celular'],
                'correo'           => $_POST['correo'],
                'pais_res'         => $_POST['pais_res'],
                'departamento_res' => $_POST['departamento_res'],
                'municipio_res'    => $_POST['municipio_res'],
                'direccion'        => $_POST['direccion'],
                'barrio'           => $_POST['barrio'],
                'tipo_vivienda'    => $_POST['tipo_vivienda'],
                'estable_procede'  => $_POST['esta_prece'],
                'inclusion'        => $_POST['inclusion'],
                'user_log'         => $_POST['id_log'],
                'fecha_update'     => date('Y-m-d H:i:s'),
            );

            $datos_user = array(
                'bloqueado'     => $_POST['bloqueado'],
                'id_estudiante' => $_POST['id_estudiante'],
                'fecha_update'  => date('Y-m-d H:i:s'),
            );

            $datos_adicionales = array(
                'id_log'        => $_POST['id_log'],
                'id_estudiante' => $_POST['id_estudiante'],
                'tipo_sangre'   => $_POST['tipo_sangre'],
                'religion'      => $_POST['religion'],
                'peso'          => $_POST['peso'],
                'estatura'      => $_POST['estatura'],
                'hermanos'      => $_POST['hermanos'],
                'mayor'         => $_POST['mayor'],
                'estado_civil'  => $_POST['estado_civil'],
                'estrato'       => $_POST['estrato'],
                'id_contacto'   => $_POST['contactar'],
                'fecha_update'  => date('Y-m-d H:i:s'),
            );

            $datos_medicos = array(
                'id_log'               => $_POST['id_log'],
                'id_estudiante'        => $_POST['id_estudiante'],
                'descp_medica'         => $_POST['descp_medica'],
                'nom_descp'            => $_POST['nom_descp'],
                'dosis_descp'          => $_POST['dosis_descp'],
                'alerg_medica'         => $_POST['alerg_medica'],
                'espc_alerg'           => $_POST['espc_alerg'],
                'trat_alerg'           => $_POST['trat_alerg'],
                'alerg_alimento'       => $_POST['alerg_alimento'],
                'especifique_alimento' => $_POST['especifique_alimento'],
                'dieta'                => $_POST['dieta'],
                'cual_dieta'           => $_POST['cual_dieta'],
                'seguro_accidente'     => $_POST['seguro_accidente'],
                'antialergicos'        => $_POST['antialergicos'],
                'analgesicos'          => $_POST['analgesicos'],
                'milanta'              => $_POST['milanta'],
                'antiespasmodico'      => $_POST['antiespasmodico'],
                'eps'                  => $_POST['eps'],
                'nom_medico'           => $_POST['nom_medico'],
                'tel_medico'           => $_POST['tel_medico'],
                'clinica'              => $_POST['clinica'],
                'num_afilia'           => $_POST['num_afilia'],
                'enf_ojos'             => $_POST['enf_ojos'],
                'lentes_perm'          => $_POST['lentes_perm'],
                'asma'                 => $_POST['asma'],
                'enf_respiratorias'    => $_POST['enf_respiratorias'],
                'enf_cardiaca'         => $_POST['enf_cardiaca'],
                'enf_gastricas'        => $_POST['enf_gastricas'],
                'diabetes'             => $_POST['diabetes'],
                'celiaquismo'          => $_POST['celiaquismo'],
                'pro_psiqui'           => $_POST['pro_psiqui'],
                'hepatitis'            => $_POST['hepatitis'],
                'anemia'               => $_POST['anemia'],
                'hiper_arterial'       => $_POST['hiper_arterial'],
                'hipo_arterial'        => $_POST['hipo_arterial'],
                'epilepsia'            => $_POST['epilepsia'],
                'hernias'              => $_POST['hernias'],
                'migrana'              => $_POST['migrana'],
                'frac_traum'           => $_POST['frac_traum'],
                'artritis'             => $_POST['artritis'],
                'desnutricion'         => $_POST['desnutricion'],
                'obesidad'             => $_POST['obesidad'],
                'cancer'               => $_POST['cancer'],
                'vih'                  => $_POST['vih'],
                'enf_renal'            => $_POST['enf_renal'],
                'otros'                => $_POST['otros'],
                'protesis'             => $_POST['protesis'],
                'cirugias'             => $_POST['cirugias'],
                'convulsion'           => $_POST['convulsion'],
                'enf_actual'           => $_POST['enf_actual'],
                'medicamentos'         => $_POST['medicamentos'],
                'cond_salud'           => $_POST['cond_salud'],
                'diabetes_fam'         => $_POST['diabetes_fam'],
                'cancer_fam'           => $_POST['cancer_fam'],
                'hiper_fam'            => $_POST['hiper_fam'],
                'cardiovascular_fam'   => $_POST['cardiovascular_fam'],
                'otros_fam'            => $_POST['otros_fam'],
                'covid'                => $_POST['covid'],
                'covid_diag'           => $_POST['covid_diag'],
                'etnia'                => $_POST['etnia'],
                'fecha_update'         => date('Y-m-d H:i:s'),
            );

            $act_estudiante = ModeloEstudiante::editarInformacionEstudianteModel($datos_estudiante);

            if ($act_estudiante == true) {

                $act_usuario = ModeloEstudiante::bloquearEstudianteModel($datos_user);

                if ($act_usuario == true) {

                    $act_adicional = ModeloEstudiante::actualizarDatosAdicionalesModel($datos_adicionales);

                    if ($act_adicional == true) {

                        $act_datos_medicos = ModeloEstudiante::actualizarDatosMedicosModel($datos_medicos);

                        if ($act_datos_medicos == true) {

                            echo '
                        <script>
                        ohSnap("Actualizado correctamente!", {color: "green", "duration": "1000"});
                        setTimeout(recargarPagina,1050);

                        function recargarPagina(){
                            window.location.replace("hoja_vida?estudiante=' . base64_encode($_POST['id_estudiante']) . '");
                        }
                        </script>
                        ';

                        }
                    }
                }
            }

            if (!empty($_FILES['foto_estudiante']['name'])) {

                $datos_foto = array(
                    'id_estudiante' => $_POST['id_estudiante'],
                    'id_log'        => $_POST['id_log'],
                    'archivo'       => $_FILES['foto_estudiante']['name'],
                    'tipo'          => 'foto_estudiante');

                $guardo_foto = $this->guardarFotoEstudianteControl($datos_foto);

            }
        }
    }

    public function guardarFotoEstudianteControl($datos)
    {
        //extraer la extencion del archivo de el archivo
        $ext_arch   = new SplFileInfo($datos['archivo']);
        $fecha_arch = date('YmdHis');

        $nombre_archivo = strtolower(md5($datos['id_estudiante'] . '_' . $fecha_arch)) . '.' . $ext_arch->getExtension();

        $datos_temp = array(
            'nombre'        => $nombre_archivo,
            'id_log'        => $datos['id_log'],
            'id_estudiante' => $datos['id_estudiante'],
        );

        $guardar_foto = ModeloEstudiante::guardarFotoEstudianteModel($datos_temp);

        if ($guardar_foto == true) {
            //ruta donde de alojamiento el archivo
            $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
            $ruta_img     = $carp_destino . $nombre_archivo;

            //verificar si subio el archivo y se mueve a su destino
            if (is_uploaded_file($_FILES['' . $datos['tipo'] . '']['tmp_name'])) {
                move_uploaded_file($_FILES['' . $datos['tipo'] . '']['tmp_name'], $ruta_img);
            }

            return true;
        }
    }

    public function guardarFotoFamiliarControl($datos)
    {
        //extraer la extencion del archivo de el archivo
        $ext_arch   = new SplFileInfo($datos['archivo']);
        $fecha_arch = date('YmdHis');

        $nombre_archivo = strtolower(md5($datos['id_familiar'] . '_' . $fecha_arch)) . '.' . $ext_arch->getExtension();

        $datos_temp = array(
            'nombre'      => $nombre_archivo,
            'id_log'      => $datos['id_log'],
            'id_familiar' => $datos['id_familiar'],
        );

        $guardar_foto = ModeloEstudiante::guardarFotoFamiliarModel($datos_temp);

        if ($guardar_foto == true) {
            //ruta donde de alojamiento el archivo
            $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
            $ruta_img     = $carp_destino . $nombre_archivo;

            //verificar si subio el archivo y se mueve a su destino
            if (is_uploaded_file($_FILES['' . $datos['tipo'] . '']['tmp_name'])) {
                move_uploaded_file($_FILES['' . $datos['tipo'] . '']['tmp_name'], $ruta_img);
            }

            return true;
        }
    }

    public function consultarDocumentoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['documento']) &&
            !empty($_POST['documento']) &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante'])
        ) {
            $documento     = $_POST['documento'];
            $id_estudiante = $_POST['id_estudiante'];

            $buscar = ModeloEstudiante::consultarDocumentoModel($documento, $id_estudiante);
            return $buscar;
        }
    }

    public function removerFamiliarControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $id = $_POST['id'];

            $buscar = ModeloEstudiante::removerFamiliarModel($id);
            return $buscar;
        }
    }

    public function enviarObservacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante']) &&
            isset($_POST['correo_destino']) &&
            !empty($_POST['correo_destino'])
        ) {
            $datos = array(
                'id_persona'     => $_POST['id_estudiante'],
                'id_log'         => $_POST['id_log'],
                'correo_destino' => $_POST['correo_destino'],
                'asunto'         => $_POST['asunto'],
                'mensaje'        => $_POST['mensaje'],
                'tipo_persona'   => 1,
            );

            $guardar_observacion = ModeloEstudiante::enviarObservacionModel($datos);

            if ($guardar_observacion == true) {

                $mensaje = '<div>
            <p style="font-size: 1.2em;">
            El siguiente correo es para informarle que el estudiante/familiar <b>' . $_POST['nombre_completo'] . '</b> se encuentra con la <b>' . $_POST['asunto'] . '</b> erronea o mal diligenciada, ingrese al sistema para diligenciar la información nuevamente para poder continuar con el proceso de matricula.
            </p>
            <p style="font-size: 1.2em;">
            <b>Observación: </b>' . $_POST['mensaje'] . '
            </p>
            </div>';

                $datos_correo = array(
                    'asunto'  => $_POST['asunto'],
                    'user'    => $_POST['nombre_completo'],
                    'correo'  => $_POST['correo_destino'],
                    'mensaje' => $mensaje,
                );

                $enviar = Correo::enviarCorreoModel($datos_correo);

                if ($enviar == true) {
                    echo '
                <script>
                ohSnap("Enviado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("hoja_vida?estudiante=' . base64_encode($_POST['id_estudiante']) . '");
                }
                </script>
                ';
                }

            }
        }
    }

    public function documentosSolicitadosControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante'])
        ) {

            if (!empty($_FILES['documento_estudiante'])) {
                $datos = array(
                    'id'           => $_POST['id_estudiante'],
                    'id_log'       => $_POST['id_log'],
                    'archivo'      => $_FILES['documento_estudiante']['name'],
                    'tipo'         => 'documento_estudiante',
                    'tipo_persona' => 1,
                    'tipo_doc'     => 1,
                    'prematricula' => '',
                );

                $guardar = $this->guardarDocumentos($datos);
            }

            if (!empty($_FILES['certificado_medico'])) {
                $datos = array(
                    'id'           => $_POST['id_estudiante'],
                    'id_log'       => $_POST['id_log'],
                    'archivo'      => $_FILES['certificado_medico']['name'],
                    'tipo'         => 'certificado_medico',
                    'tipo_persona' => 1,
                    'tipo_doc'     => 2,
                    'prematricula' => '',
                );

                $guardar = $this->guardarDocumentos($datos);
            }

            if (!empty($_FILES['certificado_vista'])) {
                $datos = array(
                    'id'           => $_POST['id_estudiante'],
                    'id_log'       => $_POST['id_log'],
                    'archivo'      => $_FILES['certificado_vista']['name'],
                    'tipo'         => 'certificado_vista',
                    'tipo_persona' => 1,
                    'tipo_doc'     => 3,
                    'prematricula' => '',
                );

                $guardar = $this->guardarDocumentos($datos);
            }

            if ($guardar == true) {

                $array_id = array();
                $array_id = $_POST['id_familiar'];

                $array_documento = array();
                $array_documento = $_FILES['documento_familiar']['name'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_id));
                $it->attachIterator(new ArrayIterator($array_documento));

                foreach ($it as $a) {
                    $datos = array(
                        'id'           => $a[0],
                        'id_log'       => $_POST['id_log'],
                        'archivo'      => $a[1],
                        'tipo'         => 'documento_familiar',
                        'tipo_persona' => 2,
                        'tipo_doc'     => 1,
                        'prematricula' => '',
                    );

                    $guardar_archivos = $this->guardarDocumentos($datos);
                }

                if ($guardar_archivos == true) {

                    $documentos_obl = ModeloEstudiante::verificarObligatorioDocInstModel();

                    if ($documentos_obl['id'] != "") {

                        echo '
                    <script>
                    ohSnap("Subido correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("documentos_inst?estudiante=' . base64_encode($_POST['id_estudiante']) . '&prematricula=' . base64_encode($_POST['prematricula']) . '");
                    }
                    </script>
                    ';
                    } else {
                        echo '
                    <script>
                    ohSnap("Subido correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("listado");
                    }
                    </script>
                    ';
                    }
                }
            }
        }

    }

    public function guardarDocumentosInstitucionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante'])
        ) {

            if (!empty($_FILES['contrato'])) {
                $datos = array(
                    'id'           => $_POST['id_estudiante'],
                    'id_log'       => $_POST['id_log'],
                    'archivo'      => $_FILES['contrato']['name'],
                    'tipo'         => 'contrato',
                    'tipo_persona' => 1,
                    'tipo_doc'     => 4,
                    'prematricula' => $_POST['id_prematricula'],
                );

                $guardar = $this->guardarDocumentos($datos);
            }

            if (!empty($_FILES['pagare'])) {
                $datos = array(
                    'id'           => $_POST['id_estudiante'],
                    'id_log'       => $_POST['id_log'],
                    'archivo'      => $_FILES['pagare']['name'],
                    'tipo'         => 'pagare',
                    'tipo_persona' => 1,
                    'tipo_doc'     => 5,
                    'prematricula' => $_POST['id_prematricula'],
                );

                $guardar = $this->guardarDocumentos($datos);
            }

            if (!empty($_FILES['hoja_matricula'])) {
                $datos = array(
                    'id'           => $_POST['id_estudiante'],
                    'id_log'       => $_POST['id_log'],
                    'archivo'      => $_FILES['hoja_matricula']['name'],
                    'tipo'         => 'hoja_matricula',
                    'tipo_persona' => 1,
                    'tipo_doc'     => 6,
                    'prematricula' => $_POST['id_prematricula'],
                );

                $guardar = $this->guardarDocumentos($datos);
            }

            if ($guardar == true) {
                echo '
            <script>
            ohSnap("Subido correctamente!", {color: "green", "duration": "1000"});
            setTimeout(recargarPagina,1050);

            function recargarPagina(){
                window.location.replace("listado");
            }
            </script>
            ';
            }
        }
    }

    public function guardarDocumentos($datos)
    {
        //extraer la extencion del archivo de el archivo
        $ext_arch   = new SplFileInfo($datos['archivo']);
        $fecha_arch = date('YmdHis');

        $nombre_archivo = strtolower(md5($datos['id'] . '_' . $datos['tipo_doc'] . '_' . $fecha_arch)) . '.' . $ext_arch->getExtension();

        $datos_guardar = array(
            'id_persona'   => $datos['id'],
            'id_log'       => $datos['id_log'],
            'nombre'       => $nombre_archivo,
            'tipo_persona' => $datos['tipo_persona'],
            'tipo_doc'     => $datos['tipo_doc'],
            'prematricula' => $datos['prematricula'],
        );

        $guardar = ModeloEstudiante::documentosSolicitadosModel($datos_guardar);

        if ($guardar == true) {

            //ruta donde de alojamiento el archivo
            $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
            $ruta_img     = $carp_destino . $nombre_archivo;

            //verificar si subio el archivo y se mueve a su destino
            if (is_uploaded_file($_FILES['' . $datos['tipo'] . '']['tmp_name'])) {
                move_uploaded_file($_FILES['' . $datos['tipo'] . '']['tmp_name'], $ruta_img);
            }
        }
        return true;
    }
}
